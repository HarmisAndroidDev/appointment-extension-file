		
<?php 
 /**

* @package Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/

defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.tooltip');

JHtml::_('behavior.formvalidator');
JHtml::_('behavior.formvalidation');
JHTMLBehavior::modal();
$uri =JURI::getInstance();
$url= $uri->root();
$model = $this->getModel('conform_order'); 

$option = JRequest::getVar('option','','request','string');

$document 	= JFactory::getDocument();
$document->addStyleSheet('components/'.$option.'/assets/css/style.css');
$staffname = JRequest::getVar('name','','request','');
$path=$url.'/'."components/".$option.'/assets/service/images/thumb_';
$oid = JRequest::getVar('oid','','request','string');

$document->addStyleSheet("components/com_appointment/assets/css/appointment.css");

    
?>
<div class="componetntdiv">

	<form action="<?php echo 'index.php?option='.$option.'&view=conform_order&limitstart=0'; ?>" method="post" name="adminForm" enctype="multipart/form-data" class="required validate-email form-validate">
 
    <?php
   

      for($i=0;$i<count($this->order);$i++)
      {
        $row=$this->order[$i];

        $orderid=$row->oid;
        if($orderid==$oid)
        {
           $services=$model->getorder($orderid);
             for($j=0;$j<count($services);$j++)
             {
                 $id1= $services[$j]->id;
                 $services1=$model->getorderservice($id1);
  ?>
          
     <div class="service_list_main_div">
          <div class="vaporderboxcontent">
                <div class="vap-order-first">
                    <h3 class="vaporderheader vap-head-first">Your Order</h3>
                        <div class="vap-printable">
                           <a target="_blank" href="/appoint_new/index.php?task=printorder&amp;oid=204&amp;sid=8D2791623PG19014&amp;tmpl=component">
                              <img src="http://192.168.1.118/appoint_new/components/com_vikappointments/assets/css/images/printer.png">
                            </a>
                        </div>
                </div>
                <div class="vaporderboxleft">
                    <div class="vapordercontentinfo">
                        <div class="vaporderinfo">Order Number: <?php  echo $orderid=$row->oid;?> </div>
                       
                        <div style="display: inline-block;" class="vaporderinfo">Status: </div>
                     <div class="vaporderinfo vapreservationstatusconfirmed" style="display: inline-block;"><?php  echo $orderid=$row->status;?></div>
                        <div class="vaporderinfo">Total Cost: <?php echo $row->totalprice;?></div>
                                                            
                    </div>
                        <div class="vaorderboxright"><div class="vaporderinfo">First Name: <?php echo  $row->fname ?></div>
                        <div class="vaporderinfo">Last Name: <?php echo $row->lname ?></div>
                        <div class="vaporderinfo">Email: <?php echo $row->email ?> </div>
                        <div class="vaporderinfo">Phone Number: <?php echo $row->contact ?></div>            
                </div>
         </div>
    </div> 


<!-- =============== Service & Variation Div ============ -->

<!--
<?php
 for($k=0;$k<count($services1);$k++)
    {
              $str=$services1[$k]->vname;
              $lst = explode(",", $str);
              $str1=$services1[$k]->vprice;
              $lst1 = explode(",", $str1);
 ?>
        <div class="vaporderboxcontent">
                <div class="vap-order-first">
                     <h3 class="vaporderheader vap-head-first"><?php echo $services1[$k]->servicename; ?></h3>
                </div>                    
                        <div class="vaporderdetailsbox">    
                            <div class="vapordercontentinfoleft">
                                <h3 class="vaporderheader">Details</h3>
                                    <div class="vapordercontentinfo">
                                        <div class="vaporderinfo">Service: <?php echo  $services1[$k]->servicename; ?> </div>
                                         <div class="vaporderinfo">Price: <?php echo $services1[$k]->price; ?></div>

                                         <?php
                                           foreach ($lst as $v)
                                             {
                                               if($v!='')
                                               {
                                          ?>
                                              <div class="vaporderinfo" >Variation Name: <?php echo  $v; ?> </div>
                                          <?php } }

                                            ?>
                                      
                                             <?php
                                           foreach ($lst1 as $v1)
                                           {
                                              if($v1!='')
                                              {
                                            ?>
                                            
                                              <div class="vaporderinfo1" >Variation Price: <?php echo  $v1; ?> </div><br/>
                                          <?php } }

                                            ?>
                                   </div>
                            </div>
                            
                        </div>          
        </div>


<?php
}
?>
        
<?php
            
        }
        }

      }
   

   ?>
-->

<!-- ================== Service & Variation Div Part over =============-->


		<input type="hidden" name="id[]" id="id[]" value="<?php echo $this->order->id; ?>"/>
		<input type="hidden" name="view" value="conform_order" />
		<input type="hidden" name="option" id="option" value="com_appointment">
		<input type="hidden" name="task" value="save" />
			
		</form>
</div>

