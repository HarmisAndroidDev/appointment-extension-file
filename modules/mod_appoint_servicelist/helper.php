<?php
/**
* @package   JE itemlist
* @copyright Copyright (C) 2009-2010 Joomlaextensions.co.in All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
**/

defined('_JEXEC') or die('Restricted access');
 
class mod_appoint_servicelistHelper
{

public static function getReturnURL()
	{
		$uri = JFactory::getURI();
		$url = $uri->toString(array('path', 'query', 'fragment'));

		return md5($url);
	}
	
	

	 public static function getser($id)
		{
			
			$paramid = $id->get('myfield', '0');
			if(count($paramid) && is_array($paramid)){
				$id1=implode(",", $paramid);
				$where = " AND id IN( ".$id1." )";
			}else{
				$where = "";
			}
			 
			
			$db = JFactory::getDBO();
			$_table_prefix = '#__appointment_';
			$query = 'SELECT name,price,img,description,id FROM '.$_table_prefix.'services WHERE 1=1 '.$where;
			$db->setQuery($query);
			return $db->LoadObjectList();
		}

}