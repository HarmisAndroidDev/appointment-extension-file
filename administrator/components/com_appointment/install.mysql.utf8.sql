

-- --------------------------------------------------------

--
-- Table structure for table `jos_appointment_addnew`
--

CREATE TABLE IF NOT EXISTS `#__appointment_addnew` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `price` float(8,2) NOT NULL,
  `img` varchar(255) NOT NULL,
  `adminid` int(11) NOT NULL,
  `published` varchar(255) NOT NULL DEFAULT '1',
  `ordering` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_appointment_addnew`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_appointment_employee`
--

CREATE TABLE IF NOT EXISTS `#__appointment_employee`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` text NOT NULL,
  `lname` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `group` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `published` int(255) NOT NULL DEFAULT '1',
  `ordering` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_appointment_employee`
--


--
-- Table structure for table `jos_appointment_employeedaytime`
--

CREATE TABLE IF NOT EXISTS `#__appointment_employeedaytime` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eid` int(11) NOT NULL,
  `day` text NOT NULL,
  `closed` int(11) NOT NULL DEFAULT '0',
  `time` int(255) NOT NULL,
  `minute` int(255) NOT NULL,
  `time1` int(255) NOT NULL,
  `minute1` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_appointment_employeedaytime`
--

-- --------------------------------------------------------

--
-- Table structure for table `jos_appointment_groupsdetail`
--

CREATE TABLE IF NOT EXISTS `#__appointment_groupsdetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `description` varchar(255) NOT NULL,
  `parent` int(255) NOT NULL,
  `adminid` int(11) NOT NULL,
  `published` int(255) NOT NULL DEFAULT '1',
  `ordering` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_appointment_groupsdetail`
--

-- --------------------------------------------------------

--
-- Table structure for table `jos_appointment_order`
--

CREATE TABLE IF NOT EXISTS `#__appointment_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oid` int(255) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `totalprice` int(11) NOT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_appointment_order`
--

-- --------------------------------------------------------

--
-- Table structure for table `jos_appointment_orderbook`
--

CREATE TABLE IF NOT EXISTS `#__appointment_orderbook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empname` varchar(255) NOT NULL,
  `day` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `service` varchar(255) NOT NULL,
  `month` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_appointment_orderbook`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_appointment_orderservice`
--

CREATE TABLE IF NOT EXISTS `#__appointment_orderservice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orid` int(255) NOT NULL,
  `servicename` varchar(255) NOT NULL,
  `price` int(255) NOT NULL,
  `vname` varchar(255) NOT NULL,
  `vprice` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_appointment_orderservice`
--

-- --------------------------------------------------------

--
-- Table structure for table `jos_appointment_serviceemp`
--

CREATE TABLE IF NOT EXISTS `#__appointment_serviceemp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sid` int(11) NOT NULL,
  `eid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_appointment_serviceemp`
--

-- --------------------------------------------------------

--
-- Table structure for table `jos_appointment_serviceoption`
--

CREATE TABLE IF NOT EXISTS `#__appointment_serviceoption` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sid` int(255) NOT NULL,
  `aid` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_appointment_serviceoption`
--

-- --------------------------------------------------------

--
-- Table structure for table `jos_appointment_services`
--

CREATE TABLE IF NOT EXISTS `#__appointment_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `duration` int(255) NOT NULL,
  `sleeptime` int(255) NOT NULL,
  `price` float(8,2) NOT NULL,
  `img` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `groups` int(255) NOT NULL,
  `adminid` int(11) NOT NULL,
  `published` int(255) NOT NULL DEFAULT '1',
  `ordering` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_appointment_services`
--

-- --------------------------------------------------------

--
-- Table structure for table `jos_appointment_user`
--

CREATE TABLE IF NOT EXISTS `#__appointment_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `ordering` varchar(255) NOT NULL,
  `userid` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_appointment_user`
--



-- --------------------------------------------------------

--
-- Table structure for table `jos_appointment_variation`
--

CREATE TABLE IF NOT EXISTS `#__appointment_variation` (
  `vid` int(11) NOT NULL AUTO_INCREMENT,
  `id` int(11) NOT NULL,
  `vname` varchar(255) NOT NULL,
  `vprice` float(8,2) NOT NULL,
  `adminid` int(11) NOT NULL,
  PRIMARY KEY (`vid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_appointment_variation`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_assets`
--
