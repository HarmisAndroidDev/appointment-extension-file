<?php
/**
* @package Appointment
* @copyright Copyright (C) 2009-2010 Joomlaextensions.co.in All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
**/ 
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.model');

class Tableservices extends JTable
{
	var $id = null;
	var $name = null;
	var $duration=null;
	var $sleeptime=null;
	var $price=null;
	var $img=null;
	var $description = null;
	var $parent=null;
	
 	
 	function Tableservices(& $db)
 	 {
 	 	 $this->_table_prefix = '#__appointment_';
 	 	 parent::__construct($this->_table_prefix.'services','id',$db);

 	 }
 	 function bind($array, $ignore = '')
	{

		if (key_exists( 'params', $array ) && is_array( $array['params'] )) {
			$registry = new JRegistry();
			$registry->loadArray($array['params']);
			$array['params'] = $registry->toString();
	}

		return parent::bind($array, $ignore);
	}

}

?>
