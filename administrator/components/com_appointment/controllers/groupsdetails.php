<?php
 /**

* @package    Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );
jimport('joomla.filesystem.file');
class groupsdetailsController extends JControllerLegacy
{
	function __construct( $default = array())
	{
		$this->_table_prefix = '#__appointment_';
		parent::__construct( $default );
	}	
	
	function display($cachable = false, $urlparams = '')
	{

		parent::display();
	}
	
	function edit() {
		JRequest::setVar ( 'view', 'groupsdetails' );
		JRequest::setVar ( 'layout', 'default' );
		JRequest::setVar ( 'hidemainmenu', 1 );
		parent::display ();
	
	}

	function save() 
	{
		$post = JRequest::get ( 'POST' );
		
		$option = JRequest::getVar('option','','request','string');
		$user	= clone(JFactory::getUser());
		$adminid= $user->id;
		
		$post['adminid']=$adminid;
		$post["description"] = JRequest::getVar( 'description', '', 'POST', 'string', JREQUEST_ALLOWRAW );
			
		$cid = JRequest::getVar ('cid', array(0), 'POST', 'array' );
					
	  	$post ['id'] = $cid [0];
	  	$post['parent']=JRequest::getvar('groups_id','','POST','string',JREQUEST_ALLOWRAW);
			  	
		$db	=JFactory::getDBO();
		$model = $this->getModel ( 'groupsdetails' );
		
		$insertid=$model->store ( $post );
		
		if ($insertid>0)
		 {
		
			if($cid[0]==0)
			{				
				$sql = "SELECT max(ordering) As ordering FROM ".$this->_table_prefix."groupsdetail ";
				$db->setQuery($sql);
				$max  = $db->loadObject();
			if($max->ordering)
				$order = $max->ordering + 1;
			else
				$order = 1;
				$query = "UPDATE ".$this->_table_prefix."groupsdetail SET ordering = ".$order." WHERE name = '".$post['name']."'";
				$db->setQuery($query);
				$db->query();
		}
				$msg = JText::_ ( 'DETAIL_SAVED' );
		} 	else 
		{
				$msg = JText::_ ( 'ERROR_SAVING_DETAIL' );
		}
				
		$this->setRedirect ( 'index.php?option=' . $option . '&view=groups', $msg );
		
	}
	
		
	 function remove() 
	 {
		$post = JRequest::get ( 'POST' );
			$option = JRequest::getVar('option','','request','string');
		
	 			$cid = JRequest::getVar ( 'cid', array (0 ), 'post', 'array' );
	
		
		if (! is_array ( $cid ) || count ( $cid ) < 1) 
		{
			JError::raiseError ( 500, JText::_ ( 'SELECT_AN_ITEM_TO_DELETE' ) );
		}
		
		$model = $this->getModel ( 'groupsdetails' );
		if (! $model->delete( $cid )) 
		{
			echo "<script> alert('" . $model->getError ( true ) . "'); window.history.go(-1); </script>\n";
		}
		$msg = JText::_ ( 'DETAIL_DELETED_SUCCESSFULLY' );
		$this->setRedirect ( 'index.php?option='.$option.'&view=groupsdetails',$msg );
	}
	function publish() 
	{			
		 $option = JRequest::getVar('option','','request','string');
		
		 $cid = JRequest::getVar ( 'cid', array (0), 'POST', 'array' );
		 
		if (! is_array ( $cid ) || count ( $cid ) < 1) 
		{
		  JError::raiseError ( 500, JText::_ ( 'SELECT_AN_ITEM_TO_PUBLISH' ) );
		}
		
		$model = $this->getModel ( 'groupsdetails' );
		if (! $model->publish ( $cid, 1 ))
		 {
			echo "<script> alert('" . $model->getError ( true ) . "'); window.history.go(-1); </script>\n";
		 }
		$msg = JText::_ ( 'DETAIL_PUBLISHED_SUCCESSFULLY' );
		$this->setRedirect ( 'index.php?option='.$option.'&view=groups',$msg );
	}
	
	function unpublish() 
	{
		
		$option = JRequest::getVar('option','','request','string');
		
		$cid = JRequest::getVar ( 'cid', array (0 ), 'POST', 'array' );
		
		if (! is_array ( $cid ) || count ( $cid ) < 1) {
			JError::raiseError ( 500, JText::_ ( 'SELECT_AN_ITEM_TO_UNPUBLISH' ) );
		}
		$model = $this->getModel ( 'groupsdetails' );
		if (! $model->publish ( $cid, 0 )) {
			echo "<script> alert('" . $model->getError ( true ) . "'); window.history.go(-1); </script>\n";
		}
		$msg = JText::_ ( 'DETAIL_UNPUBLISHED_SUCCESSFULLY' );
		$this->setRedirect ( 'index.php?option='.$option.'&view=groups',$msg );
	}
	
	
	function apply() 
	{
		$post = JRequest::get ( 'POST' );
			$user	= clone(JFactory::getUser());
		$adminid= $user->id;
		
		$post['adminid']=$adminid;
		$option = JRequest::getVar('option','','request','string');

		$post["description"] = JRequest::getVar( 'description', '', 'POST', 'string', JREQUEST_ALLOWRAW );
		
	  	$post ['id'] = $post ['cid'];
	  	
		$db	=JFactory::getDBO();
		$model = $this->getModel ( 'groupsdetails' );
		 
			$insertid= $model->store ( $post );

		if ($insertid) 
		{
			if($id[0]==0)
			{	
				 $sql = "SELECT max(ordering) As ordering FROM ".$this->_table_prefix."groupsdetail ";
				 $db->setQuery($sql);
				 $max  = $db->loadObject();
			 	if($max->ordering)

						$order = $max->ordering + 1;
				else

						$order = 1;
			}
				$msg = JText::_ ( 'DETAIL_SAVED' );
		} 
		else 
		{
			$msg = JText::_ ( 'ERROR_SAVING_DETAIL' );
		}
			
		$this->setRedirect ('index.php?option='.$option.'&view=groupsdetails&task=edit&cid[]='.$insertid, $msg );
		
	}
	
	function cancel() {
		
		$option = JRequest::getVar('option','com_appointment','request','string');
		$msg = JText::_ ( 'DETAIL_EDITING_CANCELLED' );
		$this->setRedirect ( 'index.php?option='.$option.'&view=groups',$msg );
	}
}
?>
