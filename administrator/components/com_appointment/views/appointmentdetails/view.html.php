<?php
 /**

* @package   Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/

defined('_JEXEC') or die ('restricted access');
jimport('joomla.application.component.view');
class appointmentdetailsViewappointmentdetails extends JViewLegacy{	
		function display($tpl = null)
		{
			$document =  JFactory::getDocument();
			$document->setTitle( JText::_('APPOINTMENTDETAILS') );
			$uri = JFactory::getURI();			
			$option = JRequest::getVar('option','','request','string');
			$db = jFactory::getDBO();
			$appoitment= $this->get('data');
			$model=$this->getModel('appointmentdetails');
			$variation	= $model->getvariation($appoitment->id); 
			$cdset=$this->get('data');
			// echo '<pre>';
			// print_r($cdset);
			// exit;
			$isNew = ($appoitment->id < 1);
			$text = $isNew ? JText::_( 'NEW' ) : JText::_( 'EDIT' );
			JToolBarHelper::title(JText::_( 'NEW_LIST' ).': <small><small>[ ' . $text.' ]</small></small>');
			JToolBarHelper::apply(); //for save button
			JToolBarHelper::save(); //for save & close bttun
			if ($isNew) 
			{
				JToolBarHelper::cancel(); //for cancell button
			} 
			else 
			{
		
				JToolBarHelper::cancel( 'cancel', 'Close' );
			}
			
			 
			//for dropdrawn menu use joomla code
		
	
		
			$requesturl = $uri->toString();
		

			//$this->assignRef('lists',$lists);
			$this->assignRef('addnew',$appoitment);
			$this->assignRef('variation', $variation);
			$this->assignRef('cdset',$cdset);
			$this->assignRef('request_url',$requesturl);
			parent::display($tpl);
 
		}
	
}
?>
