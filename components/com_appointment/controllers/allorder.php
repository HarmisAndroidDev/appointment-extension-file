<!DOCTYPE html>
<?php
/**
* @package  Appointment
* @copyright Copyright (C) 2009-2010 Joomlaextensions.co.in All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
**/ 
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );
jimport('joomla.filesystem.file');
$document=JFactory::getDocument();
$document->addStyleSheet("components/com_appointment/assets/css/appointment.css");
$document->addScript("components/com_appointment/assets/js/min.js");
$option = JRequest::getVar('option','','','string');
?>

<script type="text/javascript"> 
setTimeout(function() {
  jQuery('#vapbooksuccessdiv').fadeOut('fast');
}, 10000)
</script>

<?php
class allorderController extends JControllerLegacy
{
   function __construct($default=array()) 
   {
      $this->_table_prefix = '#__appointment_';
      parent::__construct($default);
   }
   
   function display($cachable=false,$urlparams='') 
   {
           
      parent::display();
      
   }
   function save() 
   {   
      $post = JRequest::get ('POST');
      $checkindate = JRequest::getVar('checkindate', array (0 ), 'request', 'string'); 
      $ser_id = JRequest::getVar('ser_id', array (0 ), 'request', 'string'); 
      $emp_id = JRequest::getVar('emp_id', array (0 ), 'request', 'string'); 
      
   }

function checkvalue() 
{
      $db = JFactory::getDbo();
      $option = JRequest::getVar('option','','request','string');
      $cid = JRequest::getVar ( 'cid', array (0 ), 'request', 'array' ); 
      $sid = JRequest::getVar ( 'sid',  'request', 'array' ); 
 
      if (! is_array ( $cid ) || count ( $cid ) < 1) 
      {
         JError::raiseError ( 500, JText::_ ( 'SELECT_AN_ITEM_TO_DELETE' ) );
      }

      $cid = implode( ',', $cid );
      $query='Select eid From '.$this->_table_prefix.'serviceemp WHERE sid='.$cid; 
      $db->setQuery($query);
      $eid = $db->loadColumn();
      $eid1 = implode(",", $eid);

      $query = 'SELECT id as value,fname as text FROM '.$this->_table_prefix.'employee WHERE id IN('.$eid1.')'; 
      $db->setQuery($query);
      $row=$db->loadObjectList();

      echo JHTML::_('select.genericlist',$row,'emp_id','class="inputbox"  size="1"  ','value','text'); 
      exit;
         
}

  function loadmonths()
    {
      $tabledata = '';
      $dbo = JFactory::getDBO();
      $option = JRequest::getVar('option','','request','string');
      $ser_id = JRequest::getVar('ser_id','','request','string');
      $emp_id = JRequest::getVar('emp_id','0','request','string');
      $cid = JRequest::getVar('cid','','request','string');
 
      $q = "select day from ".$this->_table_prefix."employeedaytime WHERE closed=0 AND eid=".$emp_id;
      $dbo->setQuery($q);
      $workday=$dbo->loadColumn();
      
      $v = "select time,minute,time1,minute1 from ".$this->_table_prefix."employeedaytime WHERE  eid=".$emp_id;
      $dbo->setQuery($v);
      $worktime=$dbo->loadObjectList();

      $z = "select price from ".$this->_table_prefix."services WHERE id=".$ser_id;
      $dbo->setQuery($z);
      $price=$dbo->loadResult();

      $w = "select name,description,duration from ".$this->_table_prefix."services WHERE id=".$ser_id;
      $dbo->setQuery($w);
      $service=$dbo->loadObject();

      $now = new DateTime('NOW');
      $year = $now->format("Y");
      $defaultdate='01-'.trim($cid).'-'.date("Y"); 
      
      $date = new DateTime($defaultdate);
      $times= $date->getTimestamp();
      
    //============ Service Information ==============

       $tabledata .='<div id="vapempblock5" class="vapempblock">';
                    $tabledata .='<div class="vapempinfoblock">';
                        $tabledata .='<div class="vap-empheader-div">';
                              $tabledata .='<div class="vapempnamediv">
                                      <span>'.$service->name.'</span>
                                      
                                 </div>';
                        $tabledata .='</div>';
                            $tabledata .='<div class="vapempdescdiv">
                                    <p>'.$service->description.'</p>       
                                </div>';
       $tabledata .='</div>';   
                          
      $tabledata .='<div class="vapempcontactdiv">';
        $tabledata .='<span class="vapempcontactsp">';
                $tabledata .='<span class="vapempserpricesp">
                   '.$price.'</span>
                        <span>$</span>
             <span class="vapempsertimesp">
                  '.$service->duration.' 
             </span>';

         $tabledata .='<span>Min.</span>';
     $tabledata .='</div>';
 $tabledata .='</div>';

//================== Service Information Over ==========



//=============== Calender start ===================
      $next=$defaultdate."next month"; 
         
       $months=array();
       $months[]=$cid;
       
      $nextmonth = date('F',strtotime($next));

       $months[]=$nextmonth;
      $nextmonth1 = date('F',strtotime('+1 month', strtotime($nextmonth)));

      $months[]=$nextmonth1;

      $next=$defaultdate."last day of this month"; 
      $nextmonth = date('Y-m-d',strtotime($next));
  
      $first=date('w', strtotime($defaultdate));
   $tabledata .= '<table>';

   $tabledata .= '<tr>';

	$today = date("d"); 

  	$nowork=0;
      foreach($months as $key=>$month)
      {
          $year = (new DateTime)->format("Y");

          //========= For Year Changing ==========

          if($month=="September")
          {
            $year=$year+1;
          }

           if($month=="October")
          {
            $year=$year+1;
          }

          if($month<"November") 
          {
             $year=$year+1;
             $defaultdate='01-'.trim($month).'-'.$year;
            
             $next=$defaultdate."last day of this month"; 
             $lastdate = date('d',strtotime($next)); 
             $curdate=date('l',strtotime(''.$month.' '.$year));
          }

          if($month=="December")
          {
            $year=$year - 1;
          }

         //============= Year Changing Close ================


         $defaultdate='01-'.trim($month).'-'.$year;

         $next=$defaultdate."last day of this month"; 
         $lastdate = date('d',strtotime($next)); 
         $curdate=date('l',strtotime(''.$month.' '.$year)); 

         $firstday = 1;
                
         if(strtoupper($curdate) == 'SUNDAY'){ $firstday = 1;}
         else if(strtoupper($curdate) == 'MONDAY'){ $firstday=2;}
         else if(strtoupper($curdate) == 'TUESDAY'){ $firstday=3;}
         else if(strtoupper($curdate) == 'WEDNESDAY'){ $firstday=4;}
         else if(strtoupper($curdate) == 'THURSDAY'){ $firstday=5;}
         else if(strtoupper($curdate) == 'FRIDAY'){ $firstday=6;}
         else if(strtoupper($curdate) == 'SATURDAY'){ $firstday=7;}
        	
         $tabledata .= '<td>';
         $tabledata .= '<table border="1" style="width: 184px; height: 170px;  margin-right: 7px;"">
                
               <tr class="monthhead">
                  <td colspan="7" align="center" >'.$month.'-'.$year.'</td>
               </tr>
               <tr>
                  <td>Sun</td>
                  <td>Mon</td>
                  <td>Tue</td>
                  <td>Wed</td>
                  <td>Thu</td>
                  <td>Fri</td>
                  <td>Sat</td>
                  
               </tr>';
               $cont=0;
               $tedate = 1;
               $null=0;
             
               $lastdate = $lastdate + $firstday;
                         
               for($i=1;$i<$lastdate;$i++)
               {  
                  $ddate = $tedate.'-'.trim($month).'-'.$year; 
                  
                  $dayName =date("l",strtotime($ddate));

                     if($cont>6)
                     {  
                        $tabledata .='<tr class="data">';
                     }  
                     if($i >= $firstday)
                     {  
                           if(in_array($dayName, $workday))
                            {  

                                 if($nowork == 0)
                                  {
                                      if($tedate < $today)
                                        {
                                            $tabledata .='<td align="center">'.$tedate.'</td>';
                                        }
                                        else
                                        {
                                          $tabledata .='<td align="center" style="background-color:rgb(2,174,54);">
                                          <a  href="javascript:void(0);"  onclick="book(\''.$dayName.'\','.$tedate.','.$year.','.$price.',\''.$month.'\','.$ser_id.')">'.$tedate.'</a></td>';
                                        }
                                  }
                                 else
                                  {
                                     $tabledata .='<td align="center" style="background-color:rgb(2,174,54);"><a  href="javascript:void(0);"  onclick="book(\''.$dayName.'\','.$tedate.','.$year.','.$price.',\''.$month.'\','.$ser_id.')">'.$tedate.'</a></td>';
                                  }
                            }
                        
                          else
                          {
                             $tabledata .='<td align="center">'.$tedate.'</td>';
                          }
                          $tedate++;  
                     }

                     else
                     {
                        $tabledata .='<td></td>';
                        
                     }

                     if($cont > 6) 
                     {  
                        '</tr>';
                        $cont = 0;
                     }  
                     $cont++;
                       
               }
               $nowork = 1;
            $tabledata .= '</table></td>';               
          
      }
      
      $tabledata .='</tr></table>';

      $tabledata.='<input type="hidden"   value="" name="day" id="dayslot">';

      $tabledata.='<input type="hidden"   value="" name="year" id="yearslot">';
      $tabledata.='<input type="hidden"   value="'.$price.'" name="price" id="priceslot">';
      $tabledata.='<input type="hidden"   value="" name="month" id="monthslot">';
      echo $tabledata;
      $_GLOBAL['timediv']=$tabledata;
      exit;
   }

//=================== Calender Over ================

function loadtime()
{
   $tabledata1 = '';
   $dbo = JFactory::getDBO();
   $option = JRequest::getVar('option','','request','string');
   $emp_id = JRequest::getVar('emp_id','','request','string');
   $ser_id = JRequest::getVar('ser_id','','request','string');
   $dayname=JRequest::getVar('dayname','','request','string');
   //$month=JRequest::getVar('month','','request','string');


   $day=JRequest::getVar('day','0','request','string');


   $v = "select time,minute,time1,minute1 from ".$this->_table_prefix."employeedaytime WHERE day='".$day."' AND eid=".$emp_id;
   $dbo->setQuery($v);
   $worktime=$dbo->loadObject();

   $p = "select duration,sleeptime from ".$this->_table_prefix."services WHERE id=".$ser_id;
   $dbo->setQuery($p);
   $slottime=$dbo->loadObject();

    $aid= "select aid from ".$this->_table_prefix."serviceoption where sid=".$ser_id; 
   $dbo->setQuery($aid);
   $appointid=$dbo->loadColumn();

   $appid=implode(',',$appointid);
 
   $var="select vname,vprice from ".$this->_table_prefix."variation where id IN('".$appid."')";
   $dbo->setQuery($var);
   $varid=$dbo->loadObjectList();

   //======= Time already Booked =======
  $done="select time,day,service,empname,month from ".$this->_table_prefix."orderbook WHERE empname=".$emp_id." AND service=".$ser_id; 
   $dbo->setQuery($done);
   $donetime=$dbo->loadObjectList();
   //echo '<pre>';print_r($donetime);exit;

   $starttime = $worktime->time. ":". $worktime->minute ; 

   $endtime=$worktime->time1. ":". $worktime->minute1 ;  
   $duration= $slottime->duration + $slottime->sleeptime; 

   $array_of_time = array ();
   $start_time    = strtotime ($starttime); //change to strtotime
   $end_time      = strtotime ($endtime); //change to strtotime

   $add_mins  = $duration * 60;
   while ($start_time <= $end_time) // loop between time
   {
      $array_of_time[] = date ("H:i", $start_time);
      $start_time += $add_mins; // to check endtie=me
   }
   $tttest = '';

   foreach ($array_of_time as $key=>$value)
   {
      for($k=0;$k<count($donetime);$k++)
      {

          if($donetime[$k]->day==$dayname )
          { 
              
              if($donetime[$k]->time==$value)
              {
                  $tttest = $value;
                    $tabledata1 .='<span id="red">'.$value.'</span>';
              }

           }

      } 
        if($tttest != $value)
        {
         $tabledata1 .='<span id="green'.$key.'" class="green"><a href="javascript:void(0);" onclick="addcart(\''.$value.'\','.$key.')" >'.$value.'</a> </span>';
      }
}
  
    $tabledata1 .='<span id="yellow">'.$endtime.'</span>';


    //=========== Variation Part===============
      $tabledata1 .='<div class="vari">';
      if($varid[0]->vname!='')
         {
              for($v=0;$v<count($varid);$v++)
                {
                        $tabledata1 .='<table>';
                        $tabledata1 .='<tr>';
                        $tabledata1 .='<td>'.$varid[$v]->vname.'</td>';
                        $tabledata1 .='<td>'.$varid[$v]->vprice.'</td>';
                $tabledata1 .='<td><input type="checkbox" id="cart" name="cart" 
        value="\''.$varid[$v]->vprice.'\'" onclick="priceadd('.$varid[$v]->vprice.',this,\''.$varid[$v]->vname.'\');"  ></td>';
           
               $tabledata1 .='</tr>';
               $tabledata1 .='</table>';
               $tabledata1.='<input type="hidden"  value="" name="time" id="newprice">';
            
               }
        }
        else
        {


        }
         $tabledata1 .='</div>';
   echo "<div class='timeslotdiv' >Select a Particular Time Slots</div>";

  $tabledata1.='<input type="hidden"  value="" name="time" id="timeslot">';

  $tabledata1 .='<div><button  class="addme" onclick="AddItemToCart();" type="button">Add Service To Cart</button>
 <a href="index.php?option=com_appointment&view=cart_detail"><button  class="bookme" onclick="BookNow();" type="button">Book Now</button></a></div>';
   echo $tabledata1;
   exit;

}
   

function addcart()
 {
      
      $tabledata1='';
      $option = JRequest::getVar('option','','request','string');
      $emp_id = JRequest::getVar('emp_id','','request','string');
      $ser_id = JRequest::getVar('ser_id','','request','string');
      $price = JRequest::getVar('price','0','request','string');
      $day = JRequest::getVar('day','0','request','string');
      $month = JRequest::getVar('month','0','request','string');
      $year = JRequest::getVar('year','0','request','string');
      $time = JRequest::getVar('time','0','request','string');
      $serid=JRequest::getVar('serid','0','request','int');
       $empid=JRequest::getVar('empid','0','request','int');
      $price1 = JRequest::getVar('price1','0','request','string');  
      $name=JRequest::getVar('name','0','request','string');
      //======= SESSION ADD TO CART ===========
    
         if(empty($_SESSION['cart']))

            $_SESSION['cart']["idx"]= 0;
         else 
            $_SESSION['cart']["idx"]= $_SESSION['cart']["idx"];
   
         $flag =0;

         for($k=0;$k<$_SESSION['cart']["idx"];$k++)
         {
            if($_SESSION['cart'][$k]['ser_id']==$ser_id)
            {
               $_SESSION['cart'][$k]['qty'] =$_SESSION['cart'][$k]['qty'] +1; 
               $flag =1;
               break;
            }
         }

         if($flag==0)
         {
            $m = $_SESSION['cart']["idx"];
            $_SESSION['cart'][$m]["ser_id"] = $ser_id;
            $_SESSION['cart'][$m]["emp_id"] = $emp_id;
            $_SESSION['cart'][$m]["price"]   = $price;
            $_SESSION['cart'][$m]["day"] = $day;
            $_SESSION['cart'][$m]["month"] = $month;
            $_SESSION['cart'][$m]["year"] = $year;
            $_SESSION['cart'][$m]["time"] = $time;
            $_SESSION['cart'][$m]["price1"] = $price1;
            $_SESSION['cart'][$m]["name"] = $name;
            $_SESSION['cart'][$m]["serid"]=$serid;
            $_SESSION['cart'][$m]["empid"]=$empid;

         }
         else
         {
            $m = $_SESSION['cart']["idx"];
            $_SESSION['cart'][$m]["ser_id"] = $ser_id;
            $_SESSION['cart'][$m]["emp_id"] = $emp_id;
            $_SESSION['cart'][$m]["price"]   = $price;
            $_SESSION['cart'][$m]["day"] = $day;
            $_SESSION['cart'][$m]["month"] = $month;
            $_SESSION['cart'][$m]["year"] = $year;
            $_SESSION['cart'][$m]["time"] = $time;
            $_SESSION['cart'][$m]["price1"] = $price1;
            $_SESSION['cart'][$m]["name"] = $name;
            $_SESSION['cart'][$m]["serid"]=$serid;
            $_SESSION['cart'][$m]["empid"]=$empid;

         }
            $_SESSION['cart'][$m]["qty"] = 1;
            $_SESSION['cart']["idx"]++;
        
         echo "<div class='vapbooksuccessdiv' id='vapbooksuccessdiv'>Service added to the cart!</div>";
     exit;
}


function book()
{
     
     $tabledata1='';
      $option = JRequest::getVar('option','','request','string');
      $emp_id = JRequest::getVar('emp_id','0','request','string');
      $ser_id = JRequest::getVar('ser_id','0','request','string');
      $price = JRequest::getVar('price','0','request','string');
      $day = JRequest::getVar('day','0','request','string');
      $month = JRequest::getVar('month','0','request','string');
      $year = JRequest::getVar('year','0','request','string');
      $time = JRequest::getVar('time','0','request','string');
      $serid=JRequest::getVar('serid','0','request','int');
       $empid=JRequest::getVar('empid','0','request','int');
      $price1 = JRequest::getVar('price1','0','request','string');  
      $name=JRequest::getVar('name','0','request','string');
      //======= SESSION ADD TO CART ===========
    
         if(empty($_SESSION['cart']))

            $_SESSION['cart']["idx"]= 0;
         else 
            $_SESSION['cart']["idx"]= $_SESSION['cart']["idx"];
   
         $flag =0;

         for($k=0;$k<$_SESSION['cart']["idx"];$k++)
         {
            if($_SESSION['cart'][$k]['ser_id']==$ser_id)
            {
               $_SESSION['cart'][$k]['qty'] =$_SESSION['cart'][$k]['qty'] +1; 
               $flag =1;
               break;
            }
         }

         if($flag==0)
         {
            $m = $_SESSION['cart']["idx"];
            $_SESSION['cart'][$m]["ser_id"] = $ser_id;
            $_SESSION['cart'][$m]["emp_id"] = $emp_id;
            $_SESSION['cart'][$m]["price"]   = $price;
            $_SESSION['cart'][$m]["day"] = $day;
            $_SESSION['cart'][$m]["month"] = $month;
            $_SESSION['cart'][$m]["year"] = $year;
            $_SESSION['cart'][$m]["time"] = $time;
            $_SESSION['cart'][$m]["price1"] = $price1;
            $_SESSION['cart'][$m]["name"] = $name;
            $_SESSION['cart'][$m]["serid"]=$serid;
            $_SESSION['cart'][$m]["empid"]=$empid;

         }
         else
         {
            $m = $_SESSION['cart']["idx"];
            $_SESSION['cart'][$m]["ser_id"] = $ser_id;
            $_SESSION['cart'][$m]["emp_id"] = $emp_id;
            $_SESSION['cart'][$m]["price"]   = $price;
            $_SESSION['cart'][$m]["day"] = $day;
            $_SESSION['cart'][$m]["month"] = $month;
            $_SESSION['cart'][$m]["year"] = $year;
            $_SESSION['cart'][$m]["time"] = $time;
            $_SESSION['cart'][$m]["price1"] = $price1;
            $_SESSION['cart'][$m]["name"] = $name;
            $_SESSION['cart'][$m]["serid"]=$serid;
            $_SESSION['cart'][$m]["empid"]=$empid;

         }
            $_SESSION['cart'][$m]["qty"] = 1;
            $_SESSION['cart']["idx"]++;
        
        
             $msg = JText::_ ('Service_added_to_the_cart');
            $link = JRoute::_('index.php?option=com_jeappointment&view=bookdetail&ser_id='.$ser_id);
            $mainframe->redirect($link,$msg);
      exit;
}


}

?>




