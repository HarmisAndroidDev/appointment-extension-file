<?php
/**
* @package Appointment
* @copyright Copyright (C) 2009-2010 Joomlaextensions.co.in All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
**/ 
defined ('_JEXEC') or die ('Restricted access');
jimport( 'joomla.application.component.controller' );
class cart_detailController extends JControllerLegacy
{
	function __construct( $default = array())
	{
		parent::__construct( $default );
		$this->_table_prefix = '#__appointment_';
	}	
	function cancel()
	{
		$this->setRedirect('index.php');
	}
	function display($cachable = false, $urlparams = '') 
	{
			parent::display();
	}

	

	function delete_product()
	{
		$post = JRequest::get('post','','','string');
		$mid = JRequest::getVar ( 'mid', '0', 'request', 'string' ); 
		//echo $mid;
		$mainframe = JFactory::getApplication();
		$temp_cart	= array();
		
		for($k=0;$k<$_SESSION['cart']['idx'];$k++)
		{
			$flag	= 0;
			if($k==$mid)
				$flag=1;
			if($flag==0)
				$temp_cart[]=$_SESSION['cart'][$k];
		}
		$_SESSION['cart']= $temp_cart;
		$_SESSION['cart']["idx"]=count($_SESSION['cart']);


		$msg = JText::_ ( 'CART_DELETE_SUCCESSFULLY');
		$link = JRoute::_('index.php?option=com_appointment&view=cart_detail');
		$mainframe->redirect($link,$msg);

	}


	function save()
	{
		$uri = JURI::getInstance();
		$url= $uri->root();

		$post = JRequest::get ( 'POST' );
	
		$mainframe = JFactory::getApplication();
		$oid=rand();

	    for($k=0;$k<$_SESSION['cart']['idx'];$k++)
	 	{
		
				$servicename=$_SESSION['cart'][$k]['ser_id'];
				$serid=$_SESSION['cart'][$k]['serid'];
				$empid=$_SESSION['cart'][$k]['empid'];

				$total=$_SESSION['totalprice'];
				$day =$_SESSION['cart'][$k]['day'];
				$time =$_SESSION['cart'][$k]['time'];
				$empname=$_SESSION['cart'][$k]['emp_id'];
				$price=$_SESSION['cart'][$k]['price'];
				$month=$_SESSION['cart'][$k]['month'];

				$db= JFactory :: getDBO();
		$sql1="INSERT INTO ".$this->_table_prefix."orderbook(`empname`,`day`,`time`,`service`,`month`) VALUES ('".$empid."','".$day."','".$time."','".$serid."','".$month."')";
				$db->setQuery($sql1);
				$db->Query();
		 }

		 $db= JFactory :: getDBO();
		 $option = JRequest::getVar('option','','request','string');
		 $cid = JRequest::getVar ( 'cid', array (0), 'POST', 'array' );
			if($post['delivery']==0)
			{
				$delstauts="Confirmed";

			}	
			else
			{
			 	$delstauts="Pending";
			}	
			$post['cid']=$cid;
			if($post['cid'][0]==0)
			{
												
				 $sql="INSERT INTO ".$this->_table_prefix."order(`oid`,`fname`,`lname`,`email`,`contact`,`totalprice`,`status`) VALUES ('".$oid."','".$post['fname']."','".$post['lname']."','".$post['email']."','".$post['phone']."','".$total."','".$delstauts."')";
							$db->setQuery($sql);
							$db->Query();	
							$orid=$db->insertid();

					 for($k=0;$k<$_SESSION['cart']['idx'];$k++)
						{
							
							$servicename=$_SESSION['cart'][$k]['ser_id'];
							$price=$_SESSION['cart'][$k]['price'];
							$vname=$_SESSION['cart'][$k]['name'];
							$vprice=$_SESSION['cart'][$k]['price1'];

							$sql="INSERT INTO ".$this->_table_prefix."orderservice(`orid`,`servicename`,`price`,`vname`,`vprice`) VALUES ('".$orid."','".$servicename."','".$price."','".$vname."','".$vprice."')";
								$db->setQuery($sql);
								$db->Query();
						}
			}
		
		if($post['delivery']==0)
		{
			$msg="Payment Successfull";
			$link = JRoute::_('index.php?option=com_appointment&view=conform_order&oid='.$oid);
			$mainframe->redirect($link,$msg);

		}	
		else
		{

			$paypal_email= 'parth@harmistechnology.com';
		 	
			$url_paypal	= 'https://www.sandbox.paypal.com/cgi-bin/webscr';
						
			$notify_url=$url.'index.php?option='.$option.'&view=cart_detail&task=conform_order&oid='.$oid;
			$return_url=$url.'index.php?option='.$option.'&view=cart_detail&task=conform_order&oid='.$oid;
			$cancel_url=$url.'index.php?option='.$option.'&view=cart_detail&task=conform_order&oid='.$oid;
			//$item_list=$this->getOrderItemList(); 
		
			echo 'Your Page are Loading....';


				
			?>

			<form name="frmpaypal" method="post" id="frmpaypal" action="<?php echo $url_paypal; ?>" >
        		<input type="hidden" name="cmd" value="_cart">
        		<input type="hidden" name="business" value="<?php echo $paypal_email; ?>">

      				<?php

		       	for($i=0;$i<$_SESSION['cart']['idx'];$i++)
				{
						
					?>

					<input type="hidden" name="item_name_<?php echo $i+1;?>"   value="<?php echo $_SESSION['cart'][$i]['ser_id']; ?>"> 
			        <input type="hidden" name="item_number_<?php echo $i+1;?>"   value="1"> <!--order id-->
			        <input type="hidden" name="amount_<?php echo $i+1;?>" value="<?php echo $_SESSION['cart'][$i]['price']; ?>"><!--product amount-->
			        <input type="hidden" name="quantity_<?php echo $i+1;?>" value="1"><!--product qty-->
					<?php	
				}
						?>
			
				<input type="hidden" name="cbt" value="Appointment">	
				<input type="hidden" name="currency_code" value="USD">
		        <input type="hidden" name="rm" value="2">
				<input type="hidden" name="upload" value="1">
		        <input type="hidden" name="return" value="<?php echo $return_url;?>" />
		        <input type="hidden" name="cancel_return" value="<?php echo $cancel_url;?>" />
		        <input type="hidden" name="notify_url" value="<?php echo $notify_url;?>" />
	    	</form>

			<script language="javascript" type="text/javascript">
				
		        document.frmpaypal.submit();
		    </script>
		   <?php
		 	

		}


	}


function conform_order()
{
		$mainframe = JFactory::getApplication();
		$post = JRequest::get ('post');
		$orid = JRequest::getVar ( 'oid', '', 'request', 'string' ); 
			
		$db = JFactory::getDBO();
		$query = "UPDATE ".$this->_table_prefix."order SET status='Confirmed' WHERE oid=".$orid; 
		$db->setQuery($query);
		$db->query();

		$msg="Payment Successfull";
		$link = JRoute::_('index.php?option=com_appointment&view=conform_order&oid='.$orid);
		$mainframe->redirect($link,$msg);
}



}	



?>