<?php

 /**

* @package Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/


defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view' );

class homeViewhome extends JViewLegacy

{

	function __construct($config = array())

	{

		 parent::__construct( $config );

	}
   

	function display($tpl = null)

	{	

		$mainframe = JFactory::getApplication();

   		$context = "";

		$document =  JFactory::getDocument();

		$document->setTitle( JText::_('Home'));

   		$document->addStyleSheet('components/com_appointment/assets/css/component.css');

   		JToolBarHelper::title(   JText::_( 'HOME_MANAGEMENT' ) );   		

   		jimport('joomla.html.pane');

   		//$pane   	= & JPane::getInstance('sliders');

 		//JToolBarHelper::addNew();

// 	 	  JToolBarHelper::editList();

//		JToolBarHelper::deleteList();		

//		JToolBarHelper::publishList();

//		JToolBarHelper::unpublishList();

	   JToolBarHelper::preferences('com_appointment', 550, 875, 'K2_PARAMETERS');

	$uri	= JFactory::getURI();	

	$this->assignRef('pane'			, $pane);

    $this->assignRef('lists',		$lists);    
   
   // $this->assignRef('request_url',	$uri->toString());    	

    	parent::display($tpl);

  }

  function quickiconButton( $link, $image, $text, $modal = 0 )
	{
		//initialise variables

		$lang 		=  JFactory::getLanguage();
?>

		<div style="float:<?php echo ($lang->isRTL()) ? 'right' : 'left'; ?>;">
		<div class="icon" style="padding-left: 16px;  padding-right: 16px;">
		<?php
			if ($modal == 1) {
				JHTML::_('behavior.modal'); 
		?>
			<a href="<?php echo $link.'/views/home/&amp;tmpl=component'; ?>" style="cursor:pointer" class="modal" rel="{handler: 'iframe', size: {x: 550, y: 400}} width: 60px; height: 60px;>
		<?php
			} 
			else 
			{
		?>
			<a href="<?php echo $link; ?>"> 
		<?php
		}
			echo JHTML::_('image','components/com_appointment/assets/icon/'.$image, $text );
		?>
			<span><?php echo $text; ?></span>
			</a>

			</div>

		</div>

		<?php

	}

}

?>

