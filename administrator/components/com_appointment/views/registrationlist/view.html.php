<?php
 /**

* @package  Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/

defined('_JEXEC') or die ('restricted access');
jimport('joomla.application.component.view');

class registrationlistViewregistrationlist extends JViewLegacy
{	
	
	function __construct( $config = array())
	{
		 parent::__construct( $config );
	}
    function display($tpl = null)

    {	
    	$context='';
        $total='';
    	$mainframe=JFactory::getApplication();
    	$document=JFactory::getDocument();
    	JToolBarHelper::title(JText::_('Registration_List'));
         jimport('joomla.html.pagination');
    	JToolBarHelper::addNew();
    	JToolBarHelper::editList();
    	JToolBarHelper::deleteList();
    	JToolBarHelper::publishList();
    	JToolBarHelper::unpublishList();
    	$filter_order=$mainframe->getUserStateFromRequest($context.'filter_order','filter_order','ordering');
    	$filter_order_Dir=$mainframe->getUserStateFromRequest($context.'filter_order_Dir','filter_order_Dir','');
    	$limitstart=$mainframe->getUserStateFromRequest($context,'limitstart','limitstart','0');
    	$limit=$mainframe->getUserStateFromRequest($context,'limit','limit','10');

    	$uri=JFactory::getURI();
    
    	$pagination = new JPagination($total,$limitstart,$limit);
    	$lists['order']=$filter_order;
    	$lists['order_Dir']=$filter_order_Dir;
        $total =  $this->get( 'Total');
    	$registrationlist=$this->get('Data');
        $publish_op = array();
        $publish_op[]= JHTML::_('select.option', '-1',JText::_('SELECT_STATE'));
        $publish_op[]= JHTML::_('select.option', '1',JText::_('PUBLISHED'));
        $publish_op[]= JHTML::_('select.option', '0', JText::_('UNPUBLISHED'));
        $published = JRequest::getVar('published', '-1','request','int');
        $searchlists['published']=JHtml::_('select.genericlist',$publish_op,'published','class="inputbox" size="1" onchange="selectsearch(this.value)"','value','text',$published);
        
     $pagination = new JPagination( $total, $limitstart, $limit);
        
        $requesturl=$uri->toString();
        $this->assignRef('registrationlist',$registrationlist);
    	$this->assignRef('requesturl',$requesturl);
    	$this->assignRef('pagination',$pagination);
    	$this->assignRef('lists',$lists);
       // $this->assignRef('searchlists', $searchlists); 
        $this->assignRef('searchlists', $searchlists);
        $this->assignRef('requesturl', $requesturl); 
    	parent::display($tpl);
    	

    }

   
    protected function getSortFields(){
		return array(
				'ordering' => JText::_('JGRID_HEADING_ORDERING'),
				'published' => JText::_('JSTATUS'),
				'name' => JText::_('JGLOBAL_TITLE')
			);
		}
	
}
?>
