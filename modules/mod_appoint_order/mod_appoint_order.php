<?php 
/**
* @package   appointment
* @copyright Copyright (C) 2009-2010 Joomlaextensions.co.in All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
**/

defined('_JEXEC') or die('Restricted access');
require_once (dirname(__FILE__).'/'.'helper.php');

$layout = $params->get('layout','default');
$document = JFactory::getDocument();
$item=mod_appoint_orderHelper::getReturnURL();

$ser_id = JRequest::getVar('ser_id',0,'','string');
$emp_id = JRequest::getVar('emp_id',0,'','string');

$ser = mod_appoint_orderHelper::getser('ser');
   			  $temp=array();
			$temp[]=JHTML::_('select.option','0',JText::_('SELECT_SERVICES'));
			$ser=@array_merge($temp,$ser);
		
$lists['ser'] = JHTML::_('select.genericlist',$ser,'ser_id','class="inputbox" onchange="change()"  size="1"  ','value','text',$ser_id); 

$emp = mod_appoint_orderHelper::getemp('emp');
		  $temp=array();
			$temp[]=JHTML::_('select.option','0',JText::_('SELECT_EMPLOYEE'));
			$emp=@array_merge($temp,$emp);
		
			$lists['emp'] = JHTML::_('select.genericlist',$emp,'emp_id','class="inputbox"   size="1"  ','value','text',$emp_id); 


require_once (dirname(__FILE__).'/'.'helper.php');

	JHtml::_('script', JURI::root().'components'.'/'.'com_appointment'.'/'.'assets'.'/'.'js'.'/'.'min.js');

$path = JModuleHelper::getLayoutPath('mod_appoint_order', $layout);
if (file_exists($path)) {
	require($path);
}
?>


