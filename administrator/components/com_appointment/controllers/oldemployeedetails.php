<?php
 /**

* @package    Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );
jimport('joomla.filesystem.file');
class employeedetailsController extends JControllerLegacy
{
	function __construct( $default = array())
	{
		parent::__construct( $default );
		$this->_table_prefix = '#__appointment_';
	}	
	
	function display($cachable = false, $urlparams = '')
	{

		parent::display();
	}
	
	function edit() {
		JRequest::setVar ( 'view', 'employeedetails' );
		JRequest::setVar ( 'layout', 'default' );
		JRequest::setVar ( 'hidemainmenu', 1 );
		parent::display ();
	
	}

	function save() 
	{

		$post = JRequest::get ( 'POST' );
		//	echo '<pre>';print_r($post);exit;
		$option = JRequest::getVar('option','','request','string');
		$user	= clone(JFactory::getUser());
		$adminid= $user->id;
		
		$post['adminid']=$adminid;
		$post["description"] = JRequest::getVar( 'description', '', 'POST', 'string', JREQUEST_ALLOWRAW );
			
		 $cid = JRequest::getVar ('cid', array(0), 'POST', 'array' );

	  	$post ['id'] = $cid [0];
	 	
			  	
		$db	=JFactory::getDBO();
		$model = $this->getModel ( 'employeedetails' );
		$file 	= JRequest::getVar ( 'img', '', 'files', 'array' );	
		
	   if($file['name']!='')
	   { 		

	 	  	$filetype = strtolower(JFile::getExt($file['name']));	
	  	
			if($filetype=='jpg' || $filetype=='jpeg' || $filetype=='gif' || $filetype=='png' || $filetype='bmp' || $filetype=='tif')
			{
			$post['img']= JPath::clean(time().'_'.$file['name']);
		                       	
            $src	= $file['tmp_name'];
			$dest 	= JPATH_ROOT.'/'.'components/com_appointment/assets/emp/images/'.$post['img'];
			JFile::upload($src,$dest);	
			$dest1 	= JPATH_ROOT.'/'.'components/com_appointment/assets/emp/images/thumb_'.$post['img'];
			copy($dest,$dest1);
			$img 	= new Thumbnail();

			$thumboption 	= array('type'   => $filetype,'width' => "50",'height'  => "50",'method'  => THUMBNAIL_METHOD_SCALE_MAX);
			Thumbnail::CreatThumb($dest, $dest1, $thumboption);

			if($post['oldphoto'])
			{
				$dest 	= JPATH_ROOT.'/'.'components/com_appointment/assets/emp/images/'.$post["oldphoto"];
				unlink($dest);
				$dest12 = JPATH_ROOT.'/'.'components/com_appointment/assets/emp/images/thumb_'.$post["oldphoto"];
				unlink($dest12);
			}		
			if($file["name"][$id]=="")
				$pname 	= $post['oldphoto'][$id];
			else
				$pname	= $file["name"][$id];

			}
		
		} 
		else
		{
			$post['img']= $post['oldphoto'];
		}
		
		$insertid=$model->store ($post,'employee' );
		$post["cid"]=$insertid;
		
		if ($insertid>0)
		 {
			
			if($cid[0]==0)
			{	
			
			$sql = "SELECT max(ordering) As ordering FROM ".$this->_table_prefix."employee ";
			$db->setQuery($sql);
			$max  = $db->loadObject();
			if($max->ordering)
					$order = $max->ordering + 1;
			else
			$order = 1;
			$query = "UPDATE ".$this->_table_prefix."employee SET ordering = ".$order." WHERE fname = '".$post['fname']."'";
			$db->setQuery($query);
			$db->query();
		}
			$msg = JText::_ ( 'DETAIL_SAVED' );
		} else {
			$msg = JText::_ ( 'ERROR_SAVING_DETAIL' );
		}
		
		// Code for employeedatetime table
	if($post["cid"]>0)
			{	
			 
			$sql = "DELETE FROM ".$this->_table_prefix."employeedaytime WHERE eid=".$post["cid"]; 
			$db->setQuery($sql);
			$db->query();

			}
		
			
		for($i=0;$i<count($post["selectday"]);$i++)
			{	
					if($post["closed"][$i]==1)	
					{	
						$data=array();
						$data["eid"]=$post["cid"];
						$data["day"]=$post["selectday"][$i];
						$data["closed"]=$post["closed"][$i];

						
					}
					else
					{	
						$data=array();
						$data["eid"]=$post["cid"];
						$data["day"]=$post["selectday"][$i];
						$data["time"]=$post["time"][$i];

						$data["minute"]=$post["minute"][$i];

						$data["time1"]=$post["time1"][$i];
						$data["minute1"]=$post["minute1"][$i];
						
				}

				$insertid=$model->store($data,'employeedaytime');
				// if(!empty($data['day']) && !empty($data['time']) && !empty($data['minute']) && !empty($data['time1']) && !empty($data['minute1']))
				// {
				// //	echo '<pre>asd';print_r($data);exit;
			
				// 	$model->store( $data,'employeedaytime');

				// }
				
				
			}
		
		
		$this->setRedirect ( 'index.php?option=' . $option . '&view=employeelist', $msg );
		
	}
	 function remove() 
	 {
		$post = JRequest::get ( 'POST' );
			$option = JRequest::getVar('option','','request','string');
		
	 			$cid = JRequest::getVar ( 'cid', array (0 ), 'post', 'array' );
		
		if (! is_array ( $cid ) || count ( $cid ) < 1) 
		{
			JError::raiseError ( 500, JText::_ ( 'SELECT_AN_ITEM_TO_DELETE' ) );
		}
		
		$model = $this->getModel ( 'employeedetails' );
		if (! $model->delete( $cid )) 
		{
			echo "<script> alert('" . $model->getError ( true ) . "'); window.history.go(-1); </script>\n";
		}
		$msg = JText::_ ( 'DETAIL_DELETED_SUCCESSFULLY' );
		$this->setRedirect ( 'index.php?option='.$option.'&view=employeelist',$msg );
	}
	function publish() 
	{
				
		$option = JRequest::getVar('option','','request','string');
		
		 $cid = JRequest::getVar ( 'cid', array (0 ), 'POST', 'array' );
				 
		if (! is_array ( $cid ) || count ( $cid ) < 1) 
		{
			JError::raiseError ( 500, JText::_ ( 'SELECT_AN_ITEM_TO_PUBLISH' ) );
		}
		
		$model = $this->getModel ( 'employeedetails' );
		if (! $model->publish ( $cid, 1 ))
		 {
			echo "<script> alert('" . $model->getError ( true ) . "'); window.history.go(-1); </script>\n";
		 }
		$msg = JText::_ ( 'DETAIL_PUBLISHED_SUCCESSFULLY' );
		$this->setRedirect ( 'index.php?option='.$option.'&view=employeelist',$msg );
	}
	
	function unpublish() {
		
		$option = JRequest::getVar('option','','request','string');
		
		$cid = JRequest::getVar ( 'cid', array (0 ), 'POST', 'array' );
		
		if (! is_array ( $cid ) || count ( $cid ) < 1) {
			JError::raiseError ( 500, JText::_ ( 'SELECT_AN_ITEM_TO_UNPUBLISH' ) );
		}
		
		$model = $this->getModel ( 'employeedetails' );
		if (! $model->publish ( $cid, 0 )) {
			echo "<script> alert('" . $model->getError ( true ) . "'); window.history.go(-1); </script>\n";
		}
		$msg = JText::_ ( 'DETAIL_UNPUBLISHED_SUCCESSFULLY' );
		$this->setRedirect ( 'index.php?option='.$option.'&view=employeelist',$msg );
	}
	
	
	function apply() 
	{
		
		$post = JRequest::get ( 'POST' );
				
		$option = JRequest::getVar('option','','request','string');
		$user	= clone(JFactory::getUser());
		$adminid= $user->id;
		
		$post['adminid']=$adminid;
		$post["description"] = JRequest::getVar( 'description', '', 'POST', 'string', JREQUEST_ALLOWRAW );
			
		 $cid = JRequest::getVar ('cid', array(0), 'POST', 'array' );

	  	$post ['id'] = $cid [0];
	 	
		$db	=JFactory::getDBO();
		$model = $this->getModel ( 'employeedetails' );
		$file 	= JRequest::getVar ( 'img', '', 'files', 'array' );	
		
	   if($file['name']!='')
	   { 		

	 	  	$filetype = strtolower(JFile::getExt($file['name']));	
	  	
			if($filetype=='jpg' || $filetype=='jpeg' || $filetype=='gif' || $filetype=='png' || $filetype='bmp' || $filetype=='tif')
			{
			$post['img']= JPath::clean(time().'_'.$file['name']);
		                       	
            $src	= $file['tmp_name'];
			$dest 	= JPATH_ROOT.'/'.'components/com_appointment/assets/emp/images/'.$post['img'];
			JFile::upload($src,$dest);	
			$dest1 	= JPATH_ROOT.'/'.'components/com_appointment/assets/emp/images/thumb_'.$post['img'];
			copy($dest,$dest1);
			$img 	= new Thumbnail();

			$thumboption 	= array('type'   => $filetype,'width' => "50",'height'  => "50",'method'  => THUMBNAIL_METHOD_SCALE_MAX);
			Thumbnail::CreatThumb($dest, $dest1, $thumboption);

			if($post['oldphoto'])
			{
				$dest 	= JPATH_ROOT.'/'.'components/com_appointment/assets/emp/images/'.$post["oldphoto"];
				unlink($dest);
				$dest12 = JPATH_ROOT.'/'.'components/com_appointment/assets/emp/images/thumb_'.$post["oldphoto"];
				unlink($dest12);
			}		
			if($file["name"][$id]=="")
				$pname 	= $post['oldphoto'][$id];
			else
				$pname	= $file["name"][$id];
			}
		
		} 
		else
		{
			$post['img']= $post['oldphoto'];
		}
		
		$insertid=$model->store ($post , 'employee');
		$post["cid"]=$insertid;
		

		if ($insertid>0)
		 {
			
			if($cid[0]==0)
			{	
			
			$sql = "SELECT max(ordering) As ordering FROM ".$this->_table_prefix."employee ";
			$db->setQuery($sql);
			$max  = $db->loadObject();
			if($max->ordering)
					$order = $max->ordering + 1;
			else
			$order = 1;
			$query = "UPDATE ".$this->_table_prefix."employee SET ordering = ".$order." WHERE fname = '".$post['fname']."'";
			$db->setQuery($query);
			$db->query();
			}
			$msg = JText::_ ( 'DETAIL_SAVED' );
		} else {
			$msg = JText::_ ( 'ERROR_SAVING_DETAIL' );
		}
		
		// Code for employeedatetime table
	if($post["cid"]>0)
			{	
			 
			$sql = "DELETE FROM ".$this->_table_prefix."employeedaytime WHERE eid=".$post["cid"]; 
			$db->setQuery($sql);
			$db->query();

			}
			for($i=0;$i<count($post['selectday']);$i++)
			{	
			if($post["closed"][$i]==1)
			{	
				$data=array();

				$data['eid']=$insertid;

				if($post['daytimeid'][$i]>0)
				{
					$data['id']=$post['daytimeid'][$i];
				}
				else
				$data['id']="0";
				$data['day']=$post['selectday'][$i];
				//echo '<pre>';print_r($data['day']);exit;
				$data['closed']=1;
	
				if($data['day']!='')
				{ 
					//echo "id:".$data['id'];
				$model->store($data,'employeedaytime');
				}
			}
			else
			{	

				$data=array();

				$data['eid']=$insertid;

				if($post['daytimeid'][$i]>0)
				{
					$data['id']=$post['daytimeid'][$i];
				}
				else
				$data['id']="0";
				$data['day']=$post['selectday'][$i];

				$data['time']=$post['time'][$i];
				$data['minute']=$post['minute'][$i];
				$data['time1']=$post['time1'][$i];
				$data['minute1']=$post['minute1'][$i];
					
				if(!empty($data['day']) && !empty($data['time']) && !empty($data['minute']) && !empty($data['time1']) && !empty($data['minute1']))
				{
				//	echo '<pre>asd';print_r($data);exit;
			
					$insertid=$model->store($data,'employeedaytime');

				}
				
			}
			

		}
			
		
		// for($i=0;$i<count($post["selectday"]);$i++)
		// 	{	
						
		// 		$data=array();
		// 		$data["eid"]=$post["cid"];
		// 		$data["day"]=$post["selectday"][$i];
		// 		$data["time"]=$post["time"][$i];
		// 		$data["minute"]=$post["minute"][$i];
		// 		$data["time1"]=$post["time1"][$i];
		// 		$data["minute1"]=$post["minute1"][$i];
		// 		$insertid=$model->store( $data,'employeedaytime');

		// 		// //$model->store( $data,'employeedaytime');
		// 		// if(!empty($data['day']) && !empty($data['time']) && !empty($data['minute']) && !empty($data['time1']) && !empty($data['minute1']))
		// 		// {
		// 		// 	//echo '<pre>asd';print_r($data);exit;
			
		// 		// $insertid=$model->store( $data,'employeedaytime');

		// 		// }
		// 	}	
				
			
				
		$this->setRedirect ('index.php?option='.$option.'&view=employeedetails&task=edit&cid[]='.$insertid, $msg );
		
	}
	
	function cancel() {
		
		$option = JRequest::getVar('option','com_appointment','request','string');
		$msg = JText::_ ( 'DETAIL_EDITING_CANCELLED' );
		$this->setRedirect ( 'index.php?option='.$option.'&view=employeelist',$msg );
	}
		
	function removeCdset() {

			//alert("dfhg");
		$db = JFactory::getDbo();
		$option = JRequest::getVar('option','','request','string');
		
		 $cid = JRequest::getVar ( 'cid', array (0 ), 'request', 'array' ); 
		
		//echo "<pre>";print_r($cid);exit;
		if (! is_array ( $cid ) || count ( $cid ) < 1) {
			JError::raiseError ( 500, JText::_ ( 'SELECT_AN_ITEM_TO_DELETE' ) );
		}
		
		$cid = implode( ',', $cid );
		  $query = 'DELETE FROM '.$this->_table_prefix.'employeedaytime WHERE id IN ( '.$cid.' )'; 
		 $db->setQuery($query);
			if(!$db->query()) {
			$this->setError($db->getErrorMsg());

				}
	
		exit;
		
	
		
	}
	
	 
}
?>
