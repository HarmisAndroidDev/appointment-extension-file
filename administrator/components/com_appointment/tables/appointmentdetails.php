<?php
 /**

* @package Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.model');

class Tableappointmentdetails extends JTable
{
	var $id = null;
	var $name = null;
	var $description = null;
	var $price=null;
	var $img = null;
	
	
 	var $published = null;
 	function Tableappointmentdetails($db)
 	 {
 	 	 $this->_table_prefix = '#__appointment_';
 	 	 parent::__construct($this->_table_prefix.'addnew','id',$db);
 	 }
 	 function bind($array, $ignore = '')
	{
		if (key_exists( 'params', $array ) && is_array( $array['params'] )) {
			$registry = new JRegistry();
			$registry->loadArray($array['params']);
			$array['params'] = $registry->toString();
		}

		return parent::bind($array, $ignore);
	}

}

?>
