<?php
 /**

* @package Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.model');

class allorderModelallorder extends JModelLegacy
{
	var $_data = null;
	var $_total = null;
	var $_pagination = null;
	var $_table_prefix = null;
                      	
	function __construct()
	{
		parent::__construct();
		global $context;
		$mainframe	= JFactory::getApplication();
		$this->_table_prefix = '#__appointment_';
		$array = JRequest::getVar('id',  0, '', 'array');		
		$this->setId((int)$array[0]);
	//$limit	= $mainframe->getUserStateFromRequest( $context.'limit', 'limit', $mainframe->getCfg('list_limit'), 0);
	//$limitstart = $mainframe->getUserStateFromRequest( $context.'limitstart', 'limitstart', 0 );
	//$this->setState('limit', $limit);
	//$this->setState('limitstart', $limitstart);
	}
  function setId($id)
	{	
	     $this->_id= $id;
	     $this->_data= null;
	 }
	
	function getData()
	{	
		if (empty($this->_data))
		{
			$mainframe  = JFactory::getApplication();
			
			$query = $this->_buildQuery();
			$this->_data = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
		}
		return $this->_data;
	}
	
	function _buildQuery()
	{

		// $serid= JRequest::getVar('ser_id',  0, '', 'int');
		// $emp_id=JRequest::getVar('emp_id',  0, '', 'int');


		$db = JFactory::getDBO();
		$mainframe    = JFactory::getApplication();	
		$user =  clone(JFactory::getUser());

		
		 $id = JRequest::getVar('cid',  0, '', 'int');
		
	 $query='select * from '.$this->_table_prefix.'services';

			  // $query='SELECT emp.*,s.*,se.* FROM
			  // '.$this->_table_prefix.'services AS s 
			  // INNER JOIN '.$this->_table_prefix.'serviceemp AS se ON s.id =se.sid
			  // INNER JOIN '.$this->_table_prefix.'employee AS emp ON se.eid=emp.id where emp.id='.$id; 
		 
		
		return $query;
	}

	function getservices($id=0)
	{
		 $query = 'SELECT name FROM '.$this->_table_prefix.'services WHERE id='.$id;
			$this->_db->setQuery($query);
			$this->_data = $this->_db->loadResult();
			return $this->_data;
	}
	
	function getCdset2()
	{
		
			 $query = 'SELECT * FROM '.$this->_table_prefix.'serviceemp WHERE sid='.$this->_id;
			$this->_db->setQuery($query);
			$this->_data = $this->_db->loadObjectList();
			return $this->_data;
	}

function getser()
		{
			$query = 'SELECT id as value,name as text FROM '.$this->_table_prefix.'services';
			$this->_db->setQuery( $query );
			return $this->_db->loadObjectlist();
		}

	function getCdset()
	{

			 $query = 'SELECT * FROM '.$this->_table_prefix.'serviceemp WHERE sid='.$this->_id;
			$this->_db->setQuery($query);
			$this->_data = $this->_db->loadObjectList();
			return $this->_data;
	}


	function getemployee($id=0)
	{
	
		/* $query='SELECT employee.fname, serviceemp.sid FROM employee INNER JOIN serviceemp ON employee.id=serviceemp.sid
		ORDER BY employee.fname';		*/			
			$query = 'SELECT fname FROM '.$this->_table_prefix.'employee WHERE sid='.$id;
			$this->_db->setQuery($query);
			$this->_data = $this->_db->loadResult();
			return $this->_data;
	}

	function getemp()
		{
			$query = 'SELECT id as value,fname as text FROM '.$this->_table_prefix.'employee';
			$this->_db->setQuery( $query );
			return $this->_db->loadObjectlist();
		}


	function getworkday()
		{
			$dbo = JFactory::getDBO();	
			 $emp_id = JRequest::getVar('emp_id','0','request','string');
			$q = "select day from #__appointment_employeedaytime WHERE eid=".$emp_id;
			$dbo->setQuery($q);
			return $this->_db->loadColumn();
		}



	function getworktime()
		{
			$dbo = JFactory::getDBO();	
			$emp_id = JRequest::getVar('emp_id','0','request','string');
			$v = "select time,minute,time1,minute1 from #__appointment_employeedaytime WHERE  eid=".$emp_id  ;
			$dbo->setQuery($v);
			return $this->_db->loadObjectList();

		}

	function getemployeeName($id=0)
	{
	
		/* $query='SELECT employee.fname, serviceemp.sid FROM employee INNER JOIN serviceemp ON employee.id=serviceemp.sid
		ORDER BY employee.fname';		*/			
			$query = 'SELECT fname FROM '.$this->_table_prefix.'employee WHERE id='.$id;
			$this->_db->setQuery($query);
			$this->_data = $this->_db->loadResult();
			return $this->_data;
	}

	// function getprice()
	// 	{

	// 		$dbo = JFactory::getDBO();	

	// 		$ser_id = JRequest::getVar('ser_id','','request','string');
	// 		if($ser_id !='')
	// 		{
	// 			$z = "select price from #__appointment_services WHERE id=".$ser_id;
	// 			$dbo->setQuery($z);
	// 			return $this->_db->loadResult();
	// 		}
	// 	}


		function getsevicedetail()
		{
			$dbo = JFactory::getDBO();	
			$emp_id = JRequest::getVar('emp_id','','request','string');
			// $query = 'SELECT id as value,fname as text FROM '.$this->_table_prefix.'employee';

			if($emp_id!='')
			{

			// echo $emp_id; exit;
			$query='SELECT s.id as value,s.name as text FROM
			  '.$this->_table_prefix.'services AS s 
			  INNER JOIN '.$this->_table_prefix.'serviceemp AS se ON s.id =se.sid
			  INNER JOIN '.$this->_table_prefix.'employee AS emp ON se.eid=emp.id where emp.id='.$emp_id; 

			$this->_db->setQuery( $query );
			return $this->_db->loadObjectlist();
		}
		else
		{
			$emp_id=0;
				$query='SELECT s.id as value,s.name as text FROM
			  '.$this->_table_prefix.'services AS s 
			  INNER JOIN '.$this->_table_prefix.'serviceemp AS se ON s.id =se.sid
			  INNER JOIN '.$this->_table_prefix.'employee AS emp ON se.eid=emp.id where emp.id='.$emp_id; 

			$this->_db->setQuery( $query );
			return $this->_db->loadObjectlist();
		}
		}
	



	
	
}
   