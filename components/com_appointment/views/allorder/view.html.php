<?php

 /**

* @package  Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/
defined( '_JEXEC' ) or die( 'Restricted access' );


jimport( 'joomla.application.component.view' );
 
class allorderViewallorder extends JViewLegacy
{
	function __construct($config = array())
	{
		 parent::__construct( $config );
	}
    
	function display($tpl = null)
	{
		
		global $mainframe, $context;
// ========================= Page title code ================================== //	
		$mainframe 	= JFactory::getApplication();
		$document 	=  JFactory::getDocument();
		$pagetitle 	= $document->getTitle();
		if($mainframe->getCfg( 'sitename_pagetitles' )==2) // After page title
			$document->setTitle($pagetitle.' - '.$mainframe->getCfg( 'sitename' ) );
		else if($mainframe->getCfg( 'sitename_pagetitles' )==1) // Before page title
			$document->setTitle($mainframe->getCfg( 'sitename' ).' - '.$pagetitle );
		else // no page title
			$document->setTitle($pagetitle );
// ======================= EOF Page title code ================================ //	
		
			$document =  JFactory::getDocument();
			$document->setTitle( JText::_('ALL_ORDER') );
   		$option	= JRequest::getVar('option', 'com_appointment','request','string');
		 $empid	= JRequest::getVar('emp_id', '0','request','string');
		 $serid	= JRequest::getVar('ser_id', '0','request','string');
		 
   		$uri	= JFactory::getURI();
		
		$appointment=  $this->get('Data');

		$db = jFactory::getDBO();
		 $lists = array();	
		$detail	= $this->get('data'); 
		// echo "<pre>";
		// print_r($detail); exit;
		$ser= $this->get('ser');
		$workday= $this->get('workday');
		$worktime= $this->get('worktime');
		// $price= $this->get('price');
		
		$sevicedetail= $this->get('sevicedetail');

	
		//for dropdrawn menu use joomla code
		
		$model=$this->getModel('allorder');
	
		$empname=$model->getemployeeName($empid);
		$lists['empname']=$empname;

		$sername=$model->getservices($serid);
		$lists['sername']=$sername;


		$now = new DateTime('NOW');
        $year = $now->format("F");


     	 
			$month[] = JHtml::_('select.option', '0', JText::_('Select_Months')); 
			$month[] = JHtml::_('select.option', 'January', JText::_('January')); 
			$month[] = JHtml::_('select.option', 'February', JText::_('February'));
			$month[] = JHtml::_('select.option', 'March', JText::_('March'));
			$month[] = JHtml::_('select.option', 'April', JText::_('April'));
			$month[] = JHtml::_('select.option', 'May', JText::_('May'));
			$month[] = JHtml::_('select.option', 'June', JText::_('June')); 
			$month[] = JHtml::_('select.option', 'July', JText::_('July'));
			$month[] = JHtml::_('select.option', 'August', JText::_('August'));
			$month[] = JHtml::_('select.option', 'September', JText::_('September'));
			$month[] = JHtml::_('select.option', 'October', JText::_('October'));
			$month[] = JHtml::_('select.option', 'November', JText::_('November'));
			$month[] = JHtml::_('select.option', 'December', JText::_('December'));



		$lists['month_list'] = JHTML::_('select.genericlist',$month, 'month[]', 'class="inputtext" onchange=mymonth()', 'value', 'text',$year);
		$requesturl = $uri->toString();

		$category = array();
		$category[0]->value= '0';
		$category[0]->text=JText::_('Services');
		$sevicedetail=@array_merge($category,$sevicedetail);
		$lists['category'] 	= JHTML::_('select.genericlist',$sevicedetail,  'sevicedetail[]', 'class="inputtext" onchange=mysevicedetail(value) ', 'value', 'text',$sevicedetail);
       
       if($sevicedetail)
       {

       	$this->assignRef('firstservice',$sevicedetail[1]->value);
       }     	
		$this->assignRef('lists',$lists);
  		$this->assignRef('appointment',$appointment); 	
  		$this->assignRef('workday',$workday); 	
  		$this->assignRef('worktime',$worktime); 		
  		$this->assignRef('price',$price); 	
        $this->assignRef('requesturl', $requesturl);	
    	parent::display($tpl);
  }
}
?>















