<?php
 /**

* @package Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/

defined('_JEXEC') or die ('restricted access');
jimport('joomla.application.component.view');
class employeedetailsViewemployeedetails extends JViewLegacy{	
		function display($tpl = null)
		{
			$document =  JFactory::getDocument();
			$document->setTitle( JText::_('EMPLOYEEDETAILS') );
			$uri = JFactory::getURI();
			
			$option = JRequest::getVar('option','','request','string');
			$db = jFactory::getDBO();
			$detail	= $this->get('data'); 
			$employeelist= $this->get('data');
			$groups= $this->get('groups');
			
			$model=$this->getModel('employeedetails');
			$employeedaytime	= $model->getemployeedaytime($employee->id); 
			$cdset=$this->get('data');
			
			$isNew = ($employee->id < 1);
			$text = $isNew ? JText::_( 'NEW' ) : JText::_( 'EDIT' );
			JToolBarHelper::title(JText::_( 'EMPLOYEE_DETAILS' ).': <small><small>[ ' . $text.' ]</small></small>');
			JToolBarHelper::apply(); //for save button
			JToolBarHelper::save(); //for save & close bttun
			if ($isNew) 
			{
				JToolBarHelper::cancel(); //for cancell button
			} 
			else 
			{
		
				JToolBarHelper::cancel( 'cancel', 'Close' );
			}
			
			$lists = array();	
			$selectday[] = JHtml::_('select.option', '0', JText::_('Select Day')); 
			$selectday[] = JHtml::_('select.option', 'Sunday', JText::_('Sunday')); 
			$selectday[] = JHtml::_('select.option', 'Monday', JText::_('Monday'));
			$selectday[] = JHtml::_('select.option', 'Tuesday', JText::_('Tuesday'));
			$selectday[] = JHtml::_('select.option', 'Wednesday', JText::_('Wednesday'));
			$selectday[] = JHtml::_('select.option', 'Thursday', JText::_('Thursday'));
			$selectday[] = JHtml::_('select.option', 'Friday', JText::_('Friday')); 
			$selectday[] = JHtml::_('select.option', 'Saturday', JText::_('Saturday'));
			$lists['selectday_list'] = JHTML::_('select.genericlist',$selectday, 'selectday[]', 'class="inputtext" onchange=myfunction()', 'value', 'text',$employeelist->selectday);
			$requesturl = $uri->toString();
			//$lists['published'] = JHTML::_('select.booleanlist',  'published', 'class="inputbox"', $employeelist->published );

			//$lists = array();	
			$time[] = JHtml::_('select.option', '00', JText::_('Select Time'));
			$time[] = JHtml::_('select.option', '08 AM', JText::_('08 AM')); 
			$time[] = JHtml::_('select.option', '09 AM', JText::_('09 AM'));
			$time[] = JHtml::_('select.option', '10 AM', JText::_('10 AM'));
			$time[] = JHtml::_('select.option', '11 AM', JText::_('11 AM'));
			$time[] = JHtml::_('select.option', '12 PM', JText::_('12 PM'));
			$time[] = JHtml::_('select.option', '13 PM', JText::_('13 PM')); 
			$time[] = JHtml::_('select.option', '14 PM', JText::_('14 PM'));
			$time[] = JHtml::_('select.option', '15 PM', JText::_('15 PM'));
			$time[] = JHtml::_('select.option', '16 PM', JText::_('16 PM'));
			$time[] = JHtml::_('select.option', '17 PM', JText::_('17 PM'));
			$lists['time_list'] = JHTML::_('select.genericlist',$time, 'time', 'class="inputtext" ', 'value', 'text',$employeelist->time);
			$requesturl = $uri->toString();
			

			$minute[] = JHtml::_('select.option', '00', JText::_('Select Minute'));
			$minute[] = JHtml::_('select.option', '0', JText::_('00')); 
			$minute[] = JHtml::_('select.option', '05', JText::_('05'));
			$minute[] = JHtml::_('select.option', '10', JText::_('10'));
			$minute[] = JHtml::_('select.option', '15', JText::_('15'));
			$minute[] = JHtml::_('select.option', '20', JText::_('20'));
			$minute[] = JHtml::_('select.option', '25', JText::_('25')); 
			$minute[] = JHtml::_('select.option', '30', JText::_('30'));
			$minute[] = JHtml::_('select.option', '35', JText::_('35'));
			$minute[] = JHtml::_('select.option', '40', JText::_('40'));
			$minute[] = JHtml::_('select.option', '45', JText::_('45'));
			$minute[] = JHtml::_('select.option', '50', JText::_('50'));
			$minute[] = JHtml::_('select.option', '55', JText::_('55'));
			$lists['minute_list'] = JHTML::_('select.genericlist',$minute, 'minute', 'class="inputtext" ', 'value', 'text',$employeelist->minute);
			
			$requesturl = $uri->toString();
			$time1[] = JHtml::_('select.option', '00', JText::_('Select Time'));
			$time1[] = JHtml::_('select.option', '08 AM', JText::_('08 AM')); 
			$time1[] = JHtml::_('select.option', '09 AM', JText::_('09 AM'));
			$time1[] = JHtml::_('select.option', '10 AM', JText::_('10 AM'));
			$time1[] = JHtml::_('select.option', '11 AM', JText::_('11 AM'));
			$time1[] = JHtml::_('select.option', '12 PM', JText::_('12 PM'));
			$time1[] = JHtml::_('select.option', '13 PM', JText::_('13 PM')); 
			$time1[] = JHtml::_('select.option', '14 PM', JText::_('14 PM'));
			$time1[] = JHtml::_('select.option', '15 PM', JText::_('15 PM'));
			$time1[] = JHtml::_('select.option', '16 PM', JText::_('16 PM'));
			$time1[] = JHtml::_('select.option', '17 PM', JText::_('17 PM'));
			$lists['time1_list'] = JHTML::_('select.genericlist',$time1, 'time1', 'class="inputtext" ', 'value', 'text',$employeelist->time1);
			$requesturl = $uri->toString();
			

			$minute1[] = JHtml::_('select.option', '00', JText::_('Select Minute'));
			$minute1[] = JHtml::_('select.option', '00', JText::_('00')); 
			$minute1[] = JHtml::_('select.option', '05', JText::_('05'));
			$minute1[] = JHtml::_('select.option', '10', JText::_('10'));
			$minute1[] = JHtml::_('select.option', '15', JText::_('15'));
			$minute1[] = JHtml::_('select.option', '20', JText::_('20'));
			$minute1[] = JHtml::_('select.option', '25', JText::_('25')); 
			$minute1[] = JHtml::_('select.option', '30', JText::_('30'));
			$minute1[] = JHtml::_('select.option', '35', JText::_('35'));
			$minute1[] = JHtml::_('select.option', '40', JText::_('40'));
			$minute1[] = JHtml::_('select.option', '45', JText::_('45'));
			$minute1[] = JHtml::_('select.option', '50', JText::_('50'));
			$minute1[] = JHtml::_('select.option', '55', JText::_('55'));
			$lists['minute1_list'] = JHTML::_('select.genericlist',$minute1, 'minute1', 'class="inputtext" ', 'value', 'text',$employeelist->minute1);
			$requesturl = $uri->toString();


			$lists['published'] = JHTML::_('select.booleanlist',  'published', 'class="inputbox"', $employeelist->published );
			

			//$this->assignRef('lists',$lists);
			$this->assignRef('employeelist',$employeelist);
			$this->assignRef('employeedaytime',$employeedaytime);
			$this->assignRef('cdset',$cdset);
			$this->assignRef('lists',$lists);
			$this->assignRef('detail',$detail);
			//$this->assignRef('cdset',$cdset);
			$this->assignRef('request_url',$requesturl);
			parent::display($tpl);
 
		}
	
}
?>
