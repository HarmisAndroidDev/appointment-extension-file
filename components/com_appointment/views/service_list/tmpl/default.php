<?php 
 /**

* @package Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/

defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.tooltip');
JHTML::_('behavior.calendar');
JHtml::_('behavior.formvalidator');
JHtml::_('behavior.formvalidation');
JHTMLBehavior::modal();
$uri =JURI::getInstance();
$url= $uri->root();
$model = $this->getModel('service_list'); 
$option = JRequest::getVar('option','','request','string');

$img_path	= $url."components/".$option."/assets/images/";
$user 		= clone(JFactory::getUser());
$app = JFactory::getApplication();
$document 	= JFactory::getDocument();
$document->addStyleSheet('components/'.$option.'/assets/css/style.css');
$staffname = JRequest::getVar('name','','request','');
$path=$url.'/'."components/".$option.'/assets/service/images/thumb_';
/*echo "<pre>";
print_r($this->grp_list); 
echo "</pre>";
*/
?>


<div class="componetntdiv ">
	<div class="ourservice"><label><?php echo JText::_('OUR_SERVICE'); ?></label></div>
	
	<form action="<?php echo 'index.php?option='.$option.'&view=service_list&limitstart=0'; ?>" method="post" name="adminForm" enctype="multipart/form-data" class="required validate-email form-validate">
	<div class="service_list_main_div">

		<?php 

			for($i=0; $i<count($this->grp_list); $i++)
			{
								
				$row=$this->grp_list[$i];
				
				$services=$model->getservices($row->id);
			
				if(count($services)==0)
				{
					continue;
				}
				?>
			<div class="grp_div">
				<!--<div class="grp_name_div"><?php echo $row->name ?></div>-->
				<?php
				for($j=0;$j<count($services); $j++)
				{
				?>
					<?php   
									$profileImage='';
									if( $services[$j]->img!='')
									{
										if(file_exists(JPATH_ROOT.'/'.'components/'.$option.'/assets/service/images/'.$services[$j]->img))
										{
												$profileImage=	$path.$services[$j]->img;
										}
										else{
								    			$profileImage=$path."PROFILE2.jpg";
											}
									}
									else
									{
										$profileImage=$path."PROFILE2.jpg";
									}
								?>	
						<div class="service_div"> 

						<div class="divimg">
							<img class="image" id="image1"  src="<?php echo $profileImage; ?>" >
						</div>
						<div class="detail">
						<div class="divnm"> 
							<a href="index.php?option=com_appointment&view=employee_list&ser_id=<?php echo $services[$j]->id;?>"  ><?php echo $services[$j]->name; ?></a>
						</div>
						<div class="divpr">$ <?php echo $services[$j]->price; ?></div>
						<div class="divdp"><?php echo $services[$j]->description; ?></div>
					</div> 
					
</div>
				<?php
				}
				?>
				</div>
				<?php
			}
		?>
</div>
		<input type="hidden" name="view" value="service_list" />
		<input type="hidden" name="userid" value="" />
		<input type="hidden" name="option" id="option" value="com_appointment">
		<input type="hidden" name="task" value="save" />
		<input type="hidden" name="id" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		</form>
</div>
