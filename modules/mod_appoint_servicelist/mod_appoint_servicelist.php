<?php 
/**
* @package   JE itemlist
* @copyright Copyright (C) 2009-2010 Joomlaextensions.co.in All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
**/

defined('_JEXEC') or die('Restricted access');
require_once (dirname(__FILE__).'/'.'helper.php');

$layout = $params->get('layout','default');

$document = JFactory::getDocument();
$item=mod_appoint_servicelistHelper::getReturnURL();

$services=mod_appoint_servicelistHelper::getser($params);

require_once (dirname(__FILE__).'/'.'helper.php');

JHtml::_('script', JURI::root().'components'.'/'.'com_appointment'.'/'.'assets'.'/'.'js'.'/'.'min.js');

$path = JModuleHelper::getLayoutPath('mod_appoint_servicelist', $layout);
if (file_exists($path)) {
	require($path);
}
?>





