<?php
 /**

* @package Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.view' );


class service_listViewservice_list extends JViewLegacy
{
	function __construct($config = array())
	{
		 parent::__construct( $config );
		
	}
	function display($tpl = null)
	{
	
		$document = JFactory::getDocument();
		$document->setTitle( JText::_('SERVICE_LIST') );
		$uri 		=JFactory::getURI();
		$option = JRequest::getVar('option','','request','string');
		$grp_list	= $this->get('Data');
	/*echo "<pre>";
		print_r($grp_list); exit;*/
	
		$url= $uri->root();
		$this->setLayout('default');
		$this->assignRef('grp_list',$grp_list);
		$this->assignRef('lists',$lists);
		
		parent::display($tpl);
	} 
}

