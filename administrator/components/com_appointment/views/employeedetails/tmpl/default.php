 <?php
  /**

* @package Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/

defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.tooltip');
//JHTMLBehavior::modal();
$uri = JURI::getInstance();
$url= $uri->root();
$editor = JFactory::getEditor();
//JHTML::_('behavior.calendar');

$option = JRequest::getVar('option','','','string');
$image_path =$url."components/".$option."/assets/emp/images/";
$document=JFactory::getDocument();
$document->addStyleSheet("components/com_appointment/assets/css/appointment.css");
?>
<!DOCTYPE html>
<script>
var option='<?php echo $option; ?>';
var url1='<?php echo $url;?>';
</script>
<script type="text/javascript" src="<?php echo $url.'/administrator/components/'.$option; ?>/assets/js/main.js">
</script>
<script language="javascript" type="text/javascript">
Joomla.submitform=function submitbutton(pressbutton)
{
 
  	var form = document.adminForm;
  	if (pressbutton)
    {form.task.value=pressbutton;}
    if (pressbutton == 'cancel') 
    {
    	 form.submit();
    }
    else
    {
	  var a = document.adminForm.fname.value;
	  if(a=="")
	  {   
	    alert("Please Enter Your First Name");
	    return false;
	  } 
	  var b = document.adminForm.lname.value;
	  if(b=="")
	  {   
	    alert("Please Enter Your Last Name");
	    return false;
	  } 
	
	var Email = document.adminForm.email.value;
    var atpos = Email.indexOf("@");
    var dotpos = Email.lastIndexOf(".");   
	if(Email=="")
	{	
				alert("Please Enter EmailId");
				return false;
	}	
		
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=Email.length)
     {
        		alert("Not a valid e-mail address");
        		return false;
    }	
     var phone= document.adminForm.contact.value;
	 if(phone=="")
	{		
		alert("Please Enter 10 Digit Number");
		return false;
	}
	if(isNaN(phone))
	{
		alert("Please Enter Only Number");
		
		return false;
	}

	if(!(phone.length == 10))
	{
		alert("Please Enter 10 Digit Number");
		return false	
	}
}
var form = document.adminForm;
   if (pressbutton)
    {form.task.value=pressbutton;}
     if ((pressbutton=='publish')||(pressbutton=='unpublish')||(pressbutton=='saveorder'))
     {       
      form.view.value="employeedetails";
     }
    try {
        form.onsubmit();
        }
    catch(e){}

    form.submit();
}
</script>
<script type="text/javascript">


function closedCheckValueChanged() {

		
	 var z = document.getElementById('checkmydata').checked;
	
	if(z==0)
		 {
		 	document.getElementById('time').style.display = 'block';
			document.getElementById('minute').style.display = 'block';
			document.getElementById('time1').style.display = 'block';
			document.getElementById('minute1').style.display = 'block';
     		
			 //jQuery('.hide').hide("slow");
		}
		else
		{
			document.getElementById('time').style.display = 'none';
			document.getElementById('minute').style.display = 'none';
			document.getElementById('time1').style.display = 'none';
			document.getElementById('minute1').style.display = 'none';
			
		}

		
}
function myfunction()
{
		
       // jQuery("#selectday option:selected").attr('disabled','disabled');
        //jQuery("#selectday option:selected[value="strUser"]").prop('disabled', true);
		
	
}

</script>

<script type="text/javascript" src="<?php echo $url.'/administrator/components/'.$option; ?>/assets/js/main.js">
</script>

 
<form action="" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
<div class="col50">
<fieldset class="adminForm">
<legend><?php echo JText::_('Employee Details');?></legend>
<table>

	<tr>
		<td style="width: 1278px;">
		<table cellpadding="5px" cellspacing="5px">

   			  <tr>
         			<td><label for="name"><?php echo JText::_('FIRST_NAME'); ?>*</label></td>
        			<td><input type="text" name="fname" placeholder="Enter First Name" required value="<?php echo 
        			$this->employeelist->fname;?>"/></td>
    		 </tr>
    		 <tr>
       				  <td><label for="name"><?php echo JText::_('LAST_NAME'); ?>*</label></td>
        			  <td><input type="text" name="lname" placeholder="Enter	Last Name" required value="<?php echo 
        			  $this->employeelist->lname;?>"/></td>
    		 </tr>
   			  <tr>
       				  <td><label for="name"><?php echo JText::_('EMAIL'); ?>*</label></td>
        			 <td><input type="text" name="email" placeholder="Enter Email Address" required value="<?php echo 
        			 $this->employeelist->email;?>"/></td>
   			  </tr>
   			 <tr>
   					  <td><label for="name"><?php echo JText::_('CONTACT');?>*</label></td>
    				 <td><input type="text" name="contact" placeholder="Enter Contact Number" required value="<?php echo 
    				 $this->employeelist->contact;?>"/></td>
  			   </tr>
 
  			   <tr>
					<td><label for="name"><?php echo JText::_('IMAGE'); ?></label></td>
					<td><input type="file"   name="img" id="img" value="<?php echo $this->employeelist->img;?>"/></td>
					<tr>
	
						<?php
							 if($this->employeelist) 
								{
									
									  if($this->employeelist->img!='')
			       					  {    
			   			  ?>
      	 				<td>
					 	   <img src="<?php echo $image_path.'thumb_'.$this->employeelist->img;?>" height="100" width="100"/>
					    </td>
									            
						       <?php 
						           }	
						           else
						           {
							   	?>
	       				 <td>
							    <img src="<?php echo $image_path.'noimagefound.jpg'; ?>" height="100" width="100"/>
						</td>
								<?php
								  		 }
								 } 
								?>
					</tr>
				</tr>

				</table>
				</td>
				</tr>

	</table>
	
 

<table id="cdsets">
<tr>

     <td><label for="name"><?php echo JText::_('Select Day'); ?></label></td>
     <td><?php echo $this->lists['selectday_list']; ?>  </td>
     <td> <input  type="checkbox" id="checkmydata" onclick="closedCheckValueChanged();" ></td>
     <td><label>Closed</label></td>
 	 <td><label for="name">&nbsp;<?php echo JText::_('From'); ?>&nbsp;|</label></td>
	 <td style="width:85px"><?php echo $this->lists['time_list']; ?>  </td>
	 <td ><?php echo $this->lists['minute_list']; ?> </td>
	 <td><label for="name"><?php echo JText::_('To'); ?>&nbsp;|</label></td>
     <td ><?php echo $this->lists['time1_list']; ?>  </td>
     <td><?php echo $this->lists['minute1_list']; ?>  </td>
  
 <td><input type="button" id="cdaddvalue" value="<?php echo JText::_('ADD Working Days') ;?>" onclick="cdaddwork();"></td>
	</tr>
	 <tr>
        <td>
          <?php 
          	          
             if(count($this->employeedaytime)>0)
             { 
                for($i=0;$i<count($this->employeedaytime);$i++)
                { 
                   $row=$this->employeedaytime[$i];
                                          
          ?>
       <div id="<?php echo $i;?>">
       <table>
          <tr>             
              <td>
              
                <input type="text" name="selectday[]" id="selectday"  
                 value="<?php echo $row->day; ?>">
                 <label for="name"><?php echo JText::_('From'); ?></label>
                <input type="text" name="time[]" id="time" value="<?php echo $row->time; ?>">
                <input type="text" name="minute[]" id="minute" value="<?php echo $row->minute; ?>">
                <label for="name"><?php echo JText::_('To'); ?></label>
                <input type="text" name="time1[]" id="time1" value="<?php echo $row->time1; ?>">
                <input type="text" name="minute1[]" id="minute1" value="<?php echo $row->minute1; ?>">
             
               <input type="hidden" name="cid[]" value="<?php echo $row->eid;?>" >
               &nbsp;<input type="button" name="cddelete" onclick="cdDelete23(<?php echo $row->id; ?>,this)" class="button" value="delete">
                    
                </td>
                </tr>
       </table>
          </div>
                <?php 
                }
                ?>
           </td>
              </tr>
            <?php

    } ?>
	
</table>


	</fieldset>

</div>
<div class="clr"></div>

<input type="hidden" name="id" value="<?php echo $this->employeelist->id; ?>" />
<input type="hidden" name="cid[]" id="cid[]" value="<?php echo $this->employeelist->id; ?>"/>
<input type="hidden" name="task" id="task" value="" />
<input type="hidden" name="view" value="employeedetails"/>
<input type="hidden" name="option" value="<?php echo $option;?>"/>
</form>