<?php
 /**

* @package Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.model');

class cart_detailModelcart_detail extends JModelLegacy
{
	var $_id = null;
	var $_data = null;
	var $_region = null;
	var $_table_prefix = null;
	var $_copydata	=	null;

	function __construct()
	{
		parent::__construct();
		$this->_table_prefix = '#__appointment_';		
	  	$cid = JRequest::getVar('cid',  '', 'request', 'string');
		$this->setId($cid);
		
	}
	function setId($id)
	{
		$this->_id		= $id;
		$this->_data	= null;
	}
	function getData()
	{
		
		if ($this->_loadData())
		{
			
		}else  $this->_initData();
		return $this->_data;
	}
	 
	
	function _loadData()
	{
		if (empty($this->_data))
		{
				/*$query = 'SELECT a.name AS grpname,b.* FROM  jos_appointment_groupsdetail AS a INNER JOIN jos_appointment_services AS b ON a.id=b.groups order by  b.groups,b.id'; */
				$query = 'SELECT id,name FROM  `#__appointment_groupsdetail`';
				$this->_db->setQuery($query);
				$this->_data = $this->_db->loadObjectlist();
				return (boolean) $this->_data;
			
			return (boolean) null;
		}
		return true;
	}
	function getservices($id)
	{
		$query = 'SELECT * FROM `#__appointment_services` WHERE groups='.$id .' LIMIT 0,3';
		$this->_db->setQuery($query);
		$this->_data = $this->_db->loadObjectlist();
		
		return $this->_data; 
	}
	
	function _initData()
	{
		if (empty($this->_data))
		{
			$detail = new stdClass();
			
			$detail->id= 0;
			$detail->userid= 0;
			$detail->fname = null;
			$detail->lname = null;
			$detail->department_id = 0;
			$detail->phone_number = null;
			$detail->branch_id = 0;
			$detail->email = null;
			$detail->username = null;
			
			$this->_data = $detail;
			return (boolean) $this->_data;
		}
		return true;
	}
	
  	function store($data)
	{ 
		$row =$this->getTable('staffs');
		if (!$row->bind($data)) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}
		if (!$row->store()) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}
		return $row;
	}	
	function getDepartments()
	{
		$db=JFactory::getDBO();
		$query = 'SELECT id AS value,name AS text FROM '.$this->_table_prefix.'departments WHERE published=1'; 
		$db->setQuery($query);
		return $db->loadObjectList();
	}
}
?>
