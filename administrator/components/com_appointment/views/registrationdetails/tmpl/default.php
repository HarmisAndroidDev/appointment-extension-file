 <?php
  /**

* @package Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/

defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.tooltip');
//JHTMLBehavior::modal();
JHtml::_('behavior.formvalidator');
$uri = JURI::getInstance();
$url= $uri->root();
$editor = JFactory::getEditor();
//JHTML::_('behavior.calendar');

$option = JRequest::getVar('option','','','string');
$image_path =$url."components/".$option."/images/";
?>
 <script language="javascript" type="text/javascript">
Joomla.submitform =function submitbutton(pressbutton)
 {
  var form = document.adminForm;
  	{
  	if (pressbutton)
    {form.task.value=pressbutton;}
    if (pressbutton == 'cancel') 
    {
    	 form.submit();
    }
	}
    else
    {
	  var a = document.adminForm.username.value;
	  if(a=="")
	  {   
	    alert("Please Enter User Name");
	    return false;
	  } 
	  
	  if(!isNaN(a))
	  {
	    alert("Please Enter Only Characters");
	    
	    return false;
	  }
	  var pass=document.adminForm.password.value;
		var pass1=document.adminForm.cpass.value;
		if(pass=="")
		{
			alert("Please Enter Password");
			return false;
		}
		if(pass1=="")
		{
			alert("Please Enter Conform Password");
			return false;
		}
		if(pass!=pass1)
		{
			alert("Password are not match");
			return false;
		}
	    
		 var Email = document.adminForm.email.value;
 	   var atpos = Email.indexOf("@");
 	  var dotpos = Email.lastIndexOf(".");   
		if(Email=="")
		{	
				alert("Please Enter EmailId");
				return false;
		}	
		
  	  if (atpos<1 || dotpos<atpos+2 || dotpos+2>=Email.length)
  	   {
        		alert("Not a valid e-mail address");
        		return false;
  		  }	
}
var form = document.adminForm;
   if (pressbutton)
    {form.task.value=pressbutton;}
     if ((pressbutton=='publish')||(pressbutton=='unpublish')||(pressbutton=='saveorder'))
     {       
      form.view.value="appointmentdetails";
     }
    try {
        form.onsubmit();
        }
    catch(e){}
    
    form.submit();


}
</script> 

<form action="" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data" class="form-validate">
<div class="col50">
<fieldset class="adminForm">
<legend><?php echo JText::_('Registration_Details');?></legend>
<table class="admintable" border="0">
	
     <tr>
         <td><label for="name"><?php echo JText::_('User Name'); ?></label></td>
         <td><input type="text" placeholder="Enter User Name" required name="username" value="<?php echo $this->user->username;?>"/></td>
     </tr>
     <tr>
      <td><label for="name"><?php echo JText::_('Password');?></label></td>
     <td><input type="password" name="password" placeholder="Enter Password"  value=""  /></td>
     </tr>
     <tr>
     <td><label for="name"><?php echo JText::_('Conform_Password');?></label></td>
     <td><input type="password" name="cpass"  placeholder="Enter Conform Password"  value="" /></td>
     </tr>
     <tr>
     <td><label for="name"><?php echo JText::_('Email');?></label></td>
     <td><input type="text" name="email"  placeholder="Enter Email Address" required value="<?php echo $this->user->email;?>"/></td>
    </tr>
        
	</table>
	</fieldset>
</div>	
<div class="clr"></div>
<input type="hidden" name="cid" id="cid" value="<?php echo $this->user->id; ?>"/>
<input type="hidden" name="task" id="task" value="save"/>
<input type="hidden" name="view" value="registrationdetails"/>
<input type="hidden" name="option" value="<?php echo $option;?>"/>
</form>
