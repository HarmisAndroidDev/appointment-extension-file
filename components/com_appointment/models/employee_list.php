<?php
/**
* @package Appointment
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
**/

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.model');

class employee_listModelemployee_list extends JModelLegacy
{
	var $_data = null;
	var $_total = null;
	var $_pagination = null;
	var $_table_prefix = null;
                      	
	function __construct()
	{
		parent::__construct();
		global $context;
		$mainframe	= JFactory::getApplication();
		$this->_table_prefix = '#__';
		$array = JRequest::getVar('id',  0, '', 'array');		
		$this->setId((int)$array[0]);

	}
  function setId($id)
	{	
	     $this->_id= $id;
	     $this->_data= null;
	 }
	
	function getData()
	{	
		if (empty($this->_data))
		{
			$mainframe  = JFactory::getApplication();
			
			$query = $this->_buildQuery();
			$this->_data = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
		}
		return $this->_data;
	}
	
	function _buildQuery()
	{
		$db = JFactory::getDBO();
		$mainframe    = JFactory::getApplication();	
		$user =  clone(JFactory::getUser());

		
		 $id = JRequest::getVar('id',  0, '', 'int');
		
	 $query='select * from `#__appointment_employee`';


			 		return $query;
	}

	function getempsevice($empid)
	{
		$db=JFactory::getDBO();

			  $query='SELECT s.name FROM
			  '.$this->_table_prefix.'appointment_services AS s 
			  INNER JOIN '.$this->_table_prefix.'appointment_serviceemp AS se ON s.id =se.sid
			  INNER JOIN '.$this->_table_prefix.'appointment_employee AS emp ON se.eid=emp.id where emp.id='.$empid; 


		$db->setQuery($query);
		return $db->loadColumn();
	}


	function getemployee($serid)
	{
		$db=JFactory::getDBO();
		 $query="select eid from ".$this->_table_prefix."appointment_serviceemp where sid=".$serid; 
		$db->setQuery($query);
		return	$db->loadColumn();


	}


	
}
