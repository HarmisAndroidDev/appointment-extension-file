<?php
/**
* @package  Appointment
* @copyright Copyright (C) 2009-2010 Joomlaextensions.co.in All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
**/ 
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.model');

class Tableemployeedaytime extends JTable
{
	var $eid = null;
	var $day = null;
	var $closed =null;
	var $time=null;
	var $minute=null;
	var $time1=null;
	var $minute1=null;
	
	
	
 	var $published = null;
 	function Tableemployeedaytime($db)
 	 {
 	 	 $this->_table_prefix = '#__appointment_';
 	 	 parent::__construct($this->_table_prefix.'employeedaytime','id',$db);
 	 }
 	 function bind($array, $ignore = '')
	{
		/*echo '<pre>';
		print_r($array);
		exit;*/
		
		if (key_exists( 'params', $array ) && is_array( $array['params'] )) {
			$registry = new JRegistry();
			$registry->loadArray($array['params']);
			$array['params'] = $registry->toString();
			
		}

		return parent::bind($array, $ignore);
	}

}

?>
