<?php
 /**

* @package  Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/

defined( '_JEXEC' ) or die( 'Restricted access' );


jimport( 'joomla.application.component.view' );

class groupsViewgroups extends JViewLegacy
{
	function __construct( $config = array())
	{
		 parent::__construct( $config );
	}
    
	function display($tpl = null)
	{	
		$mainframe = JFactory::getApplication();
		$context='groups';
		
		$document =  JFactory::getDocument();
		$document->setTitle( JText::_('GROUPS'));
   		jimport('joomla.html.pagination');
   		
   		JToolBarHelper::title(   JText::_( 'GROUPS_MANAGEMENT' )  );
     	JToolBarHelper::addNew();
 		JToolBarHelper::editList();		
		JToolBarHelper::deleteList();
		JToolBarHelper::publishList();
		JToolBarHelper::unpublishList();		
  		$uri	= JFactory::getURI();
		$filter_order     = $mainframe->getUserStateFromRequest( $context.'filter_order',      'filter_order', 	  'ordering' );
		$filter_order_Dir = $mainframe->getUserStateFromRequest( $context.'filter_order_Dir',  'filter_order_Dir', '' );
		$groups = $mainframe->getUserStateFromRequest( $context.'groups','groups',0 );
		$limitstart     = $mainframe->getUserStateFromRequest( $context.'limitstart',      'limitstart', 	  '0' );
		$limit = $mainframe->getUserStateFromRequest( $context.'limit',  'limit', '10' );
		$lists['order'] 		= $filter_order;  
		$lists['order_Dir'] 	= $filter_order_Dir;
		$lists['order'] 	= $filter_order;  
		$lists['order_Dir'] = $filter_order_Dir;

		$total =  $this->get( 'Total');
					
		$groups=  $this->get( 'Data');
		$pagination = new JPagination( $total, $limitstart, $limit);
		
		$publish_op = array();
		$publish_op[]   	= JHTML::_('select.option', '-1',JText::_('SELECT_STATE'));
		$publish_op[]   	= JHTML::_('select.option', '1',JText::_('PUBLISHED'));
		$publish_op[]   	= JHTML::_('select.option', '0', JText::_('UNPUBLISHED'));
		$published	= JRequest::getVar('published', '-1','request','int');
		$searchlists['published'] = JHTML::_('select.genericlist',$publish_op,'published','class="inputbox" size="1" onchange="selectsearch(this.value)" ','value','text' ,$published); 
		/*$lists['id'] = JHTML::_('select.genericlist', $groups, 'id', 'class="inputbox" onchange="adminForm.submit()" ', 'value', 'text', $id );*/
		$requesturl = $uri->toString(); 
		$this->assignRef('lists',		$lists);
		$this->assignRef('searchlists',	$searchlists);   
  	   	$this->assignRef('pagination',	$pagination);	
  	   	$this->assignRef('groups',		$groups); 		    
		$this->assignRef('request_url',	$requesturl);    		  	
   
   	 	parent::display($tpl);
  }
  		  protected function getSortFields(){
			return array(
				'ordering' => JText::_('JGRID_HEADING_ORDERING'),
				'published' => JText::_('JSTATUS'),
				'name' => JText::_('JGLOBAL_TITLE')
			);
		}
		

	}
  
  
  
