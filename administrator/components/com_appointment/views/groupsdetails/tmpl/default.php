 <?php
  /**

* @package Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/

defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.tooltip');
//JHTMLBehavior::modal();
$uri = JURI::getInstance();
$url= $uri->root();
$editor = JFactory::getEditor();
//JHTML::_('behavior.calendar');

$option = JRequest::getVar('option','','','string');


?>

<script language="javascript" type="text/javascript">
Joomla.submitform =function submitbutton(pressbutton)
 {

		var form = document.adminForm;
		if (pressbutton == 'cancel') 

		{
			submitform( pressbutton );
			return;
		}
		//save button are not work
		
		 var a = document.adminForm.name.value;
	  		if(a=="")
	  		{   
	    		alert("Please Enter Groups Name");
	    		return false;
	  		} 
	  
	 		 if(!isNaN(a))
	  		{
	    		alert("Please Enter Only Characters");
	       		 return false;
	  		}
	  		submitform( pressbutton );
}	
function submitform(pressbutton)
{
	var form = document.adminForm;
	   if (pressbutton)
		{form.task.value=pressbutton;}
	  if ((pressbutton=='publish')||(pressbutton=='unpublish')||(pressbutton=='saveorder'))
	  {   
	   form.view.value="groupsdetails";
	  }
	 try {
	  form.onsubmit();
	  }
	 catch(e){}
	  form.submit();
	 
}
</script>


<form action="" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
<div class="col50">
	
<fieldset class="adminForm">
<legend><?php echo JText::_('GROUPS_DETAILS');?></legend>
<table class="admintable" border="0">

     <tr>
         <td><label for="name"><?php echo JText::_('NAME'); ?></label></td>
         <td><input type="text" name="name" placeholder="Enter Name" required value="<?php echo $this->groups1->name;?>"/></td>
     </tr>
    
	<tr>
	<td><label for="name"><?php echo JText::_('DESCRIPTION');?></label></td>
	<td style="width: 498px;"><?php $editor = JFactory::getEditor(); ?>
	<?php echo $editor->display("description",$this->groups1->description,500,'100','20','0'); ?>
	</td>
	</tr>
	 <!-- <tr>
      <td><label for="name"><?php echo JText::_('SELECT_PARENT');?></label></td>
      <td><?php echo $this->lists['groups']; ?></td>
    </tr> -->
	
	</table>
	</fieldset>
</div>	
<div class="clr"></div>

<input type="hidden" name="cid" id="cid" value="<?php echo $this->groups1->id; ?>"/>
<input type="hidden" name="task" id="task" value="save"/>
<input type="hidden" name="view" value="groupsdetails"/>
<input type="hidden" name="option" value="<?php echo $option;?>"/>
</form>
