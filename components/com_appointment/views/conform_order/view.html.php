<?php
 /**

* @package  Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.view' );


class conform_orderViewconform_order extends JViewLegacy
{
	function __construct($config = array())
	{
		 parent::__construct( $config );
		
	}
	function display($tpl = null)
	{
	
		$document = JFactory::getDocument();
		$document->setTitle( JText::_('CONFORM_ORDER') );
		$uri 		=JFactory::getURI();
		$option = JRequest::getVar('option','','request','string');
		$order	= $this->get('Data');
		/*echo "<pre>";
		print_r($order); exit;*/
	
		$url= $uri->root();
		$this->setLayout('default');
		$this->assignRef('order',$order);
		$this->assignRef('lists',$lists);
		
		parent::display($tpl);
	} 
}

