<?php
 /**

* @package Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );
jimport('joomla.filesystem.file');
class servicesdetailsController extends JControllerLegacy
{
	function __construct( $default = array())
	{
		$this->_table_prefix = '#__appointment_';
		parent::__construct( $default );
	}	
	
	function display($cachable = false, $urlparams = '')
	{

		parent::display();
	}
	
	function edit() {
		JRequest::setVar ( 'view', 'servicesdetails' );
		JRequest::setVar ( 'layout', 'default' );
		JRequest::setVar ( 'hidemainmenu', 1 );
		parent::display ();
	
	}

	function save() 
	{

		$post = JRequest::get ( 'POST' );
		$option = JRequest::getVar('option','','request','string');
		$user	= clone(JFactory::getUser());
		$adminid= $user->id;
		$post['adminid']=$adminid;
		$post["description"] = JRequest::getVar( 'description', '', 'POST', 'string', JREQUEST_ALLOWRAW );
		$cid = JRequest::getVar ('cid', array(0), 'POST', 'array' );

		if($cid[0]!=0)
		{
			$post['id']=$cid[0];
		}
		else
		{
			unset($post['id']);
		}

		$emp_id=JRequest::getVar('emp_id',array(0),'POST','array');
		$appointment_id=JRequest::getVar('appointment_id',array(0),'POST','array');			
	  	$post ['id'] = $cid [0];
	  	$post['groups']=JRequest::getvar('groups_id','','POST','string',JREQUEST_ALLOWRAW);
		$post['duration']=JRequest::getvar('duration','','POST','string',JREQUEST_ALLOWRAW);
		$post['sleeptime']=JRequest::getvar('sleeptime','','POST','string',JREQUEST_ALLOWRAW);
		$post['price']=JRequest::getvar('price','','POST','string',JREQUEST_ALLOWRAW);	  	
		$db	=JFactory::getDBO();
			
			

			$file 	= JRequest::getVar ( 'img', '', 'files', 'array' );	
		
	  		  if($file['name']!='')
	  		 { 		

	 	  	$filetype = strtolower(JFile::getExt($file['name']));	
	  	
			if($filetype=='jpg' || $filetype=='jpeg' || $filetype=='gif' || $filetype=='png' || $filetype='bmp' || $filetype=='tif')
			{
			$post['img']= JPath::clean(time().'_'.$file['name']);
		                       	
            
			$src	= $file['tmp_name'];
			$dest 	= JPATH_ROOT.'/'.'components/com_appointment/assets/service/images/'.$post['img'];
			JFile::upload($src,$dest);	
			$dest1 	= JPATH_ROOT.'/'.'components/com_appointment/assets/service/images/thumb_'.$post['img'];
			copy($dest,$dest1);
			$img 	= new Thumbnail();

			$thumboption 	= array('type'   => $filetype,'width' => "50",'height'  => "50",'method'  => THUMBNAIL_METHOD_SCALE_MAX);
			Thumbnail::CreatThumb($dest, $dest1, $thumboption);

			if($post['oldphoto'])
			{
				$dest 	= JPATH_ROOT.'/'.'components/com_appointment/assets/service/images/'.$post["oldphoto"];
				unlink($dest);
				$dest12 = JPATH_ROOT.'/'.'components/com_appointment/assets/service/images/thumb_'.$post["oldphoto"];
				unlink($dest12);
			}		
			if($file["name"][$id]=="")
				$pname 	= $post['oldphoto'][$id];
			else
				$pname	= $file["name"][$id];

			}
		
		} 
		else
		{
			$post['img']	= $post['oldphoto'];
		}

		$model = $this->getModel ( 'servicesdetails' );
	
	//For Ordering
		if($cid[0]==0)
			{	

				 $sql = "SELECT max(ordering) As ordering FROM ".$this->_table_prefix."services"; 
				$db->setQuery($sql);
				$max  = $db->loadObject();
				if($max->ordering)
				{
					$post['ordering']=$max->ordering+1;
				}
				else
				{
				$post['ordering']=1;
				}
			}
			else
			{
				unset($post['ordering']);
			}

		if($cid = $model->store ( $post ))
		{			
			if(count($post['emplid']))
			{	
				for($i=0;$i<count($post['emplid']); $i++)
				{
					$row=$post['emplid'][$i];
					
					$sqlphoto="INSERT INTO ".$this->_table_prefix."serviceemp (sid,eid) VALUES(".$cid.",'".$row."')";

					$db->setQuery($sqlphoto);

					 $db->query();
				}
			}

			if(count($post['appid']))
			{	
				for($i=0;$i<count($post['appid']); $i++)
				{
					$row=$post['appid'][$i];
					
					$sqlphoto="INSERT INTO ".$this->_table_prefix."serviceoption (sid,aid) VALUES(".$cid.",'".$row."')";

					$db->setQuery($sqlphoto);

					 $db->query();
				}
			}


		$msg = JText::_ ( 'DETAIL_SAVED' );
		
	}
		
		
		$this->setRedirect ( 'index.php?option=' . $option . '&view=services', $msg );
		
	}
	
		
	 function remove() 
	 {
		$post = JRequest::get ( 'POST' );
			$option = JRequest::getVar('option','','request','string');
		
	 			$cid = JRequest::getVar ( 'cid', array (0 ), 'post', 'array' );
	
		
		if (! is_array ( $cid ) || count ( $cid ) < 1) 
		{
			JError::raiseError ( 500, JText::_ ( 'SELECT_AN_ITEM_TO_DELETE' ) );
		}
		
		$model = $this->getModel ( 'servicesdetails' );
		if (! $model->delete( $cid )) 
		{
			echo "<script> alert('" . $model->getError ( true ) . "'); window.history.go(-1); </script>\n";
		}
		$msg = JText::_ ( 'DETAIL_DELETED_SUCCESSFULLY' );
		$this->setRedirect ( 'index.php?option='.$option.'&view=services',$msg );
	}
	function publish() 
	{			
		 $option = JRequest::getVar('option','','request','string');
		
		 $cid = JRequest::getVar ( 'cid', array (0), 'POST', 'array' );
		 
		if (! is_array ( $cid ) || count ( $cid ) < 1) 
		{
		  JError::raiseError ( 500, JText::_ ( 'SELECT_AN_ITEM_TO_PUBLISH' ) );
		}
		
		$model = $this->getModel ( 'servicesdetails' );
		if (! $model->publish ( $cid, 1 ))
		 {
			echo "<script> alert('" . $model->getError ( true ) . "'); window.history.go(-1); </script>\n";
		 }
		$msg = JText::_ ( 'DETAIL_PUBLISHED_SUCCESSFULLY' );
		$this->setRedirect ( 'index.php?option='.$option.'&view=services',$msg );
	}
	
	function unpublish() 
	{
		
		$option = JRequest::getVar('option','','request','string');
		
		$cid = JRequest::getVar ( 'cid', array (0 ), 'POST', 'array' );
		
		if (! is_array ( $cid ) || count ( $cid ) < 1) {
			JError::raiseError ( 500, JText::_ ( 'SELECT_AN_ITEM_TO_UNPUBLISH' ) );
		}
		$model = $this->getModel ( 'servicesdetails' );
		if (! $model->publish ( $cid, 0 )) {
			echo "<script> alert('" . $model->getError ( true ) . "'); window.history.go(-1); </script>\n";
		}
		$msg = JText::_ ( 'DETAIL_UNPUBLISHED_SUCCESSFULLY' );
		$this->setRedirect ( 'index.php?option='.$option.'&view=services',$msg );
	}
	
	
	function apply() 
	{

		$post = JRequest::get ( 'POST' );
	
		$option = JRequest::getVar('option','','request','string');
		$user	= clone(JFactory::getUser());
		$adminid= $user->id;
		$post['adminid']=$adminid;
		$post["description"] = JRequest::getVar( 'description', '', 'POST', 'string', JREQUEST_ALLOWRAW );
		$cid = JRequest::getVar ('cid', array(0), 'POST', 'array' );
		if($cid[0]!=0)
		{
			$post['id']=$cid[0];
		}
		else
		{
			unset($post['id']);
		}

		$emp_id=JRequest::getVar('emp_id',array(0),'POST','array');
		$appointment_id=JRequest::getVar('appointment_id',array(0),'POST','array');			
	  	$post ['id'] = $cid [0];
	  	$post['groups']=JRequest::getvar('groups_id','','POST','string',JREQUEST_ALLOWRAW);
		$post['duration']=JRequest::getvar('duration','','POST','string',JREQUEST_ALLOWRAW);
		$post['sleeptime']=JRequest::getvar('sleeptime','','POST','string',JREQUEST_ALLOWRAW);
		$post['price']=JRequest::getvar('price','','POST','string',JREQUEST_ALLOWRAW);	  	
		$db	=JFactory::getDBO();
		


			$file 	= JRequest::getVar ( 'img', '', 'files', 'array' );	
		
	  			  if($file['name']!='')
	  		 { 		

	 	  	$filetype = strtolower(JFile::getExt($file['name']));	
	  	
			if($filetype=='jpg' || $filetype=='jpeg' || $filetype=='gif' || $filetype=='png' || $filetype='bmp' || $filetype=='tif')
			{
			$post['img']= JPath::clean(time().'_'.$file['name']);
		                       	
            
			$src	= $file['tmp_name'];
			$dest 	= JPATH_ROOT.'/'.'components/com_appointment/assets/service/images/'.$post['img'];
			JFile::upload($src,$dest);	
			$dest1 	= JPATH_ROOT.'/'.'components/com_appointment/assets/service/images/thumb_'.$post['img'];
			copy($dest,$dest1);
			$img 	= new Thumbnail();

			$thumboption 	= array('type'   => $filetype,'width' => "50",'height'  => "50",'method'  => THUMBNAIL_METHOD_SCALE_MAX);
			Thumbnail::CreatThumb($dest, $dest1, $thumboption);

			if($post['oldphoto'])
			{
				$dest 	= JPATH_ROOT.'/'.'components/com_appointment/assets/service/images/'.$post["oldphoto"];
				unlink($dest);
				$dest12 = JPATH_ROOT.'/'.'components/com_appointment/assets/service/images/thumb_'.$post["oldphoto"];
				unlink($dest12);
			}		
			if($file["name"][$id]=="")
				$pname 	= $post['oldphoto'][$id];
			else
				$pname	= $file["name"][$id];

			}
		
		} 
		else
		{
			$post['img']	= $post['oldphoto'];
		}
		
	
		$model = $this->getModel ( 'servicesdetails' );
	
	if($cid[0]==0)
			{	

				 $sql = 'SELECT max(ordering) As ordering FROM '.$this->_table_prefix.'services'; 
				$db->setQuery($sql);
				$max  = $db->loadObject();
				if($max->ordering)
				{
					$post['ordering']=$max->ordering+1;
				}
				else
				{
				$post['ordering']=1;
				}
			}
			else
			{
				unset($post['ordering']);
			}

		if($cid = $model->store ( $post ))
		{
			if(count($post['emplid']))
			{	
				for($i=0;$i<count($post['emplid']); $i++)
				{
					$row=$post['emplid'][$i];
					
					$sqlphoto="INSERT INTO ".$this->_table_prefix."serviceemp (sid,eid) VALUES(".$cid.",'".$row."')";

					$db->setQuery($sqlphoto);

					 $db->query();
				}
			}

			if(count($post['appid']))
			{	
				
				for($i=0;$i<count($post['appid']); $i++)
				{
					$row=$post['appid'][$i];
					
					$sqlphoto="INSERT INTO ".$this->_table_prefix."serviceoption (sid,aid) VALUES(".$cid.",'".$row."')";

					$db->setQuery($sqlphoto);

					 $db->query();
				}
			}


	
				

		$msg = JText::_ ( 'DETAIL_SAVED' );
		
	}
		
		$this->setRedirect ('index.php?option='.$option.'&view=servicesdetails&task=edit&cid[]='.$cid, $msg );
		
	}
	
	function cancel() {
		
		$option = JRequest::getVar('option','com_appointment','request','string');
		$msg = JText::_ ( 'DETAIL_EDITING_CANCELLED' );
		$this->setRedirect ( 'index.php?option='.$option.'&view=services',$msg );
	}

function removeCdset() {

			
		$db = JFactory::getDbo();
		$option = JRequest::getVar('option','','request','string');
		
		 $cid = JRequest::getVar ( 'cid', array (0 ), 'request', 'array' ); 
		 	 $sid = JRequest::getVar ( 'sid',  'request', 'array' ); 
		 	 	 $eid = JRequest::getVar ( 'eid',  'request', 'array' ); 
			
		//echo "<pre>";print_r($cid);exit;
		if (! is_array ( $cid ) || count ( $cid ) < 1) {
			JError::raiseError ( 500, JText::_ ( 'SELECT_AN_ITEM_TO_DELETE' ) );
		}
		
		//$cid = implode( ',', $cid );
		   $query = 'DELETE FROM '.$this->_table_prefix.'serviceemp WHERE sid=' .$sid. ' AND eid='.$eid;
		 $db->setQuery($query);
			if(!$db->query()) {
			$this->setError($db->getErrorMsg());

		}

}
function removeCdset2()
 {
		
		$db = JFactory::getDbo();
		$option = JRequest::getVar('option','','request','string');
		
		 $cid = JRequest::getVar ( 'cid', array (0 ), 'request', 'array' ); 
		 	 $sid = JRequest::getVar ( 'sid',  'request', 'array' ); 
		 	 	 $aid = JRequest::getVar ( 'aid',  'request', 'array' ); 
			
		//echo "<pre>";print_r($cid);exit;
		if (! is_array ( $cid ) || count ( $cid ) < 1) {
			JError::raiseError ( 500, JText::_ ( 'SELECT_AN_ITEM_TO_DELETE' ) );
		}
		
		//$cid = implode( ',', $cid );
		echo   $query = 'DELETE FROM '.$this->_table_prefix.'serviceoption WHERE sid=' .$sid. ' AND aid='.$aid;
		 $db->setQuery($query);
			if(!$db->query()) {
			$this->setError($db->getErrorMsg());

		}

}

}
?>
