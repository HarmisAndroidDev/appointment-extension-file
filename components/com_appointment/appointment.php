<?php
/**
* @package Appointment
* @copyright Copyright (C) 2009-2010 Joomlaextensions.co.in All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
**/ 
defined ('_JEXEC') or die ('Restricted access');
  $controller=JRequest::getVar('view','allorder','request','string');
 $task=JRequest::getVar('task','');

 //echo JPATH_COMPONENT.'/'.'controllers'.'/'.$controller.'.php'; exit;
// require_once(JPATH_SITE . DS ."components". DS ."com_appointment". DS . "helpers" . DS ."lib.vikappointments.php");

$userviews = array('allorder','service_list','employee_list','employee_detail','cart_detail','conform_order');
if(in_array ($controller,$userviews))
{
require_once (JPATH_COMPONENT.'/'.'controllers'.'/'.$controller.'.php');
require_once(JPATH_COMPONENT.'/'."helpers/thumbnail.php");

	
	$classname  = $controller.'controller';
	$controller = new $classname( array('default_task' => 'display') );
	$controller->execute( JRequest::getVar('task','','request','string'));
	$controller->redirect();
}
else
{
	$mainframe = JFactory::getApplication();
	$option = JRequest::getVar('option','','request','string');
	$mainframe->redirect ( 'index.php?option=com_appointment&view=allorder&Itemid=');
}
?>