<?php
 
/**

* @package Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/

defined('_JEXEC') or die('Restricted access');
$option = JRequest::getVar('option','','request','string');
$uri =JURI::getInstance();
$url= $uri->root();
$db= JFactory::getDBO();
$model = $this->getModel ( 'employee_detail' );
$editor = JFactory::getEditor();
JHTML::_('behavior.tooltip');
JHTML::_('behavior.calendar');
$image_path = $url."components/".$option."/assets/emp/images/";
$link="index.php?option=com_appointment&view=employee_detail&task=save";
$document=JFactory::getDocument();
$document->addStyleSheet("components/com_appointment/assets/css/appointment.css");
$row = $this->appointment[0];

?>

<form action="" name="adminForm" id="adminForm" method="post"  onSubmit="return validation();"  enctype="multipart/form-data">
<div class="main">
	<div class="maininside"> 

		<div class="mainimage">
				<img src="<?php echo $image_path.$row->img; ?>"  /> 
     	</div>
		
		<div class="datanameee">
		    	<?php echo $row->fname." ".$row->lname;?>
		</div>

		<div class="datanapro">
				<?php echo $row->group; ?>
		</div>

		<div class="tomandjerry">
				<?php echo $row->contact; ?>
		</div>

	</div>	
</div>

 

<input type="hidden" name="view" value="employee_detail"/>
<input type="hidden" name="cid[]"  id="cid"  value="<?php echo $value->id;?>"/>
<input type="hidden" name="task" id="task" value="save"/>
<input type="hidden" name="option" id="option" value="<?php echo $option;?>"/>
</form>
    
 

    
    
