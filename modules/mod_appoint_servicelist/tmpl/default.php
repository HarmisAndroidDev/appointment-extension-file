    
<?php 
/**
* @package   JE EMCLIENT component
* @copyright Copyright (C) 2016 - 2017 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/ 
defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.formvalidator');
JHtml::_('behavior.formvalidation');
JHTMLBehavior::modal();
$uri =JURI::getInstance();
$url= $uri->root();

$option = 'com_appointment';

$img_path = $url."components/".$option."/assets/images/";

$app = JFactory::getApplication();
$document   = JFactory::getDocument();
$document->addStyleSheet('components/'.$option.'/assets/css/style12.css');

$path=$url.'/'."components/".$option.'/assets/service/images/thumb_';


?>
<div class="componetntdiv ">
   <form action="<?php echo 'index.php?option='.$option.'&view=service_list&limitstart=0'; ?>" method="post" name="adminForm" enctype="multipart/form-data" class="required validate-email form-validate">
  <div class="service_list_main_div">

  <?php
       for($j=0;$j<count($services); $j++)
        {
  ?>
         <div class="service_div"> 
             
                  <div class="divname"> <a href="index.php?option=com_appointment&view=employee_list&ser_id=<?php echo $services[$j]->id;?>"  ><?php echo $services[$j]->name; ?></a></div>
                    <!--  <div class="divprice"><?php echo $services[$j]->price; ?> $</div>-->
                         <?php   
                              $profileImage='';
                              if( $services[$j]->img!='')
                              {
                                 if(file_exists(JPATH_ROOT.'/'.'components/'.$option.'/assets/service/images/'.$services[$j]->img))
                                  {
                                     $profileImage=  $path.$services[$j]->img;
                                  }
                                  else
                                  {
                                      $profileImage=$path."PROFILE2.jpg";
                                  }
                              }
                              else
                              {
                                $profileImage=$path."PROFILE2.jpg";
                              }
                        ?>  
                        <div class="divimage">
                             <img class="im" src="<?php echo $profileImage; ?>" >
                        </div>
                    <!-- <div class="divdesp"><?php echo $services[$j]->description; ?></div>-->
                               </div> 
          <?php

        }
        ?>
        </div>
     
    </div>
    <input type="hidden" name="view" value="service_list" />
    <input type="hidden" name="userid" value="" />
    <input type="hidden" name="option" id="option" value="com_appointment">
    <input type="hidden" name="task" value="save" />
    <input type="hidden" name="id" value="" />
    <input type="hidden" name="boxchecked" value="0" />
    </form>
</div>

