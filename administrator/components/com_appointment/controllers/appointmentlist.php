<?php
 /**

* @package    Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );
class appointmentlistController extends JControllerLegacy
{
	function __construct( $default = array())
	{
		parent::__construct( $default );
		$this->_table_prefix = '#__appointment_';
		$cid = JRequest::getVar('id', array(0), 'post', 'array');
		$task = JRequest::getCmd('task');
	}
	function display($cachable = false, $urlparams = array()) 
	{
			
			parent::display();
		
	}
	function cancel($key=NULL)
	{
		$option=JRequest::getVar('option');
		$this->setredirect('index.php?option='.$option.'$view=appointmentlist');
                                               
	}

	 function saveOrderAjax()
	{
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$pks =$this->input->post->get('id', null,'array');
		$order =$this->input->post->get('order', null,'array');
		$originalOrder = explode(',',$this->input-getString('original_order_values'));
		if(!($order == $originalOrder))
			$model=$this->getModel();
			$return = $model->saveorder($pks,$order);
			if($return)
			{
				echo "1";

			}
	}
	
	
	
}

?>


