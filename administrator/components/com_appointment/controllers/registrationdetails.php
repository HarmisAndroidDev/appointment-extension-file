<?php
 /**

* @package   Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );
jimport('joomla.filesystem.file');
class registrationdetailsController extends JControllerLegacy
{
	function __construct( $default = array())
	{
		$this->_table_prefix = '#__';
		parent::__construct( $default );
	}	
	
	function display($cachable = false, $urlparams = '')
	{

		parent::display();
	}
	
	function edit() 
	{
		JRequest::setVar ( 'view', 'registrationdetails' );
		JRequest::setVar ( 'layout', 'default' );
		JRequest::setVar ( 'hidemainmenu', 1 );
		parent::display ();
	}

	function save() 
	{
		$db	=JFactory::getDBO();
		$model = $this->getModel ( 'registrationdetails' );

		$post = JRequest::get ( 'POST' );
		
		$option = JRequest::getVar('option','','request','string');
			$user	= clone(JFactory::getUser());
		$adminid= $user->id;
		
		$post['adminid']=$adminid;
		$post["description"] = JRequest::getVar( 'description', '', 'POST', 'string', JREQUEST_ALLOWRAW );
			
		 if($post['cid']>0)
		 {
						
	   			$user= clone(JFactory::getUser($post['cid']));
	   			$user->set('id',$post['cid']);
	  			$password=md5($post['password']);
	  			if($post['password']!='')
	  			{
	  				$user->set('password', $password);
	  			}

	  				$user->set('name', $post["username"]);
			 		$user->set('username', $post["username"]);
					$user->set('password', $password);
					$user->set('email', $post["email"]);
					if (!$user->save())
					{
						JError::raiseWarning('', JText::_( $user->getError()));
						return false;
					}
	  	}
	  	else
	  	{
	  			$user	= clone(JFactory::getUser());
				$config		= JFactory::getConfig();
				$authorize	= JFactory::getACL();
				$document   = JFactory::getDocument();
				$usersConfig = JComponentHelper::getParams( 'com_users');
				$newUsertype = $usersConfig->get( 'new_usertype' );
				if ($newUsertype) 
				{
					$newUsertype = 'Administrator';
				}

				$password=md5($post['password']);
				$user->set('id', 0);
				$user->set('name', $post["username"]);
			 	$user->set('username', $post["username"]);
				$user->set('password', $password);
				$user->set('usertype', $newUsertype);
				$user->set('email', $post["email"]);
				
					$jdate = new JDate;
					$user->set('registerDate', $jdate->toSql());
				
				$useractivation = $usersConfig->get( 'useractivation' );
					if($useractivation == '1')
					{
						jimport('joomla.user.helper');
						$user->set('activation',JApplication::getHash( JUserHelper::genRandomPassword()) );
						$user->set('block', '0');
					}
					if(!$user->save())
					{
								JError::raiseWarning('', JText::_( $user->getError()));
								return false;
					}

					$post["user_id"]=$user->id;	
					
					$sql = 'SELECT id FROM #__usergroups WHERE title="Administrator"'; 
					$db->setQuery($sql);
					$res = $db->LoadObject();
					$sql_temp="INSERT INTO #__user_usergroup_map(user_id,group_id) values('".$post['user_id']."','".$res->id."')";
					$db->setQuery($sql_temp);
					$db->Query();
	  	}
			
		$this->setRedirect ( 'index.php?option=' . $option . '&view=registrationlist', $msg );
		
	}


	 function remove() 
	 {
		$post = JRequest::get ( 'POST' );
			$option = JRequest::getVar('option','','request','string');
		
	 			$cid = JRequest::getVar ( 'cid', array (0 ), 'post', 'array' );
	
		
		if (! is_array ( $cid ) || count ( $cid ) < 1) 
		{
			JError::raiseError ( 500, JText::_ ( 'SELECT_AN_ITEM_TO_DELETE' ) );
		}
		
		$model = $this->getModel ( 'registrationdetails' );
		if (! $model->delete( $cid )) 
		{
			echo "<script> alert('" . $model->getError ( true ) . "'); window.history.go(-1); </script>\n";
		}
		$msg = JText::_ ( 'DETAIL_DELETED_SUCCESSFULLY' );
		$this->setRedirect ( 'index.php?option='.$option.'&view=registrationlist',$msg );
	}
	function publish() 
	{
		
		
		$option = JRequest::getVar('option','','request','string');
		
		 $cid = JRequest::getVar ( 'cid', array (0 ), 'POST', 'array' );
		 
		 
		if (! is_array ( $cid ) || count ( $cid ) < 1) 
		{
			JError::raiseError ( 500, JText::_ ( 'SELECT_AN_ITEM_TO_PUBLISH' ) );
		}
		
		$model = $this->getModel ( 'registrationdetails' );
		if (! $model->publish ( $cid, 1 ))
		 {
			echo "<script> alert('" . $model->getError ( true ) . "'); window.history.go(-1); </script>\n";
		 }
		$msg = JText::_ ( 'DETAIL_PUBLISHED_SUCCESSFULLY' );
		$this->setRedirect ( 'index.php?option='.$option.'&view=registrationlist',$msg );
	}
	
	function unpublish() {
		
		$option = JRequest::getVar('option','','request','string');
		
		$cid = JRequest::getVar ( 'cid', array (0 ), 'POST', 'array' );
		
		if (! is_array ( $cid ) || count ( $cid ) < 1) {
			JError::raiseError ( 500, JText::_ ( 'SELECT_AN_ITEM_TO_UNPUBLISH' ) );
		}
		
		$model = $this->getModel ( 'registrationdetails' );
		if (! $model->publish ( $cid, 0 )) {
			echo "<script> alert('" . $model->getError ( true ) . "'); window.history.go(-1); </script>\n";
		}
		$msg = JText::_ ( 'DETAIL_UNPUBLISHED_SUCCESSFULLY' );
		$this->setRedirect ( 'index.php?option='.$option.'&view=registrationlist',$msg );
	}
	
	
	function apply() 

	{
		$db	=JFactory::getDBO();
		$post = JRequest::get ( 'POST' );
	
		$option = JRequest::getVar('option','','request','string');
			$user	= clone(JFactory::getUser());
		$adminid= $user->id;
		
		$post['adminid']=$adminid;
		 if($post['cid']>0)
		 {
	  			$user	= clone(JFactory::getUser($post['cid']));
	  			$user->set('id',$post['cid']);
	  			$password=md5($post['password']);
	  			if($post['password']!='')
	  			{
	  				$user->set('password', $password);
	  			}
	  				$user->set('name', $post["username"]);
			 		$user->set('username', $post["username"]);
					$user->set('password', $password);
					$user->set('usertype', $newUsertype);
					$user->set('email', $post["email"]);
					if ( !$user->save() )
					{
								JError::raiseWarning('', JText::_( $user->getError()));
								return false;
					}
	  			}
	  				else
	  				{
	  	
					$db	=JFactory::getDBO();
					$model = $this->getModel ( 'registrationdetails' );
 
 					$user	= clone(JFactory::getUser());
			
				$config		= JFactory::getConfig();
				$authorize	= JFactory::getACL();
				$document   = JFactory::getDocument();
				$usersConfig = JComponentHelper::getParams( 'com_users');
				$newUsertype = $usersConfig->get( 'new_usertype' );
				if ($newUsertype) 
				{
					$newUsertype = 'Administrator';
				}
				$password=md5($post['password']);
				$user->set('id', 0);
				$user->set('name', $post["username"]);
			 	$user->set('username', $post["username"]);
				$user->set('password', $password);
				$user->set('usertype', $newUsertype);
				$user->set('email', $post["email"]);
				
					$jdate = new JDate;
					$user->set('registerDate', $jdate->toSql());
				
				$useractivation = $usersConfig->get( 'useractivation' );
					if ($useractivation == '1')
					{
						jimport('joomla.user.helper');
						$user->set('activation',JApplication::getHash( JUserHelper::genRandomPassword()) );
						$user->set('block', '0');
					}
					if ( !$user->save() )
					{
								JError::raiseWarning('', JText::_( $user->getError()));
								return false;
					}

				
					$post["user_id"]=$user->id;	

					$sql = 'SELECT id FROM #__usergroups WHERE title="Administrator"'; 
					$db->setQuery($sql);
					$res = $db->LoadObject();
			$sql_temp="INSERT INTO #__user_usergroup_map(user_id,group_id) values('".$post['user_id']."','".$res->id."')";
					$db->setQuery($sql_temp);
					$db->Query();
					}
	
				$msg = JText::_ ( 'DETAIL_SAVED' );
				
		
		$this->setRedirect ('index.php?option='.$option.'&view=registrationdetails&task=edit&cid[]='.$user->id, $msg );
		
	}
	
	function cancel() {
		
		$option = JRequest::getVar('option','com_appointment','request','string');
		$msg = JText::_ ( 'DETAIL_EDITING_CANCELLED' );
		$this->setRedirect ( 'index.php?option='.$option.'&view=registrationlist',$msg );
	}
 
}
?>
