<?php
 /**

* @package Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/

defined('_JEXEC') or die ('restricted access');
jimport('joomla.application.component.view');
class servicesdetailsViewservicesdetails extends JViewLegacy{	
		function display($tpl = null)
		{
			$document =  JFactory::getDocument();
			$document->setTitle( JText::_('servicesdetails') );
			$uri = JFactory::getURI();
			
			$option = JRequest::getVar('option','','request','string');
			$db = jFactory::getDBO();
			$services= $this->get('data');
			$detail	= $this->get('data'); 
			$emp= $this->get('emp');

			$appointment= $this->get('appointment');
			$groups= $this->get('groups');
			$model=$this->getModel('servicesdetails');
			$servicedetail	= $model->getservicedetail($services->id); 
			/*echo '<pre>';
			print_r($student);
			exit;*/
			$isNew = ($services->id < 1);
			$text = $isNew ? JText::_( 'NEW' ) : JText::_( 'EDIT' );
			JToolBarHelper::title(JText::_( 'SERVICES_LIST' ).': <small><small>[ ' . $text.' ]</small></small>');
			JToolBarHelper::apply(); //for save button
			JToolBarHelper::save(); //for save & close bttun
			if ($isNew) 
			{
				JToolBarHelper::cancel(); //for cancell button
			} 
			else 
			{
		
				JToolBarHelper::cancel('cancel', 'Close');
			}
			//FOR A GROUPS
			$temp=array();			
			$temp[]=JHTML::_('select.option','0',JText::_( 'SELECT_GROUPS'));
			$groups=@array_merge($temp,$groups);
		
			$lists['groups'] = JHTML::_('select.genericlist',$groups,'groups_id','class="inputbox" size="1"  ','value','text',$detail->groups); 
			$lists['published'] = JHTML::_('select.booleanlist', 'published', 'class="inputbox"', $detail->published);
			 

			//FOR AN EMPLOYEE
			
			$temp=array();
			$temp[]=JHTML::_('select.option','0',JText::_( 'SELECT_EMPLOYEE'));
			$emp=@array_merge($temp,$emp);
		
			$lists['emp'] = JHTML::_('select.genericlist',$emp,'emp_id','class="inputbox" size="1"  ','value','text',$detail->emp); 
			$lists['published'] = JHTML::_('select.booleanlist',  'published', 'class="inputbox"', $detail->published );
			 

			//FOR A NEW OPTION
			 $temp=array();			
			$temp[]=JHTML::_('select.option','0',JText::_( 'SELECT_NEW_OPTION'));
			$appointment=@array_merge($temp,$appointment);
		
			$lists['appointment'] = JHTML::_('select.genericlist',$appointment,'appointment_id','class="inputbox" size="1"  ','value','text',$detail->appointment); 
			$lists['published'] = JHTML::_('select.booleanlist',  'published', 'class="inputbox"', $detail->published );
			 
			//for dropdrawn menu use joomla code
	

			$this->assignRef('lists',$lists);
			$this->assignRef('services',$services);
			$this->assignRef('lists',		$lists);
			$this->assignRef('detail',		$detail);
			$this->assignRef('servicedetail', $servicedetail);
			$this->assignRef('request_url',$requesturl);
			parent::display($tpl);
 
		}
	
}
?>