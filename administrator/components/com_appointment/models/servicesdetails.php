<?php
 /**

* @package  Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/

defined('_JEXEC') or die('Access Denied');
jimport('joomla.application.component.model');
jimport('joomla.filesystem.file');
class servicesdetailsModelservicesdetails extends JModelLegacy

{

	var $_id = null;
	var $_data = null;
	var $_table_prefix = null;

 function __construct()
 {
 	parent::__construct();
 	$this->_table_prefix='#__appointment_';	 
 	$array=JRequest::getVar('cid',0,'','array');
 	$this->setId((int)$array[0]);

 }

 	function setId($id)
 	{
 		$this->_id=$id;
 		$this->_data=null;

 	}

 	function getData()
 	{
 		if($this->_loadData())
 		{

 		}else  $this->_initData();

 		return $this->_data;

 	}

 	function _loadData()
 	{
 		if(empty($this->_data))
 		{
 		 $query='SELECT * FROM '.$this->_table_prefix.'services WHERE id='.$this->_id; 
 			$this->_db->setQuery($query);
 			$this->_data=$this->_db->loadObject();

 			return (boolean) $this->_data;
  		}
  		return true;

 	}
 	 function _initData()
 	 {
 	 	if(empty($this->_data))
 	 	{
 	 		
 		}
 	}
 	function store($data)
 	{
 		$mylink='';
 		$row=$this->getTable('services');
 		$mainframe=JFactory::getApplication();
 		$option=JRequest::getVar('option');
 		if(!$row->bind($data))
 		{
  			$this->setError($this->_db->getErrorMsg());
			return false;
 		}
 		if(!$row->store())
		{
			$this->setError($this->_db->getErrorMsg());
			return false;

		}
		return $row->id;
		
 	}

 	function getgroups()
		{
			$query = 'SELECT id as value,name as text FROM '.$this->_table_prefix.'groupsdetail';
			$this->_db->setQuery( $query );
			return $this->_db->loadObjectlist();
		}
		function getemp()
		{
			$query = 'SELECT id as value,fname as text FROM '.$this->_table_prefix.'employee';
			$this->_db->setQuery( $query );
			return $this->_db->loadObjectlist();
		}


		//FOR NEW OPTION GROUP
		function getappointment()
		{
			$query = 'SELECT id as value,name as text FROM '.$this->_table_prefix.'addnew';
			$this->_db->setQuery( $query );
			return $this->_db->loadObjectlist();
		}

 	function delete($id= array())
 	{
 		if(count($id))
 		{
 			$ids=implode(',', $id);
 			 $query='DELETE FROM '.$this->_table_prefix.'services WHERE id IN ( '.$ids.' )'; 
 			$this->_db->setQuery( $query );
			if(!$this->_db->query()) {
				$this->setError($this->_db->getErrorMsg());
				return false;
 		}
 	}  
 		return true;
 	}
 	function publish($cid = array(), $publish = 1)
	{		

		if (count( $cid ))
		{
			$cids = implode( ',', $cid );
			
			  $query = 'UPDATE '.$this->_table_prefix.'services '
				. ' SET published = ' . intval( $publish )
				. ' WHERE id IN ( '.$cids.' )';
			$this->_db->setQuery( $query );
			if (!$this->_db->query()) {
				$this->setError($this->_db->getErrorMsg());
				return false;
			}
		}

		return true;
	}


function getCdset()
	{
		
			$query = 'SELECT * FROM '.$this->_table_prefix.'serviceemp WHERE sid='.$this->_id;
			$this->_db->setQuery($query);
			$this->_data = $this->_db->loadObjectList();
			return $this->_data;
	}

function getCdset2()
	{
		
			$query = 'SELECT * FROM '.$this->_table_prefix.'serviceoption WHERE sid='.$this->_id;
			$this->_db->setQuery($query);
			$this->_data = $this->_db->loadObjectList();
			return $this->_data;
	}

	function getemployee($id)
	{
		 $query = 'SELECT fname FROM '.$this->_table_prefix.'employee WHERE id='.$id;
			$this->_db->setQuery($query);
			$this->_data = $this->_db->loadResult();
			return $this->_data;
	}

	function getappointmentdata($id)
	{
		 $query = 'SELECT name FROM '.$this->_table_prefix.'addnew WHERE id='.$id;
			$this->_db->setQuery($query);
			$this->_data = $this->_db->loadResult();
			return $this->_data;
	}
	
	function getservicedetail($id=0)
	{
		
			 $query = 'SELECT * FROM '.$this->_table_prefix.'serviceemp WHERE id='.$this->_id;
			$this->_db->setQuery($query);
			$this->_data = $this->_db->loadObjectList();
			return $this->_data;
	}


	function deleteCdSet($cid = array())
	{
		if (count( $cid ))
		{
			$cids = implode( ',', $cid );
			
			$query = 'DELETE FROM '.$this->_table_prefix.'serviceemp WHERE id IN ( '.$cids.' )';
			//echo $query;exit;
			$this->_db->setQuery($query );
			if(!$this->_db->query()) {
			$this->setError($this->_db->getErrorMsg());
				return false;
			}
		}

		return true;
	}

		function deleteCdSet2($cid = array())
	{
		if (count( $cid ))
		{
			$cids = implode( ',', $cid );
			
			$query = 'DELETE FROM '.$this->_table_prefix.'serviceoption WHERE id IN ( '.$cids.' )';
			//echo $query;exit;
			$this->_db->setQuery($query );
			if(!$this->_db->query()) {
			$this->setError($this->_db->getErrorMsg());
				return false;
			}
		}

		return true;
	}
	
	
}
?>
