<?php
/**
* @package  appointment
* @copyright Copyright (C) 2009-2010 Joomlaextensions.co.in All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
**/

defined('_JEXEC') or die('Restricted access');
 
class mod_appoint_orderHelper
{

public static function getReturnURL()
	{
		$uri = JFactory::getURI();
		$url = $uri->toString(array('path', 'query', 'fragment'));
		return md5($url);
	}
	
	public static function getmaincategory() {
		
		$db = JFactory::getDBO();
		$_table_prefix = '#__appointment_';	
		$sql = "SELECT id,name FROM ".$_table_prefix."services";

		$db->setQuery($sql);
		return $db->LoadObjectList();
	
	
	}

	public static function getsubcategory($mainid) {
		
		$db = JFactory::getDBO();
		$_table_prefix = '#__appointment_';	
		$sql = "SELECT id,name FROM ".$_table_prefix."services WHERE groups=".$mainid;

		$db->setQuery($sql);
		return $db->LoadObjectList();
		
	}
public static function getser()
		{
			$db = JFactory::getDBO();
			$_table_prefix = '#__appointment_';
			$query = 'SELECT id as value,name as text FROM '.$_table_prefix.'services';
			$db->setQuery($query);
			return $db->LoadObjectList();
		}

 public static function getemp()
		{
			$db = JFactory::getDBO();
			$_table_prefix = '#__appointment_';
			$query = 'SELECT id as value,fname as text FROM '.$_table_prefix.'employee';
			$db->setQuery($query);
			return $db->LoadObjectList();
		}

}