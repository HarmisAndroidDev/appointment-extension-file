 <?php
  /**

* @package Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/


defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.tooltip');
//JHTMLBehavior::modal();
$uri = JURI::getInstance();
$url= $uri->root();
$editor = JFactory::getEditor();
//JHTML::_('behavior.calendar');

$option = JRequest::getVar('option','','','string');
$image_path =$url."components/".$option."/assets/service/images/";
$document=JFactory::getDocument();
$document->addStyleSheet("components/com_appointment/assets/css/appointment.css");
?>
<script>
var option='<?php echo $option; ?>';
var url1='<?php echo $url;?>';
</script>
<script language="javascript" type="text/javascript">
Joomla.submitform=function submitbutton(pressbutton)
{
  var form = document.adminForm;
    if (pressbutton)
    {form.task.value=pressbutton;}
    if (pressbutton == 'cancel') 
    {
       form.submit();
    }
    else
    {
    var a = document.adminForm.name.value;
    if(a=="")
    {   
      alert("Please Enter Your Name");
      return false;
    } 
     var b = document.adminForm.duration.value;
    if(b=="")
    {   
      alert("Please Enter Duration");
      return false;
    } 
      var c = document.adminForm.sleeptime.value;
    if(c=="")
    {   
      alert("Please Enter Sleep Time");
      return false;
    } 
       
     var priceval=document.adminForm.price.value;
      var RE = /^\d*\.?\d*$/;            
      if(priceval=="")
      { 
         alert("Please Enter Price");
          return false;
      }
    
     if(!priceval.match(RE))   
     {                
           alert('Wrong...!')  
           return false;  
     } 
    
}
 
var form = document.adminForm;
   if (pressbutton)
    {form.task.value=pressbutton;}
     if ((pressbutton=='publish')||(pressbutton=='unpublish')||(pressbutton=='saveorder'))
     {       
      form.view.value="servicesdetails";
     }
    try {
        form.onsubmit();
        }
    catch(e){}
    
    form.submit();

}
 </script>

<script type="text/javascript" src="<?php echo $url.'/administrator/components/'.$option; ?>/assets/js/main.js">
</script>

<form action="" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
<div class="col50">
  
<fieldset class="adminForm">
<legend><?php echo JText::_('SERVICES_DETAILS');?></legend>
<table class="admintable" border="0" CELLSPACING="20">
     <tr>
         <td><label for="name"><?php echo JText::_('NAME'); ?></label></td>
         <td><input type="text" name="name" placeholder="Enter Name" required value="<?php echo $this->services->name;?>"/></td>
     </tr>

    <tr>
         <td><label for="name"><?php echo JText::_('Duration'); ?></label></td>
         <td><input type="number" name="duration"  placeholder="Enter Duration" required min="0" max="100" step="1" value="<?php echo $this->services->duration;?>"/></td>
     </tr>
     
    <tr>
         <td><label for="name"><?php echo JText::_('sleeptime'); ?></label></td>
         <td><input type="number" name="sleeptime"  placeholder="Enter Sleep Time"  min="0" max="100" step="1" value="<?php echo $this->services->sleeptime;?>"/></td>
     </tr>
    <tr>
         <td><label for="name"><?php echo JText::_('price'); ?></label></td>
         <td><input type="text" name="price" placeholder="Enter Price" required value="<?php echo $this->services->price;?>"/></td>
     </tr>
     <tr>
    <td><label for="name"><?php echo JText::_('IMAGE'); ?></label></td>
    <td><input type="file"   name="img" id="img" value="<?php echo $this->services->img;?>"/></td>
    <tr>
    <?php
       if($this->services) 
      {
        if($this->services->img!='')
        {    
        ?>
      <td>
       <img src="<?php echo $image_path.'thumb_'.$this->services->img;?>" height="100" width="100"/>
      </td>
            
       <?php 
          } 
        else
          {
      ?>
         <td>
        <img src="<?php echo $image_path.'noimagefound.jpg'; ?>" height="100" width="100"/>
        </td>
    <?php
     }
     } 
  ?>
  </tr>
    </tr>

    <tr>
    <td><label for="name"><?php echo JText::_('DESCRIPTION');?></label></td>
    <td style="width: 498px;"><?php $editor = JFactory::getEditor(); ?>
    <?php echo $editor->display("description",$this->services->description,500,'100','20','0'); ?>
    </td>
  </tr>
  <tr>
      <td><label for="name"><?php echo JText::_('SELECT_GROUP');?></label></td>
      <td><?php echo $this->lists['groups'];?></td>
    </tr>
  
  </table>
  
<hr style="border: solid 1px;">
<!--CODE FOR EMPLOYEE AND OPTION-->
<table>
<tr>
<td>
      <table id="cdsets">   
       <tr>
          <td>
            <label for="name"><?php echo JText::_('SELECT_EMPLOYEE');?></label>
            <?php echo $this->lists['emp'];?>&nbsp;<input type="button" id="cdaddvalue" value="<?php echo JText::_('ADD NEW') ;?>" onclick="cdaddNewRow1();"></td> 
              <td>
              <?php 
                   $model = $this->getModel('servicesdetails');
                   $employee=$model->getCdset();
                    if(count($employee)>0)
                     {   
                      for($j=0; $j<count($employee); $j++)
                       {
                          $employeename=$model->getemployee($employee[$j]->eid);


                            $row=$this->employee[$j];
                    ?>
           </td>
           </tr>
            <div id="<?php echo $j;?>">
             
          <tr>
              <td>
                <input type="text" name="empl" id="empl"  readonly="" value="<?php echo $employeename; ?>">
                 <input type="button" name="cdDelete" onclick="cdDelete21(<?php echo $employee[$j]->eid;?>,<?php echo $this->services->id; ?>,this)" value="delete">
                    
                </td>
                <?php
                  }
                ?>
              </tr>
             
         </div>
           <?php

          }
         else{
              $j=1;
          ?>
         <?php 
        }

        ?>
                
      </table>
</td>

<td style="padding-left: 140px;">

<table id="cdsets2">
    <tr>
     <td><label for="name"><?php echo JText::_('SELECT_OPTION');?></label>
      <?php echo $this->lists['appointment'];?>&nbsp;<input type="button" id="cdaddvalue" value="<?php echo JText::_('ADD NEW') ;?>" onclick="cdaddNewRow2();"></td>
       <td>
          <?php 
            $model = $this->getModel('servicesdetails');
            $appointment=$model->getCdset2();
             if(count($appointment)>0)
              {               
                 for($j=0; $j<count($appointment);$j++)
                 {
                  
                  $appointmentname=$model->getappointmentdata($appointment[$j]->aid);                                   
                  $row=$this->appointment[$j];
                            
          ?>
          </td>
        
       <div id="<?php echo $j;?>">
      
          <tr> 
              <td>
                <input type="text" name="app" id="app"  readonly="" value="<?php echo $appointmentname;?>">
                                 
                <input type="button" name="cdDelete" onclick="cdDelete22(<?php echo $appointment[$j]->aid;?>,<?php echo $this->services->id; ?>,this)" value="delete">
                    
                </td>
                <?php
                  }
                ?>
                </tr>
        </tr>
          </div>
                
            <?php

             }
              else{
            $j=1;
      ?>
     
    <?php 
  }

  ?>
   

</table>
</td>
</tr>
</table>
  </fieldset>
</div>  
<div class="clr"></div>
<input type="hidden" name="eid[]" value="<?php echo $this->employee->id; ?>" />
<input type="hidden" name="aid[]" value="<?php echo $this->addnew->id; ?>" />
<input type="hidden" name="cid" id="cid" value="<?php echo $this->services->id; ?>"/>
<input type="hidden" name="task" id="task" value="save"/>
<input type="hidden" name="view" value="servicesdetails"/>
<input type="hidden" name="option" value="<?php echo $option;?>"/>
</form>

