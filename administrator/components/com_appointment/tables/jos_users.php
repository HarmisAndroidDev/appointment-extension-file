<?php
/**
* @package Appointment
* @copyright Copyright (C) 2009-2010 Joomlaextensions.co.in All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
**/ 
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.model');

class Tablejos_users extends JTable
{
	var $id = null;
	
	var $username  = null;
	var $password = null;
	var $email=null;
	
	
	
 	
 	function Tablejos_users($db)
 	 {
 	 	 $this->_table_prefix = '#__appointment_';
 	 	 parent::__construct('#__users','id',$db);
 	 }
 	 function bind($array, $ignore = '')
	{
		if (key_exists( 'params', $array ) && is_array( $array['params'] )) {
			$registry = new JRegistry();
			$registry->loadArray($array['params']);
			$array['params'] = $registry->toString();
		}

		return parent::bind($array, $ignore);
	}

}

?>
