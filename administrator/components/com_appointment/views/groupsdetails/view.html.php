<?php
 /**

* @package  Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/

defined('_JEXEC') or die ('restricted access');
jimport('joomla.application.component.view');
class groupsdetailsViewgroupsdetails extends JViewLegacy{	
		function display($tpl = null)
		{
			$document =  JFactory::getDocument();
			$document->setTitle( JText::_('groupsdetails') );
			$uri = JFactory::getURI();
			
			$option = JRequest::getVar('option','','request','string');
			$db = jFactory::getDBO();
			$groups1= $this->get('data');
				$detail	= $this->get('data'); 
				$groups= $this->get('groups');
			/*echo '<pre>';
			print_r($student);
			exit;*/
			$isNew = ($groups->id < 1);
			$text = $isNew ? JText::_( 'NEW' ) : JText::_( 'EDIT' );
			JToolBarHelper::title(JText::_( 'GROUPS_LIST' ).': <small><small>[ ' . $text.' ]</small></small>');
			JToolBarHelper::apply(); //for save button
			JToolBarHelper::save(); //for save & close bttun
			if ($isNew) 
			{
				JToolBarHelper::cancel(); //for cancell button
			} 
			else 
			{
		
				JToolBarHelper::cancel( 'cancel', 'Close' );
			}
			$temp=array();
			
			$temp[]=JHTML::_('select.option','0',JText::_( 'SELECT_GROUP'));
			$groups=@array_merge($temp,$groups);
			/*echo '<pre>';
			print_r($groups);
			exit;*/
			$lists['groups'] = JHTML::_('select.genericlist',$groups,'groups_id','class="inputbox" size="1"  ','value','text',$detail->groups); 
			$lists['published'] = JHTML::_('select.booleanlist',  'published', 'class="inputbox"', $detail->published );
			 
			//for dropdrawn menu use joomla code
	

			//$this->assignRef('lists',$lists);
			$this->assignRef('groups1',$groups1);
			$this->assignRef('lists',$lists);
			$this->assignRef('detail',		$detail);
			$this->assignRef('request_url',$requesturl);
			parent::display($tpl);
 
		}
	
}
?>