<?php
 /**

* @package Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/

defined('_JEXEC') or die ('restricted access');
jimport('joomla.application.component.view');
class registrationdetailsViewregistrationdetails extends JViewLegacy{	
		function display($tpl = null)
		{
			$document =  JFactory::getDocument();
			$document->setTitle( JText::_('registrationdetails') );
			$uri = JFactory::getURI();
			
			$option = JRequest::getVar('option','','request','string');
			$db = jFactory::getDBO();
			$appointment= $this->get('data');
			/*echo '<pre>';
			print_r($student);
			exit;*/
			$isNew = ($appointment->id < 1);
			$text = $isNew ? JText::_( 'NEW' ) : JText::_( 'EDIT' );
			JToolBarHelper::title(JText::_( 'NEW_LIST' ).': <small><small>[ ' . $text.' ]</small></small>');
			JToolBarHelper::apply(); //for save button
			JToolBarHelper::save(); //for save & close bttun
			if ($isNew) 
			{
				JToolBarHelper::cancel(); //for cancell button
			} 
			else 
			{
		
				JToolBarHelper::cancel( 'cancel', 'Close' );
			}
			
			 
			//for dropdrawn menu use joomla code
	
	
			//$this->assignRef('lists',$lists);
			$this->assignRef('user',$appointment);
			$this->assignRef('request_url',$requesturl);
			parent::display($tpl);
 
		}
	
}
?>