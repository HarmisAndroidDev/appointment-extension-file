<?php
 /**

* @package Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/

defined('_JEXEC') or die('Access Denied');
 $option=JRequest::getVar('option','','','string');
 $filter=JRequest::getVar('filter'); 
$model =$this->getModel('employeelist');

$listOrder=$this->escape($this->lists['order']);
$listDirn=$this->escape($this->lists['order_Dir']);
$saveOrder=$listOrder == 'ordering';

if($saveOrder)
{
	$saveOrderingUrl ='index.php?tmpl=component&option='.$option.'&view=employeelist&task=saveOrederAjax';
	$saveOrderingUrl ='index.php?tmpl=component&option='.$option.'&view=employeelist&task=saveOrederAjax';
	JHtml::_('sortablelist.sortable','list','adminForm',strtolower($listDirn), $saveOrderingUrl);
}
$sortFields = $this->getSortFields();
$search=JRequest::getVar('search','','request','string');
?>
<script type="text/javascript">
	Joomla.orderTable=function()
	{
		table=document.getElementById("sortTable");
		direction=document.getElementById("directionTable");
		order=table.options[table.selectedIndex].value;
		if(order !='<?php echo $listOrder; ?>')
		{
			dirn='asc';
		}else
		{
			dirn=direction.options(order,dirn,'');
		}
			Joomla.tableOrdering(order,'dirn','');
	}
</script>
<script type="text/javascript">

Joomla.submitform=function submitform(pressbutton)
{
	//alert(pressbutton);
	var form =document.adminForm;
	if(pressbutton)
	{
		form.task.value=pressbutton;
	}
	if((pressbutton=='add') || (pressbutton=='edit') || (pressbutton=='remove') || (pressbutton=='copy') || (pressbutton=='publish') || (pressbutton=='unpublish'))
	{
		form.view.value="employeedetails";

	}	
	try
	{
		form.onsubmit();
	}
	catch(e){}
	form.submit();
}

function mysubmit_form(adminForm_id)
{
	var form=document.adminForm;
	form.submit();
}
function get_select()
 {

		var form = document.adminForm;
		form.submit();
}
	function selectsearch()  //This is for publish or unpublish search defined in veiw.html
	{	
		var form = document.adminForm;

		form.submit();
	}
		function cleardata(gfd)
		{
		var form = document.adminForm;
		document.getElementById(gfd).value = '';
		form.submit();
		}

</script>
<form action="<?php echo 'index.php?option='.$option;?>" method="POST" name="adminForm" id="adminForm" enctype="multipart/form-data">
<div id="editcell">
	<table align="right">
   	<tr>
   		<td>
   			<div id="filter-bar" class="btn-toolbar">
   			<div class="filter-search btn-group pull-left">
   				<label for="filter_search" class="element-invisible"><?php echo JText::_( 'COM_JEAPPOIMENTFILTER' ); ?>:</label>
   				<input type="text" name="search" id="filter_search" style="width:150px;" value="<?php echo $search;?>" />
   			</div>
   			<div class="btn-group pull-left hidden-phone">
				<button class="btn hasTooltip" type="submit" title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>"><i class="icon-search"></i></button>
				<button class="btn hasTooltip" type="button" title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>" onclick="cleardata('filter_search').value='';this.form.submit();"><i class="icon-remove"></i></button>
			</div>
   			</div>
   		</td>
   		 <td> <?php echo $this->searchlists['published']; ?></td>
   		 <td>
    
            <div id="filter-bar" class="btn-toolbar">
			<div class="btn-group pull-right hidden-phone">
				<label for="limit" class="element-invisible"><?php JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC');?></label>
				 <?php echo $this->pagination->getLimitBox(); ?>
			</div>
	</div> 
</td>
   	</tr>
   </table>

   <br/>
<table id="list" class="table table-striped" style="background:#F5F5F5; border: 2px solid #EEE;">
	<thead>
		<tr>
		<th width="1%" class="nowrap center hidden-phone">
			<?php echo JHtml::_('grid.sort', '<i class="icon-menu-2"></i>', 'ordering', $listDirn, $listOrder, null, 'asc', 'JGRID_HEADING_ORDERING'); ?></th>
			<th width="5%"><?php echo JText::_('NUM')?></th>	
			<th width="5%"><input type="checkbox" name="toggle" onclick="Joomla.checkAll(this);"/></th>
		<th width="5%"><?php echo JHtml::_('grid.sort','ID','id',''); ?></th>
		<th width="5%"><?php echo JHtml::_('grid.sort','NAME','fname',''); ?></th>
		<th width="5%"><?php echo JHtml::_('grid.sort','PUBLISHED','published',$this->lists['order_Dir'],$this->lists['order']); ?></th>
			
		</tr>
	</thead>
<?php
$k=0;
for($i=0, $n=count($this->employeelist); $i<$n; $i++)
{
	$row=$this->employeelist[$i];

	$row->id=$row->id;
	$link=JRoute::_('index.php?option='.$option.'&view=employeedetails&task=edit&cid[]='. $row->id );
	$published = JHTML::_('grid.published',$row,$i);

?>
<tr class="row<?php echo $i%2;?>" id="<?php echo $row->id;?>">


<td class="order nowrap center hidden-phone" align="center"><?php
	$disableClassName='';
	$disabledLabel ='';
	if(!$saveOrder) :
			$disabledLabel =JText::_('JORDERINGDISABLED');
		$disableClassName = 'inactive tip-top';
		endif;
		?>
		<span class="sortable-handler hasTooltip <?php echo $disableClassName; ?>" title="<?php echo $disabledLabel; ?>">
				<i class="icon-menu"></i>
				</span>
		<input type="text"  style="display:none" name="order[]" value="<?php echo $row->ordering; ?>" />
		<td class="order" align="center"><?php echo $this->pagination->getRowOffset($i);?></td>
		<td align="center"><?php echo JHTML::_('grid.id',$i,$row->id);?></td>
		<td align="center"><?php echo $row->id;?></td>
		<td align="center"><a href="<?php echo $link?>" title="<?php echo JText::_('EDIT_EMPLOYEE');?>" ><?php echo $row->fname;?></a></td>
		<td align="center"><div class="btn-group"><?php echo JHtml::_('jgrid.published',$row->published,$i,'','cb');?></div></td>
	</tr>

	<?php
		$k=1-$k;
	}
	?>
	<tfoot>
		<td colspan="10"><?php echo $this->pagination->getListFooter(); ?> </td>
	</tfoot>
</div>
</table>
<input type="hidden" name="view" value="employeelist" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="filter_order_Dir" id="filter_order_Dir" value="<?php echo $listDirn; ?>" />
<input type="hidden" name="filter_order" id="filter_order" value="<?php echo $listOrder; ?>" />

<?php echo JHTML::_( 'form.token' ); ?>
</form>
