<?php
 /**

* @package Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/

defined('_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.model');
class appointmentlistModelappointmentlist extends JModelLegacy
{	          
    var $_data = null;
    var $_total = null;
    var $_pagination = null;
    var $_table_prefix = null;
    
   function __construct()
	{			
		parent::__construct();
		global $context;
		$mainframe	= JFactory::getApplication();
		$this->_table_prefix = '#__appointment_';
		$array = JRequest::getVar('id',  0, '', 'array');	
		$limit= $mainframe->getUserStateFromRequest( $context.'limit', 'limit', $mainframe->getCfg('list_limit'), 0);
    	 $limitstart = $mainframe->getUserStateFromRequest( $context.'limitstart', 'limitstart', 0 );
    	 $this->setState('limit', $limit);
    	 $this->setState('limitstart', $limitstart);	
	
	}
	
	function getData()
	{
		if(empty($this->data))
		{
			$query=$this->_buildQuery();
			$this->_data=$this->_getList($query,$this->getState('limitstart'),$this->getState('limit'));
		}
		return $this->_data;

	}

	function getTotal()
	{
		if(empty($this->_total))
		{
			$query=$this->_buildQuery();
			$this->_total=$this->_getListCount($query);

		}
		return $this->_total;
	}

	function getPagination()
	{
		if(empty($this->_pagination))
		{

			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination ($this->getState('limitstart'),$this->getState('limit'));

		}
		return $this->_pagination;
	}

	function _buildQuery()
		{
			$orderby=$this->_buildContentOrderBy();
			$squery='';
			$name=JRequest::getVar('search','','request','string');
			$myselpublished = JRequest::getVar('published','-1');
			if($name!='')
			{
				 $squery.="AND name Like '%".$name."%'";

			}
			if($myselpublished!='-1')
			{
				 $squery.=' AND published='.$myselpublished;
			}
		 	 $query = ' SELECT * '.' FROM '.$this->_table_prefix.'addnew WHERE 1=1 ' .$squery.$orderby;
			return $query;
		}
	function _buildContentOrderBy()
		{
			$mainframe=JFactory::getApplication();

			global $context;

			$filter_order = $mainframe->getUserStateFromRequest( $context.'filter_order',      'filter_order', 	  'id' );
			$filter_order_Dir = $mainframe->getUserStateFromRequest( $context.'filter_order_Dir',  'filter_order_Dir', '' );						
			$orderby = ' ORDER BY '.$filter_order.' '.$filter_order_Dir;					
			return $orderby;


		}
		
}	


?>
