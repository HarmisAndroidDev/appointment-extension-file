 <?php
  /**

* @package Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/

defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.tooltip');
$uri = JURI::getInstance();
$url= $uri->root();
$editor = JFactory::getEditor();
$document=JFactory::getDocument();
$document->addStyleSheet("components/com_appointment/assets/css/appointment.css");
$option = JRequest::getVar('option','','','string');
$image_path =$url."components/".$option."/assets/images/";
?>
<script>
var option='<?php echo $option; ?>';
var url1='<?php echo $url;?>';
</script>

<script type="text/javascript" src="<?php echo $url.'/administrator/components/'.$option; ?>/assets/js/main.js">
</script>

<script language="javascript" type="text/javascript">
Joomla.submitform=function submitbutton(pressbutton)
{
 
 var form = document.adminForm;
    if (pressbutton)
    {
      form.task.value=pressbutton;
    }
    
    if (pressbutton == 'cancel') 
    {
       form.submit();
    }
    
    else
    {
      var a = document.adminForm.name.value;
      if(a=="")
      {   
        alert("Please Enter Your Name");
        return false;
      } 
    
    if(!isNaN(a))
    {
      alert("Please Enter Only Characters");
      return false;
    }

     var priceval=document.adminForm.price.value;
     var RE = /^\d*\.?\d*$/;            
     
      if(priceval=="")
      { 
         alert("Please Enter Price");
         return false;
      }
    
     if(!priceval.match(RE))   
     {                
           alert('Wrong...!')  
           return false;  
     } 
}
var form = document.adminForm;
   if(pressbutton)
    {
      form.task.value=pressbutton;
    }
     
    if ((pressbutton=='publish')||(pressbutton=='unpublish')||(pressbutton=='saveorder'))
    {       
        form.view.value="appointmentdetails";
    }
    try 
    {
        form.onsubmit();
    }
    catch(e)
    {}
    
    form.submit();
}
</script>

<form action="" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
<div class="col50">
<fieldset class="adminForm">
<legend><?php echo JText::_('NEW_DETAILS');?></legend>
<table cellpadding="5px" cellspacing="5px">
<tr>
<td style="width: 571px;">
<table >
       <tr>
         <td><label for="name"><?php echo JText::_('NAME'); ?>*</label></td>
         <td><input type="text" name="name" placeholder="Enter Name" value="<?php echo $this->addnew->name;?>"/></td>
       </tr>
       <tr>
          <td><label for="name"><?php echo JText::_('DESCRIPTION');?></label></td>
          <td><?php $editor = JFactory::getEditor();  ?>
          <?php echo $editor->display("description",$this->addnew->description,100,'50','50','0'); ?>
          </td>
        </tr>
         <tr>
           <td><label for="name"><?php echo JText::_('PRICE')?>*</label></td>
            <td><input type="text" name="price" placeholder="Enter Price" value="<?php echo $this->addnew->price;?>"/></td>
         </tr>
          <tr>
              <td><label for="name"><?php echo JText::_('IMAGE'); ?></label></td>
              <td><input type="file"   name="img" id="img" value="<?php echo $this->addnew->img;?>"/></td>
          <tr>
    
            <?php
               if($this->addnew) 
                {
                
                    if($this->addnew->img!='')
                        {    
                ?>
                <td>
               <img src="<?php echo $image_path.'thumb_'.$this->addnew->img;?>" height="100" width="100"/>
              </td>
                  
             <?php 
                 }  
                 else
                 {
            ?>
                <td>
              <img src="<?php echo $image_path.'noimagefound.jpg'; ?>" height="100" width="100"/>
              </td>
          <?php
                 }
           } 
          ?>
        </tr>
      </tr>

  </table>
  </td>
    <td>
 <!-- code For Variation part-->
<div>
  <table id="cdsets" cellspacing="5px" cellpadding="5px" >
    <tr>
       <td>
          <?php echo JText::_('Use this button to add new variation'); ?>
         <input type="button" id="cdaddvalue" value="<?php echo JText::_('ADD NEW:') ;?>" onclick="cdaddNewRow();">
        </td>
     </tr>
     <tr>
                  <th><?php echo JText::_('Variation Name');?></th>
                  <th><?php echo JText::_('Price');?></th>
                  <th><?php echo JText::_('$');?></th>
                  <th><?php echo JText::_('Delete');?></th>
      </tr>

     <tr>
        <td>
          <?php 
          // echo '<pre>';
          // print_r($this->cdset);
          // exit;
             if(count($this->variation)>0)
             { 
                for($i=0;$i<count($this->variation);$i++)
                { 
                   $row=$this->variation[$i];
                                          
          ?>
       <div id="<?php echo $i;?>">

       <table>
          <tr>
              <td>
                           
              </td>
              <td>
                <input type="text" name="cdname[]" id="cdname" value="<?php echo $row->vname; ?>">
                <input type="text" name="price1[]" id="price1" value="<?php echo $row->vprice;?>">
                     
                <input type="hidden" name="cdid[]" value="<?php echo $row->vid;?>">
                <input class="button" type="button" name="cdDelete" onclick="cdDelete2(<?php echo $row->vid;?>,this)" value="delete">
                    
                </td>
                </tr>
       </table>
          </div>
                <?php 
                }
                ?>
           </td>
              </tr>
            <?php

    } else{
            $j=1;
      ?>
      
        <tr>

          <td>
            <input type="text" name="cdname[]"  id="cdname" placeholder="Enter Variation Name">
            <input type="hidden" name="cdid[]" value="0">
            <input type="text" name="price1[]" id="price1" placeholder="Enter Variation Price" />
            <b>$</b>
             <input class="button" type="button" name="cdDelete" onclick="cdDelete2(<?php echo $row->vid; ?>,this)" value="delete">
            
      </tr>
    <?php 
  }
  ?>

        </table>
       </div>


  </fieldset>
</div>  
<div class="clr"></div>
<input type="hidden" name="cid[]" id="cid[]" value="<?php echo $this->addnew->id; ?>"/>
<input type="hidden" name="task" id="task" value="save"/>
<input type="hidden" name="view" value="appointmentdetails"/>
<input type="hidden" name="option" value="<?php echo $option;?>"/>
</form>
