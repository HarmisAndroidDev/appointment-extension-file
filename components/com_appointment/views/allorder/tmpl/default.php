<?php
/**

* @package  Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/
defined('_JEXEC') or die('Restricted access');
$option = JRequest::getVar('option','','request','string');
$uri = JURI::getInstance();
$uri= JFactory::getURI();
$url= $uri->root();
$db= JFactory::getDBO();
$model = $this->getModel ( 'allorder' );
$editor = JFactory::getEditor();
JHTML::_('behavior.tooltip');
JHTML::_('behavior.calendar');
$image_path = $url."components/".$option."/images/";
$link="index.php?option=com_appointment&view=allorder&task=save";
$document=JFactory::getDocument();
$document->addStyleSheet("components/com_appointment/assets/css/appointment.css");
$document->addScript("components/com_appointment/assets/js/min.js");
$emp_id=JRequest::getVar('emp_id','0','request','string');
$ser_id=JRequest::getVar('ser_id','0','request','string');



?>
<!DOCTYPE html>
<html class="" slick-uniqueid="3" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" dir="ltr" lang="en-gb"><head>
 <script>
var option='<?php echo $option; ?>';
var url1='<?php echo $url;?>';
</script>

</script> 

<script type="text/javascript" src="<?php echo $url.'/components/'.$option; ?>/assets/js/min.js">
</script>
<form action="" name="adminForm" id="adminForm" method="post"  onSubmit="return validation();"  enctype="multipart/form-data">
 
 <table>
<tr>
<td>

    <?php echo $this->lists['month_list']; ?>  

</td>
<?php

		$hide = JRequest::getVar('hide','','request','string');
		if($hide == 1)
			{
				 $style="display:block";
			}
			else
			{
				 $style="display:none";
			}
?>
	
			<td style="<?php echo $style; ?>">
				<?php echo $this->lists['category']; ?>
			<td> 
   
</td>
</tr>
</table> 

 <div id="calenderdiv">

<?php 
		$dbo = JFactory::getDBO();
		$option = JRequest::getVar('option','','request','string');
		$ser_id=0;
		if($this->firstservice>0)
		{
			$ser_id = $this->firstservice;
		}
		else
		{
			$ser_id = JRequest::getVar('ser_id','','request','string');
		}
		$emp_id = JRequest::getVar('emp_id','0','request','string');
		$now = new DateTime('NOW');
        $cid = $now->format("F");

		$now = new DateTime('NOW');
		$year = $now->format("Y");

		$defaultdate='01-'.trim($cid).'-'.date("Y"); 
			
		$date = new DateTime($defaultdate);
		$times= $date->getTimestamp();
	
		echo "Service Price:"; ?>
	<tr>
		<td> <?php echo $this->price; ?> </td>
	</tr>
<?php

		$next=$defaultdate."next month"; 
			
		 $months=array();
		 $months[]=$cid;
	
		$nextmonth = date('F',strtotime($next));

		$months[]=$nextmonth;
		$nextmonth1 = date('F', strtotime('+1 month', strtotime($nextmonth)));

		$months[]=$nextmonth1;

		$next=$defaultdate."last day of this month"; 
		$nextmonth = date('Y-m-d',strtotime($next));
			
		$first=date('w', strtotime($defaultdate));
		?>
			<table>

			<tr>
			<?php

		foreach($months as $key=>$month)
		{
			
		 	$defaultdate='01-'.trim($month).'-'.date("Y");
			$next=$defaultdate."last day of this month"; 
			$lastdate = date('d',strtotime($next)); 
			
		    $curdate=date('l',strtotime(''.$month.' '.date("Y"))); 

			$firstday = 1;
			$year = (new DateTime)->format("Y");
			
			if(strtoupper($curdate) == 'SUNDAY'){ $firstday = 1;}
			else if(strtoupper($curdate) == 'MONDAY'){ $firstday=2;}
			else if(strtoupper($curdate) == 'TUESDAY'){ $firstday=3;}
			else if(strtoupper($curdate) == 'WEDNESDAY'){ $firstday=4;}
			else if(strtoupper($curdate) == 'THURSDAY'){ $firstday=5;}
			else if(strtoupper($curdate) == 'FRIDAY'){ $firstday=6;}
			else if(strtoupper($curdate) == 'SATURDAY'){ $firstday=7;}
			?>
			<td>
			<table border="1">
					
					<tr class="monthhead">
						<td colspan="7" align="center" ><?php echo $month; ?>-<?php echo $year; ?></td>
					</tr>
					<tr>
						<td>Sun</td>
						<td>Mon</td>
						<td>Tue</td>
						<td>Wed</td>
						<td>Thu</td>
						<td>Fri</td>
						<td>Sat</td>
						
					</tr>
					<?php
					$cont=0;
					$tedate = 1;
					$null=0;
					
					$lastdate = $lastdate + $firstday;
					$ddatear = array();
					
					for($i=1;$i<$lastdate;$i++)
					{	

					 $ddate = $tedate.'-'.trim($month).'-'.date("Y"); 
										 	
						 $dayName =date("l",strtotime($ddate));

							if($cont>6)
							{	?>
								<tr class="data">
								<?php
							}	
							if($i >= $firstday)
							{	
								
								if(in_array($dayName, $this->workday))
								{	

									?>

					<td align="center" style="background-color:rgb(2,174,54);"><a  href="javascript:void(0);"  onclick="return book('<?php echo $dayName; ?>','<?php echo $tedate; ?>','<?php echo $year; ?>','<?php echo $this->price; ?>','<?php echo $month; ?>','<?php echo $ser_id; ?>')"><?php echo $tedate ;?></a></td>
					<?php
								}

								else
								{ ?>
									<td align="center"><?php echo $tedate; ?></td>
									<?php	
								}
								
								$tedate++;	
																
							}	
							else
							{ ?>
								<td></td>
								<?php
							}
							if( $cont > 6 ) 
							{	'</tr>';
								$cont = 0;
							}	
							$cont++;
													
					} ?>
			</table></td>	
			<?php			

		}?>
	</tr></table>
	<?php
	?>
	
</div> 
<div id="timediv"  > 
 <?php echo $_GLOBAL['timediv']; ?>

</div>

<div id="addcart">


</div>
<input type="hidden" name="view" value="allorder"/>
 <input type="hidden" name="cid[]"  id="cid[]"  value="<?php echo $this->appointment->id;?>"/> 
<input type="hidden" name="task" id="task" value="save"/>
<input type="hidden" name="emp_id" id="emp_id" value="<?php echo $emp_id; ?>"/>
<input type="hidden" name="ser_id" id="ser_id" value="<?php echo $ser_id; ?>"/>
<input type="hidden" name="empname" id="empname" value="<?php echo $this->lists['empname']; ?>"/>
<input type="hidden" name="sername" id="sername" value="<?php echo $this->lists['sername']; ?>"/>
</form>
 
 <!-- 
<script type="text/javascript">
mymonth();
</script> -->
 <script> jQuery( document ).ready(function() { mymonth(); }); </script>
    
