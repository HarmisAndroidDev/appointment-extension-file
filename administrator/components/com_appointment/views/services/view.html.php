<?php
 /**

* @package Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/

defined( '_JEXEC' ) or die( 'Restricted access' );


jimport( 'joomla.application.component.view' );

class servicesViewservices extends JViewLegacy
{
	function __construct( $config = array())
	{
		 parent::__construct( $config );
	}
    
	function display($tpl = null)
	{	
		$mainframe = JFactory::getApplication();
		$context='services';
		
		$document =  JFactory::getDocument();
		$document->setTitle( JText::_('services'));
   		jimport('joomla.html.pagination');
   		
   		JToolBarHelper::title(   JText::_( 'SERVICES_MANAGEMENT' )  );
     	JToolBarHelper::addNew();
 		JToolBarHelper::editList();		
		JToolBarHelper::deleteList();
		JToolBarHelper::publishList();
		JToolBarHelper::unpublishList();		
  		$uri	= JFactory::getURI();
		$filter_order     = $mainframe->getUserStateFromRequest( $context.'filter_order',      'filter_order', 	  'ordering' );
		$filter_order_Dir = $mainframe->getUserStateFromRequest( $context.'filter_order_Dir',  'filter_order_Dir', '' );
		$services = $mainframe->getUserStateFromRequest( $context.'services','services',0 );
		$limitstart     = $mainframe->getUserStateFromRequest( $context.'limitstart',      'limitstart', 	  '0' );
		$limit = $mainframe->getUserStateFromRequest( $context.'limit',  'limit', '10' );
		$lists['order'] 		= $filter_order;  
		$lists['order_Dir'] 	= $filter_order_Dir;
		$lists['order'] 	= $filter_order;  
		$lists['order_Dir'] = $filter_order_Dir;


		 //$category_id= JRequest::getVar('catid', '0','request','int'); //we need some catogoryid so 1st add the field in product databse category_id
		 $total =  $this->get( 'Total');
		 //echo '<pre>';
		 //print_r($total);
		 //exit;
		$services=  $this->get( 'Data');
		//This is for select category
		/*$category	= $this->get('Category');  
		$sel_category = array();
		$sel_category[] = JHtml::_('select.option', '0', JText::sprintf('Select Category'));
		$category	= @array_merge($sel_category,$category);
		$lists['catid'] = JHTML::_('select.genericlist',$category, 'catid', 'class="inputtext" onchange=myform(this.value);', 'value', 'text',$category_id);
*/
		//$searchlists	= array();

	    
		$pagination = new JPagination( $total, $limitstart, $limit);
		//$sel_cat	= array();
		//$allcategory	= $this->get('category');
		//$sel_cat[]  = JHTML::_('select.option', '0 ', JText::_( 'SELECT_CATEGORY'));
		//$sel_cat[0]->value	= '0';
		//$allcategory	= @array_merge($sel_cat,$allcategory);
		//$sel_catid		= JRequest::getVar('cat_id', '0','request','int');
		//$searchlists['catname'] = JHTML::_('select.genericlist',$allcategory,'cat_id', 'class="inputbox" size="1" onchange="selectsearch(this.value)"', 'value', 'text',$sel_catid);
		$publish_op = array();
		$publish_op[]   	= JHTML::_('select.option', '-1',JText::_('SELECT_STATE'));
		$publish_op[]   	= JHTML::_('select.option', '1',JText::_('PUBLISHED'));
		$publish_op[]   	= JHTML::_('select.option', '0', JText::_('UNPUBLISHED'));
		$published	= JRequest::getVar('published', '-1','request','int');
		/*$searchlists['published'] = JHTML::_('select.genericlist',$publish_op,'published','class="inputbox" size="1" onchange="selectsearch(this.value)" ','value','text' ,$published); 
		$lists['id'] = JHTML::_('select.genericlist', $services, 'id', 'class="inputbox" onchange="adminForm.submit()" ', 'value', 'text', $id );*/
		$requesturl = $uri->toString(); 
		$this->assignRef('lists',		$lists);
		$this->assignRef('searchlists',	$searchlists);   
  	   	$this->assignRef('pagination',	$pagination);	
  	   	$this->assignRef('services',	$services); 		    
		$this->assignRef('request_url',	$requesturl);    		  	
   
   	 	parent::display($tpl);
  }
  		  protected function getSortFields(){
			return array(
				'ordering' => JText::_('JGRID_HEADING_ORDERING'),
				'published' => JText::_('JSTATUS'),
				'name' => JText::_('JGLOBAL_TITLE')
			);
		}
		

	}
  
  
  
