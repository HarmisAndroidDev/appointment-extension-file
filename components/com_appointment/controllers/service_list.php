<?php
 /**

* @package Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/
defined ( '_JEXEC' ) or die ( 'Restricted access' );
jimport ( 'joomla.application.component.controller' );
jimport( 'joomla.filesystem.file' );

class service_listController extends JControllerLegacy 
{

	
	function __construct($default = array()) 
	{
		parent::__construct ( $default );
		$this->_table_prefix = '#__bundi_';
	}
	
	function edit() 
	{
		JRequest::setVar ( 'view', 'categories' );
		JRequest::setVar ( 'layout', 'default' );
		JRequest::setVar ( 'hidemainmenu', 1 );
		parent::display ();
	}
	
	function save() 
	{
		$model = $this->getModel ( 'staff' );
		$app	= JFactory::getApplication();
		$post = JRequest::get ( 'post' );
		$option = JRequest::getVar('option','','request','string');
		$db = JFactory::getDBO();
		$user_id=$post['userid'];
		$fullname=trim($post['fname']." ".$post['lname']);
		$pass="";
		if($user_id==0)
		{
			$pass=$this->randomString(5);
			$password=md5($pass);
		}
		else
		{
			$sql = 'SELECT password FROM #__users WHERE id='.$user_id; 
			$db->setQuery($sql);
			$password = $db->LoadResult();
		}
		$db = JFactory::getDBO();    
        $user       = clone(JFactory::getUser());   
		$user->set('id', $user_id);
		$user->set('name', addslashes($fullname));
		$user->set('username', addslashes($post['username']));
		$user->set('password', $password);
		$user->set('email', addslashes($post['email']));
		$user->set('block', '0');
		$user->set('activation', '0');
		
			$post['userid']=$user->id;
			$row=$model->store($post);
	} 
	
    function remove()
    {
    	$option = JRequest::getVar('option','','request','string');
		$cid = JRequest::getVar('cid',  '', 'request', 'string');
		$db = JFactory::getDBO(); 
		$app	= JFactory::getApplication();
		$res=0;
		if(!empty($cid))
		{
			$query ='DELETE FROM '.$this->_table_prefix.'staffs WHERE userid IN ('.$cid.')';
		    $db->setQuery($query);
		    $db->query();

		    $query ='DELETE FROM '.$this->_table_prefix.'usertypes WHERE userid IN ('.$cid.')';
		    $db->setQuery($query);
		    $db->query();

		    $query ='DELETE FROM #__user_usergroup_map WHERE user_id IN ('.$cid.')';
		    $db->setQuery($query);
		    $db->query();

			$query ='DELETE FROM #__users WHERE id IN ('.$cid.')';
		    $db->setQuery($query);
		    $res=$db->query();
			$nItemId ="";
		}
	    if($res)
		{
			$msg=JText::_('STAFF_DETAIL_HAS_BEEN_DELETED_SUCCESS');
			$app->redirect(JRoute::_('index.php?option='.$option.'&view=staffs&Itemid='.$nItemId, $msg));
		}		
		else
		{
			$msg=JText::_('STAFF_DETAIL_HAS_BEEN_NOT_DELETED_SUCCESS');
			$app->redirect(JRoute::_('index.php?option='.$option.'&view=staffs&Itemid='.$nItemId, $msg));
		}		
    }
}
?>