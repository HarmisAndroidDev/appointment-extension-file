 <?php
/**

* @package Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
**/

defined('_JEXEC') or die('Restricted access');
$option = JRequest::getWord('option','','request');
$uri = JURI::getInstance();
$ser_id = JRequest::getVar('ser_id','','','int');
$url= $uri->root();
$img_src = JRoute::_('components/com_jeappointment/assets/images/');

$document = JFactory::getDocument();


$document->addStyleSheet("components/com_appointment/assets/css/appointment.css");
$document->addScript("components/com_appointment/assets/js/min.js");

?>

<script type="text/javascript">
function check()
{
	var a = document.adminForm.fname.value;
    if(a=="")
    {   
      alert("Please Enter Your First Name");
      return false;
    }

    var b = document.adminForm.lname.value;
    if(b=="")
    {   
      alert("Please Enter Your Last Name");
      return false;
    } 

    var Email = document.adminForm.email.value;
    var atpos = Email.indexOf("@");
    var dotpos = Email.lastIndexOf(".");   
	if(Email=="")
	{	
		alert("Please Enter EmailId");
		return false;
	}
				
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=Email.length)
    {
    	alert("Not a valid e-mail address");
        return false;		
    }	
    
    var phone= document.adminForm.phone.value;
	if(phone=="")
	{		
		alert("Please Enter 10 Digit Number");
		return false;
	}
	if(isNaN(phone))
	{
		alert("Please Enter Only Number");
		return false;
	}

	if(!(phone.length == 10))
	{
		alert("Please Enter 10 Digit Number");
		return false;
	}
}	
</script>


<!DOCTYPE html>
<h2><?php echo JText::_('Order Summery'); ?></h2>


<?php 


if($_SESSION['cart']['idx']!=0)
 {
 	
 ?>
<?php
{

	 $newprice=0;
	 for($k=0;$k<$_SESSION['cart']['idx'];$k++)
	 { 
 	
?>

 <div class="shopdetial">
<table>
	<tr>
		<td><?php echo JText::_('SERVICE_NAME'); ?></td><td><?php echo JText::_('SERVICE_PRICE'); ?></td><td align="right"> <a href="index.php?option=com_appointment&view=cart_detail&task=delete_product&mid=<?php echo $k;?>"  >
  					<span class="remove"></span></a></td>
	</tr>
	<tr>
		<td><?php echo $_SESSION['cart'][$k]['ser_id']; ?></td><td><?php echo $_SESSION['cart'][$k]['price']; 
				$newprice=$newprice + $_SESSION['cart'][$k]['price'];
				 $_SESSION['totalprice']=$newprice;  ?>&nbsp;$<?php   ?></td>
	</tr>

<?php
if($_SESSION['cart'][$k]['name']!='')
{

?>
	<tr>
		<td><?php echo JText::_('VARIATION_NAME'); ?></td><td><?php echo $_SESSION['cart'][$k]['name'];?></td>
	</tr>

	<tr>
		<td><?php echo JText::_('VARIATION_PRICE'); ?></td><td><?php echo $_SESSION['cart'][$k]['price1'];?></td>
	</tr>
<?php } ?>

	<tr>
		<td><?php echo JText::_('EMPLOYEE_NAME'); ?></td><td>&nbsp;<?php  echo $_SESSION['cart'][$k]['emp_id'];?></td>
	</tr>

	<tr>
		<td><?php echo JText::_('DAY'); ?></td><td><?php echo JText::_('MONTH'); ?></td><td><?php echo JText::_('YEAR'); ?></td><td><!-- <?php echo JText::_('TIME'); ?> --></td>
	</tr>

	<tr>
		<td><?php echo $_SESSION['cart'][$k]['day']; ?></td><td><?php echo $_SESSION['cart'][$k]['month']; ?></td><td><?php echo $_SESSION['cart'][$k]['year']; ?></td><!-- <td><?php echo $_SESSION['cart'][$k]['time'];?></td> -->
	</tr>
</table>

<?php
}

}
?>
<div class="row" align="right" style=" width: 924px;">
		<?php echo JText::_('TOTAL_PRICE'); ?>&nbsp; <?php
		echo $newprice;
		?><span> &nbsp;$</span><?php
		
		?>
		</div>
<?php
}
?>

</div>
<div>
<a class="vapcontinueshoplink" href="/jeappointment/index.php" >Continue Shopping</a>
</div>
 


<!--===================== Client Register =================-->
<form action="" method="post" name="adminForm" id="adminForm" onsubmit="return check(this);">

<div class="vapcompleteorderdiv">
		<div class="vapcompleteorderheadtitle">Complete Order</div>
		
		<div style="display: none;" class="vapordererrordiv" id="vapordererrordiv">
			
		</div>
		
			<div class="vapcustomfields">
					<div>
						<span class="cf-label"><span class="vaprequired"><sup>*</sup></span> <span id="vapcf1" style="margin-right: 14px;">First Name</span> </span>
						<span class="cf-value">
						<input type="text" style="width: 220px;" class="vapinput" size="40" value="" id="vapcfinput1" name="fname">
						</span>
					</div>
					<div>
						<span class="cf-label"><span class="vaprequired"><sup>*</sup></span> <span id="vapcf2" style="margin-right: 15px;">Last Name</span> </span>
						<span class="cf-value">
						<input type="text" style="width: 220px;" class="vapinput" size="40" value="" id="vapcfinput2" name="lname">
						</span>
					</div>
					<div>
						<span class="cf-label"><span class="vaprequired"><sup>*</sup></span> 
						<span id="vapcf3" style="margin-right: 40px;">E-mail</span> </span>
						<span class="cf-value">
						<input type="text" style="width: 220px;" class="vapinput" size="40" value="" id="vapcfinput3" name="email">
						</span>
					</div>
					<div>
						<span class="cf-label"><span class="vaprequired"><sup>*</sup></span> 
						<span class="cf-label"><span id="vapcf4">Phone Number</span> </span>
						<span class="cf-value">
						<input id="vapcfinput3" class="vapinput" type="text" name="phone" value="" size="40" style="width: 220px;">
						</span>
					</div>
			</div>
		
				</div>		
					<div class="vapbookdiv">
						<span class="cf-label" style="margin-left: -10px;"><span id="vapcf5">Choose a way to pay :</span> </span>
						<span class="cf-value">
						<input type="radio" value="0" checked="" name="delivery">Cash on Delivery
						<input type="radio" value="1"   name="delivery">Paypal 
					
						</span>
					</div>
	<!-- <a href="index.php?option=com_appointment&view=cart_detail&task=save" class="vap-btn big blue">Confirm Appointment</a> -->
			 <input class="vap-btn big blue" type="submit" name="submit" id="submit"  value="Confirm Appointment" /> 

<input type="hidden" name="view" value="cart_detail"/>
<input type="hidden" name="cid[]"  id="cid"  value="<?php echo $value->id;?>"/>
<input type="hidden" name="task" id="task" value="save"/>
<input type="hidden" name="option" id="option" value="<?php echo $option;?>"/>
</form>
    
 

    
    
