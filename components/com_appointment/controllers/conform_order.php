<?php
 /**

* @package Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/

defined ( '_JEXEC' ) or die ( 'Restricted access' );
jimport ( 'joomla.application.component.controller' );
jimport( 'joomla.filesystem.file' );

class conform_orderController extends JControllerLegacy 
{
	
	function __construct($default = array()) 
	{
		parent::__construct ( $default );
		$this->_table_prefix = '#__appointment_';
	}
	
	function edit() 
	{
		JRequest::setVar ( 'view', 'categories' );
		JRequest::setVar ( 'layout', 'default' );
		JRequest::setVar ( 'hidemainmenu', 1 );
		parent::display ();
	
	}
	function save() 
	{
	} 
	
    function remove()
    {
    	$option = JRequest::getVar('option','','request','string');
		$cid = JRequest::getVar('cid',  '', 'request', 'string');
		$db = JFactory::getDBO(); 
		$app	= JFactory::getApplication();
		$res=0;
		if(!empty($cid))
		{
			$query ='DELETE FROM '.$this->_table_prefix.'staffs WHERE userid IN ('.$cid.')';
		    $db->setQuery($query);
		    $db->query();

		    $query ='DELETE FROM '.$this->_table_prefix.'usertypes WHERE userid IN ('.$cid.')';
		    $db->setQuery($query);
		    $db->query();

		    $query ='DELETE FROM #__user_usergroup_map WHERE user_id IN ('.$cid.')';
		    $db->setQuery($query);
		    $db->query();

			$query ='DELETE FROM #__users WHERE id IN ('.$cid.')';
		    $db->setQuery($query);
		    $res=$db->query();
			$nItemId ="";// JFactory::getApplication()->getMenu()->getItems('link', 'index.php?option=com_jesample&view=staff', true)->id;
		}
	    if($res)
		{
			$msg=JText::_('DETAIL_HAS_BEEN_DELETED_SUCCESS');
			$app->redirect(JRoute::_('index.php?option='.$option.'&view=staffs&Itemid='.$nItemId, $msg));
		}		
		else
		{
			$msg=JText::_('DETAIL_HAS_BEEN_NOT_DELETED_SUCCESS');
			$app->redirect(JRoute::_('index.php?option='.$option.'&view=staffs&Itemid='.$nItemId, $msg));
		}		
    }
}
?>