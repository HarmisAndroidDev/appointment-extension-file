<?php

/**
* @package  Appointment
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
**/
defined( '_JEXEC' ) or die( 'Restricted access' );


jimport( 'joomla.application.component.view' );
 
class employee_listViewemployee_list extends JViewLegacy
{
	function __construct($config = array())
	{
		 parent::__construct( $config );
	}
    
	function display($tpl = null)
	{
		
		global $mainframe, $context;
// ========================= Page title code ================================== //	
		$mainframe 	= JFactory::getApplication();
		$document 	=  JFactory::getDocument();
		$pagetitle 	= $document->getTitle();
		if($mainframe->getCfg( 'sitename_pagetitles' )==2) // After page title
			$document->setTitle($pagetitle.' - '.$mainframe->getCfg( 'sitename' ) );
		else if($mainframe->getCfg( 'sitename_pagetitles' )==1) // Before page title
			$document->setTitle($mainframe->getCfg( 'sitename' ).' - '.$pagetitle );
		else // no page title
			$document->setTitle($pagetitle );
// ======================= EOF Page title code ================================ //	
		
		$document =  JFactory::getDocument();
		$document->setTitle( JText::_('employee_list') );
   		$option	= JRequest::getVar('option', 'com_appointment','request','string');
   		$ser_id = JRequest::getVar('ser_id','','request','string');

		$uri	= JFactory::getURI();
		$appointment=  $this->get('Data');
		$service= $this->get('service');
		

	
		$db = JFactory::getDBO();
		
		$model=$this->getModel('employee_list');
		$requesturl = $uri->toString();
            	
		$this->assignRef('lists',$lists);
  		$this->assignRef('appointment',$appointment); 		
  		$this->assignRef('service',$service);
        $this->assignRef('request_url', $requesturl);	
    	parent::display($tpl);
  }
}
?>















