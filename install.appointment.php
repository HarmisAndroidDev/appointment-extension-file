<?php
/**
* @package   Appointment
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
**/
// no direct access
defined('_JEXEC') or die ;


if (version_compare(JVERSION, '1.6.0', '<'))
{
    jimport('joomla.installer.installer');

    // Load K2 language file
    $language = JFactory::getLanguage();
    $language->load('com_appointment');
    $db = JFactory::getDBO();
    $status = new stdClass;
    $status->modules = array();
    $status->plugins = array();
    $src = $this->parent->getPath('source');
   // $isUpdate = JFile::exists(JPATH_SITE.DS.'modules'.DS.'mod_k2_content'.DS.'mod_k2_content.php');

    $modules = $this->manifest->getElementByPath('modules');
    if (is_a($modules, 'JSimpleXMLElement') && count($modules->children()))
    {
        foreach ($modules->children() as $module)
        {
            $mname = $module->attributes('module');
            $client = $module->attributes('client');
            if (is_null($client))
            {
                $client = 'site';
            }
            $path = $client == 'administrator' ? $src.DS.'administrator'.DS.'modules'.DS.$mname : $src.DS.'modules'.DS.$mname;
            $installer = new JInstaller;
            $result = $installer->install($path);
            $status->modules[] = array('name' => $mname, 'client' => $client, 'result' => $result);
        }
}
}
?>
<?php if (version_compare(JVERSION, '1.6.0', '<')): ?>
<?php $rows = 0; ?>

<h2><?php echo JText::_('Appointment Install Successfully'); ?></h2>
<table class="adminlist">
	<thead>
		<tr>
			<th class="title" colspan="2"><?php echo JText::_('Component'); ?></th>
			<th width="30%"><?php echo JText::_('Status'); ?></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="3"></td>
		</tr>
	</tfoot>
	<tbody>
		<tr class="row0">
			<td class="key" colspan="2"><?php echo 'JE '.JText::_('Appointment Component'); ?></td>
			<td><strong><?php echo JText::_('Installed'); ?></strong></td>
		</tr>
		<?php if (count($status->modules)): ?>
		<tr>
			<th><?php echo JText::_('Module'); ?></th>
			<th><?php echo JText::_('Client'); ?></th>
			<th></th>
		</tr>
		<?php foreach ($status->modules as $module): ?>
		<tr class="row<?php echo(++$rows % 2); ?>">
			<td class="key"><?php echo $module['name']; ?></td>
			<td class="key"><?php echo ucfirst($module['client']); ?></td>
			<td><strong><?php echo ($module['result'])?JText::_('Installed'):JText::_('Not Installed'); ?></strong></td>
		</tr>
		<?php endforeach; ?>
		<?php endif; ?>
		<?php if (count($status->plugins)): ?>
		<tr>
			<th><?php echo JText::_('Plugin'); ?></th>
			<th><?php echo JText::_('Group'); ?></th>
			<th></th>
		</tr>
		<?php foreach ($status->plugins as $plugin): ?>
		<tr class="row<?php echo(++$rows % 2); ?>">
			<td class="key"><?php echo ucfirst($plugin['name']); ?></td>
			<td class="key"><?php echo ucfirst($plugin['group']); ?></td>
			<td><strong><?php echo ($plugin['result'])?JText::_('Installed'):JText::_('Not Installed'); ?></strong></td>
		</tr>
		<?php endforeach; ?>
		<?php endif; ?>
	</tbody>
</table>
<?php endif; ?>