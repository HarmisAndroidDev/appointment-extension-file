<?php
 /**

* @package Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.model');

class conform_orderModelconform_order extends JModelLegacy
{
	var $_id = null;
	var $_data = null;
	var $_region = null;
	var $_table_prefix = null;
	var $_copydata	=	null;

	function __construct()
	{
		parent::__construct();
		$this->_table_prefix = '#__appointment_';		
	  	$cid = JRequest::getVar('cid',  '', 'request', 'string');
		$this->setId($cid);
		
	}
	function setId($id)
	{
		$this->_id		= $id;
		$this->_data	= null;
	}
	function getData()
	{
		
		if ($this->_loadData())
		{
			
		}else  $this->_initData();
		return $this->_data;
	}
	 
	
	function _loadData()
	{
		
				/*$query = 'SELECT a.name AS grpname,b.* FROM  jos_appointment_groupsdetail AS a INNER JOIN jos_appointment_services AS b ON a.id=b.groups order by  b.groups,b.id'; */
				 $query='SELECT * FROM '.$this->_table_prefix.'order ';
				$this->_db->setQuery($query);
				$this->_data = $this->_db->loadObjectlist();
				return (boolean) $this->_data;
			
			
	}
	function getorder($id)
	{
		$query = 'SELECT * FROM `#__appointment_order` WHERE oid='.$id;
		$this->_db->setQuery($query);
		$this->_data = $this->_db->loadObjectlist();
		
		return $this->_data; 
	}

	function getorderservice($id)
	{
		  $query = 'SELECT servicename,price,vname,vprice FROM `#__appointment_orderservice` WHERE orid='.$id;
		$this->_db->setQuery($query);
		$this->_data = $this->_db->loadObjectlist();
		
		return $this->_data; 
	}
	
	function _initData()
	{
		if (empty($this->_data))
		{
			
			
			
		}
		return true;
	}
	
  	function store($data)
	{ 
		$row =$this->getTable('staffs');
		if (!$row->bind($data)) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}
		if (!$row->store()) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}
		return $row;
	}	
	function getDepartments()
	{
		$db=JFactory::getDBO();
		$query = 'SELECT id AS value,name AS text FROM '.$this->_table_prefix.'departments WHERE published=1'; 
		$db->setQuery($query);
		return $db->loadObjectList();
	}
}
?>
