<?php
 /**

* @package Appointment

* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.

* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php

* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com

**/

defined('_JEXEC') or die('Access Denied');
jimport('joomla.application.component.model');
jimport('joomla.filesystem.file');
class employeedetailsModelemployeedetails extends JModelLegacy

{

	var $_id = null;
	var $_data = null;
	var $_table_prefix = null;

 function __construct()
 {
 	parent::__construct();
 	$this->_table_prefix='#__appointment_';	 
 	$array=JRequest::getVar('cid',0,'','array');
 	$this->setId((int)$array[0]);

 }

 	function setId($id)
 	{
 		$this->_id=$id;
 		$this->_data=null;

 	}

 	function getData()
 	{
 		if($this->_loadData())
 		{

 		}else  $this->_initData();
 		return $this->_data;

 	}

 	function _loadData()
 	{
 		if(empty($this->_data))
 		{
 			$query='SELECT * FROM '.$this->_table_prefix.'employee WHERE id='.$this->_id; 
 			$this->_db->setQuery($query);
 			$this->_data=$this->_db->loadObject();
 			//echo <''>;
 			return (boolean) $this->_data;
  		}
  		return true;

 	}
 	 function _initData()
 	 {
 	 	if(empty($this->_data))
 	 	{
  		}
 	}
 	function store($data,$table)
 	{
 		$mylink='';
 		$row=$this->getTable($table);
 		$mainframe=JFactory::getApplication();
 		$option=JRequest::getVar('option');
 		if(!$row->bind($data))
 		{
  			$this->setError($this->_db->getErrorMsg());
			return false;
 		}
 		if(!$row->store())
		{
			$this->setError($this->_db->getErrorMsg());
			return false;

		}
		return $row->id;
		
 	}

 	function delete($id= array())
 	{
 		if(count($id))
 		{
 			$ids=implode(',', $id);
 			 $query='DELETE FROM '.$this->_table_prefix.'employee WHERE id IN ( '.$ids.' )'; 
 			$this->_db->setQuery( $query );
			if(!$this->_db->query()) {
				$this->setError($this->_db->getErrorMsg());
				return false;
 		}
 	}  
 		return true;
 	}
 	function publish($cid = array(), $publish = 1)
	{		
		if (count( $cid ))
		{
			$cids = implode( ',', $cid );
			
			  $query = 'UPDATE '.$this->_table_prefix.'employee '
				. ' SET published = ' . intval( $publish )
				. ' WHERE id IN ( '.$cids.' )';
			$this->_db->setQuery( $query );
			if (!$this->_db->query()) {
				$this->setError($this->_db->getErrorMsg());
				return false;
			}
		}

		return true;
	}


function getCdset()
	{
		
	  	 $query = 'SELECT * FROM '.$this->_table_prefix.'employeedaytime WHERE id='.$this->_id; 
			$this->_db->setQuery($query);
			$this->_data = $this->_db->loadObjectList();
			return $this->_data;
	}
	

	


	
	function getemployeedaytime($id=0)
	{
		
		 $query = 'SELECT * FROM '.$this->_table_prefix.'employeedaytime WHERE eid='.$this->_id;
		$this->_db->setQuery($query);
		$this->_data = $this->_db->loadObjectList();
		return $this->_data;
	}


	function deleteCdSet($cid = array())
	{
		if (count( $cid ))
		{
			$cids = implode( ',', $cid );
			
			  $query = 'DELETE FROM '.$this->_table_prefix.'employeedaytime WHERE eid IN ( '.$cids.' )';
			//echo $query;exit;
			$this->_db->setQuery($query );
			if(!$this->_db->query()) {
			$this->setError($this->_db->getErrorMsg());
				return false;
			}
		}

		return true;
	}


	
}
?>
